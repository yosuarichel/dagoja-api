import http from 'http';
import config from './config/config';
import App from './config/express';
import '@babel/polyfill';

// eslint-disable-next-line no-unused-vars
const debug = require('debug')('dagoja:index');
const socketIo = require('socket.io');

// make bluebird default Promise
Promise = require('bluebird'); // eslint-disable-line no-global-assign

class Index extends App {
    constructor(port, env) {
        super();
        this.port = port;
        this.env = env;
        this.server = http.Server(this.expressApp);
        this.io = socketIo(this.server, {
            path: '/api/socket',
            transports: ['polling'],
            origins: '*:*',
            allowEIO3: true,
            cors: true,
        });

        // module.parent check is required to support mocha watch
        if (require.main === module) {
            // Listen on this.port
            this.server.listen(this.port, () => {
                console.info(`Server started on port ${this.port} (${this.env})`); // eslint-disable-line no-console
            });
            this.socketIo(this.io);
        }
    }
}

export default new Index(config.port, config.env);
