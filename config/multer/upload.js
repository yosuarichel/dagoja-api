import errorCodes from '../../server/errors/index.error';

const multer = require('multer');
const path = require('path');
// require and configure dotenv, will load vars in .env in PROCESS.ENV
// require('dotenv').config();

const supplierStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        let folderName = '';
        switch (file.fieldname) {
        case 'supplier_image':
            folderName = 'supplier';
            break;
        case 'business_siup_image':
            folderName = 'business-siup';
            break;
        case 'business_npwp_image':
            folderName = 'business-npwp';
            break;
        case 'owner_nik_image':
            folderName = 'owner-nik';
            break;
        case 'owner_npwp_image':
            folderName = 'owner-npwp';
            break;
        case 'dropshipper_nik_image':
            folderName = 'dropshipper-nik';
            break;
        case 'dropshipper_image':
            folderName = 'dropshipper';
            break;
        case 'payment_method_image':
            folderName = 'payment-method';
            break;

        default:
            folderName = 'other';
            break;
        }
        return cb(null, `${process.env.ASSETS_ORIGINAL_PATH}/${folderName}/`);
    },
    filename: (req, file, cb) => cb(null, `${Date.now()}-${file.originalname.replace(/\s/g, '-').toLowerCase().substring(0, 50)}`),
});
const upload = multer({
    storage: supplierStorage,
    fileFilter: async (req, file, cb) => {
        const ext = path.extname(file.originalname);
        const filetype = file.mimetype.split('/').shift();
        const allowedExt = ['.png', '.jpg', '.jpeg', '.svg'];
        if (filetype !== 'image') {
            req.fileValidationError = [];
            req.fileValidationError.push({
                param: file.fieldname,
                msg: req.t(errorCodes.generalGEError.FILE.FILE_MUST_BE_IMAGE.message),
            });
            return cb(null, false, req.fileValidationError);
        }
        if (!allowedExt.includes(ext)) {
            req.fileValidationError = [];
            req.fileValidationError.push({
                param: file.fieldname,
                msg: req.t(errorCodes.generalGEError.FILE.FILE_EXT_NOT_ALLOWED.message),
            });
            return cb(null, false, req.fileValidationError);
        }
        return cb(null, true);
    },
    // limits: {
    //     fileSize: 500,
    // },
});

export default upload;
