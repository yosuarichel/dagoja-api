import passport from 'passport';
import LocalStrategy from 'passport-local';

import db from './sequelize';
// import initialization from '../server/misc/initialization';
import Middleware from '../server/misc/middleware';
import {
    Response,
} from '../server/response/response';
import errorCodes from '../server/errors/index.error';

const crypto = require('crypto');

const Admin = db.admin;

// class Passport {
//     constructor() {
//         this.passport = passport;
//         this.passportAdmin();
//         // App Use Checker
//         // this.initialization();
//     }

//     passportAdmin() {
//         console.log('masuk', this.passport)
//         this.passport.use('local', new LocalStrategy.Strategy({
//             usernameField: 'email',
//             passwordField: 'password',
//             // session: false,
//             // passReqToCallback: true,
//         }, (username, password, done) => {
//             console.log(username)
//             Admin.findOne({
//                 where: {
//                     email: username,
//                 },
//                 raw: true,
//             }).then((result) => {
//                 if (!result) {
//                     return done(errorCodes.authAUError.INVALID_CREDENTIAL);
//                 }
//                 if (result.status === 'inactive') {
//                     return done(errorCodes.authAUError.ACCOUNT_SUSPENDED);
//                 }
//                 const passHashed = crypto.createHash('sha256')
//                     .update(password)
//                     .digest('hex');
//                 if (result.password !== passHashed) {
//                     return done(errorCodes.authAUError.INVALID_CREDENTIAL);
//                 }
//                 console.log(result)
//                 return done(null, result);
//             }).catch((e) => console.log(e));
//         }));
//     }
// }

const passportAdmin = () => {
    passport.use('local', new LocalStrategy.Strategy({
        usernameField: 'email',
        passwordField: 'password',
        session: false,
        // passReqToCallback: true,
    }, (username, password, done) => {
        console.log(username)
        Admin.findOne({
            where: {
                email: username,
            },
            raw: true,
        }).then((result) => {
            if (!result) {
                return done(errorCodes.authAUError.INVALID_CREDENTIAL);
            }
            if (result.status === 'inactive') {
                return done(errorCodes.authAUError.ACCOUNT_SUSPENDED);
            }
            const passHashed = crypto.createHash('sha256')
                .update(password)
                .digest('hex');
            if (result.password !== passHashed) {
                return done(errorCodes.authAUError.INVALID_CREDENTIAL);
            }
            console.log(result)
            return done(null, result);
        }).catch((e) => console.log(e));
    }));
};

export default passportAdmin;
