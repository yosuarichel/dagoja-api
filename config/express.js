import express from 'express';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import compress from 'compression';
import methodOverride from 'method-override';
import cors from 'cors';
import httpStatus from 'http-status';
import expressWinston from 'express-winston';
import helmet from 'helmet';
import i18next from 'i18next';
import i18nextMiddleware from 'i18next-express-middleware';
import Backend from 'i18next-node-fs-backend';
import moment from 'moment';
import passport from 'passport';
import session from 'express-session';
import winstonInstance from './winston';
import MainRoute from '../server/routes/index.route';
import config from './config';
import Initialization from '../server/misc/initialization';
import Response from '../server/response/response';
import errorCodes from '../server/errors/index.error';

const sentry = require('@sentry/node');
const useragent = require('express-useragent');
const mongoose = require('mongoose');
const expressip = require('express-ip');
// const rid = require('connect-rid');
// const csurf = require('csurf');
// const csrfMiddleware = csurf({
//     cookie: true,
// });

mongoose.Promise = global.Promise;

require('dotenv').config();

sentry.init({
    debug: false,
    dsn: process.env.SENTRY_URL,
    environment: process.env.SENTRY_ENVIRONMENT,
});

class App {
    constructor() {
        this.express = express;
        this.expressApp = this.express();

        this.expressApp.use(sentry.Handlers.requestHandler());
        this.expressApp.use(sentry.Handlers.errorHandler());

        this.expressApp.sentry = sentry;

        this.expressApp.use(logger('dev'));

        // parse body params and attache them to req.body
        this.expressApp.use(express.json());
        this.expressApp.use(express.urlencoded({ extended: true }));

        this.expressApp.use(cookieParser());
        this.expressApp.use(compress());
        this.expressApp.use(methodOverride());

        // secure apps by setting various HTTP headers
        this.expressApp.use(helmet());

        // enable CORS - Cross Origin Resource Sharing
        this.cors();

        // Get user agent information
        this.expressApp.use(useragent.express());

        // Generate Rid
        // this.expressApp.use(rid({
        //     headerName: 'X-RID',
        // }));

        // CSURF Protection
        // this.expressApp.use(csrfMiddleware);

        // Call i18Next Translation
        this.translation();

        // Call passport middleware
        // this.passport();

        // App Use Checker
        this.initialization();

        // Mount all routes on /api path
        this.route();

        // Error handler
        this.errorHandler();

        // Catch 404 and forward to error handler
        this.errorNotfound();

        // Log error in winston transports except when executing test suite
        this.logError();

        // Error handler, send stacktrace only during development
        this.errorStackTrace();

        // Mongoose connection
        // this.mongooseConnection();
    }

    translation() {
        i18next
            .use(Backend)
            .use(i18nextMiddleware.LanguageDetector)
            .init({
                backend: {
                    loadPath: `${__dirname}/../server/locales/{{lng}}/{{ns}}.json`,
                    addPath: `${__dirname}/../server/locales/{{lng}}/{{ns}}.missing.json`,
                },
                fallbackLng: 'id',
                preload: ['en', 'id'],
                saveMissing: true,
                interpolation: {
                    format: (value, format) => {
                        if (format === 'uppercase') return value.charAt(0).toUpperCase() + value.slice(1);
                        return value;
                    },
                },
                detection: {
                    // order and from where user language should be detected
                    order: ['querystring', 'header', 'cookie'],
                    // keys or params to lookup language from
                    lookupQuerystring: 'lang',
                    lookupHeader: 'accept-language',
                },
            });
        this.expressApp.use(i18nextMiddleware.handle(i18next));
    }

    cors() {
        this.expressApp.use(cors({
            origin: '*',
        }));
        this.expressApp.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
            res.header('Access-Control-Allow-Headers', 'X-Requested-With');
            res.header('Access-Control-Allow-Headers', 'Content-Type');
            res.header('Access-Control-Max-Age', '3600');
            return next();
        });
    }

    passport() {
        this.expressApp.use(session({
            secret: 'keyboard cat',
            resave: true,
            saveUninitialized: true,
        }));
        this.expressApp.use(passport.initialize());
        this.expressApp.use(passport.session());
    }

    initialization() {
        this.expressApp.use(expressip().getIpInfoMiddleware);
        this.expressApp.use(Initialization.getSetting);
        // this.expressApp.use(middleware.sanitizePhoneNumber);
        // this.expressApp.use(initialization.checkUserData);
    }

    route() {
        // Get API Version from .env (or else assume 1.0)
        const routes = new MainRoute();
        const baseUrl = `/api/v${config.apiVersion}`;
        this.expressApp.use(`${baseUrl}`, routes);
    }

    errorHandler() {
        this.expressApp.use((err, req, res, next) => {
            // Set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = ['development', 'local'].includes(req.app.get('env')) ? err : {};
            return next(err);
        });
    }

    errorNotfound() {
        this.expressApp.use((req, res) => {
            const response = new Response(req, res);
            return response.failResponse(404, errorCodes.generalGEError.PAGE_NOT_FOUND);
        });
    }

    logError() {
        expressWinston.requestWhitelist.push('body');
        expressWinston.responseWhitelist.push('body');
        this.expressApp.use(expressWinston.logger({
            winstonInstance: winstonInstance.mongoLogger,
            msg: `{{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms ${moment().tz('Asia/Jakarta').format()}`,
        }));
        if (config.env !== 'test') {
            this.expressApp.use(expressWinston.errorLogger({
                winstonInstance: winstonInstance.mongoLogger,
            }));
        }
    }

    errorStackTrace() {
        this.expressApp.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
            res.status(err.status).json({
                message: err.isPublic ? err.message : httpStatus[err.status],
                stack: config.env === 'development' ? err.stack : {},
            });
        });
    }

    socketIo(io) {
        this.expressApp.io = io;
        io.on('connection', (socket) => {
            this.expressApp.io = io;
            // eslint-disable-next-line no-console
            console.log(`Client connected...with id = ${socket.id}`);
            socket.on('disconnect', () => {
                this.expressApp.io = io;
                // io.emit('disconnect');
            });
        });
    }

    mongooseConnection() {
        mongoose.connect(process.env.MONGODB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        }).then(() => {
            this.expressApp.listen(process.env.MONGODB_PORT, () => {
                // eslint-disable-next-line no-console
                console.log(`Mongodb server started on port ${process.env.MONGODB_PORT}`);
            });
        }).catch((e) => {
            // eslint-disable-next-line no-console
            console.log('Mongodb connection failed.');
            // eslint-disable-next-line no-console
            console.log('with error => ', e);
        });
    }
}

export default App;
