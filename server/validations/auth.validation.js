/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';
import Middleware from '../misc/middleware';

const adminRole = db.admin_role;
const phoneOperator = db.phone_operator;
const Occupation = db.occupation;
const businessInformation = db.business_information;
const ownerInformation = db.owner_information;
const Supplier = db.supplier;
const Bank = db.bank;

// const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'registerAdmin': {
        return [
            body('admin_role_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && adminRole.findOne({
                    where: {
                        admin_role_id: value,
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('full_name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('email')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isEmail().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_EMAIL.message)),
            body('password')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('status')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isIn(['active', 'inactive']).withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ENUM.message)),
        ];
    }

    case 'loginAdmin': {
        return [
            body('email')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isEmail().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_EMAIL.message)),
            body('password')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'registerSupplier': {
        return [
            body('account_email')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isEmail().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_EMAIL.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Supplier.findOne({
                            where: {
                                email: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('account_password')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('confirmation_account_password')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        if (value !== req.body.account_password) {
                            return Promise.reject();
                        }
                        return Promise.resolve();
                    }
                    return Promise.reject();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.VALUE_NOT_MATCH.message)),
            body('account_phone_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom(async (value, { req }) => {
                    if (value) {
                        const sanitize = await middleware.sanitizePhoneNumber(req, value);
                        if (!sanitize) {
                            return Promise.reject(new Error(req.t(errorCodes.validationVLError.INVALID_PHONE_NUMBER.message)));
                        }
                        const checkPhoneOperator = await phoneOperator.findOne({
                            where: {
                                code: sanitize,
                            },
                            raw: true,
                        });
                        if (!checkPhoneOperator) {
                            return Promise.reject(new Error(req.t(errorCodes.phoneOperatorPOError.PHONE_OPERATOR_NOT_FOUND.message)));
                        }
                        return Promise.resolve();
                    }
                    return Promise.resolve();
                })
                .customSanitizer(async (value, { req }) => {
                    if (value) {
                        await middleware.sanitizePhoneNumber(req, value);
                        return req.body.account_phone_number;
                    }
                    return Promise.resolve();
                })
                .custom((value, { req }) => {
                    if (value) {
                        return Supplier.findOne({
                            where: {
                                phone_number: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),

            body('business_name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('business_siup_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return businessInformation.findOne({
                            where: {
                                siup_number: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('business_npwp_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return businessInformation.findOne({
                            where: {
                                npwp_number: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('business_province')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('business_city')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('business_district')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('business_village')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('business_postal_code')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message)),
            body('business_address')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),

            body('owner_name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('owner_occupation_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Occupation.findOne({
                            where: {
                                occupation_id: value,
                                type: 'bisnis',
                            },
                        }).then((result) => {
                            if (!result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('owner_nik_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return ownerInformation.findOne({
                            where: {
                                nik_number: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('owner_npwp_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return ownerInformation.findOne({
                            where: {
                                npwp_number: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('owner_bank_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Bank.findOne({
                            where: {
                                bank_id: value,
                            },
                        }).then((result) => {
                            if (!result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('owner_bank_account')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('owner_province')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('owner_city')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('owner_district')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('owner_village')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('owner_postal_code')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message)),
            body('owner_address')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'loginSupplier': {
        return [
            body('email')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isEmail().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_EMAIL.message)),
            body('password')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'registerDropshipper': {
        return [
            body('email')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isEmail().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_EMAIL.message)),
            body('password')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('full_name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('phone_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom(async (value, { req }) => {
                    if (value) {
                        const sanitize = await middleware.sanitizePhoneNumber(req, value);
                        if (!sanitize) {
                            return Promise.reject(new Error(req.t(errorCodes.validationVLError.INVALID_PHONE_NUMBER.message)));
                        }
                        const checkPhoneOperator = await phoneOperator.findOne({
                            where: {
                                code: sanitize,
                            },
                            raw: true,
                        });
                        if (!checkPhoneOperator) {
                            return Promise.reject(new Error(req.t(errorCodes.phoneOperatorPOError.PHONE_OPERATOR_NOT_FOUND.message)));
                        }
                        return Promise.resolve();
                    }
                    return Promise.resolve();
                })
                .customSanitizer(async (value, { req }) => {
                    if (value) {
                        await middleware.sanitizePhoneNumber(req, value);
                        return req.body.phone_number;
                    }
                    return Promise.resolve();
                }),
            body('birth_date')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('gender')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isIn(['m', 'f']).withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ENUM.message)),
            body('nik')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('occupation_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && Occupation.findOne({
                    where: {
                        occupation_id: value,
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('province')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('city')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('district')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('vilage')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('postal_code')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message)),
            body('address')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'loginDropshipper': {
        return [
            body('email')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isEmail().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_EMAIL.message)),
            body('password')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'sendEmailDropshipper': {
        return [
            body('email')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isEmail().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_EMAIL.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
