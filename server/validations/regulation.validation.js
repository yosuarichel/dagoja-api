/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';

const Regulation = db.regulation;

const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'createRegulation': {
        return [
            body('type')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .toLowerCase()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Regulation.findOne({
                            where: {
                                type: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('description')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'updateRegulation': {
        return [
            body('type')
                .optional()
                .toLowerCase()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Regulation.findOne({
                            where: {
                                regulation_id: {
                                    [Sequelize.Op.not]: req.params.regulationId,
                                },
                                type: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('description')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
