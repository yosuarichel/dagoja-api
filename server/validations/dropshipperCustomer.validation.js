/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';
import Middleware from '../misc/middleware';

const Dropshipper = db.dropshipper;
// const dropshipperCustomer = db.dropshipper_customer;
const phoneOperator = db.phone_operator;

// const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'createDropshipperCustomer': {
        return [
            body('full_name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('phone_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .isLength({ min: 9 }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_MATCH_MIN_CHARACTER.message))
                .isLength({ max: 14 }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_MATCH_MAX_CHARACTER.message))
                .custom(async (value, { req }) => {
                    if (value) {
                        const sanitize = await middleware.sanitizePhoneNumber(req, value);
                        if (!sanitize) {
                            return Promise.reject(new Error(req.t(errorCodes.validationVLError.INVALID_PHONE_NUMBER.message)));
                        }
                        const checkPhoneOperator = await phoneOperator.findOne({
                            where: {
                                code: sanitize,
                            },
                            raw: true,
                        });
                        if (!checkPhoneOperator) {
                            return Promise.reject(new Error(req.t(errorCodes.phoneOperatorPOError.PHONE_OPERATOR_NOT_FOUND.message)));
                        }
                        return Promise.resolve();
                    }
                    return Promise.resolve();
                })
                .customSanitizer(async (value, { req }) => {
                    if (value) {
                        await middleware.sanitizePhoneNumber(req, value);
                        return req.body.phone_number;
                    }
                    return Promise.resolve();
                }),
            body('province')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('city')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('district')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('vilage')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('postal_code')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message)),
            body('address')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'updateDropshipperCustomer': {
        return [
            body('full_name')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('phone_number')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom(async (value, { req }) => {
                    if (value) {
                        const sanitize = await middleware.sanitizePhoneNumber(req, value);
                        if (!sanitize) {
                            return Promise.reject(new Error(req.t(errorCodes.validationVLError.INVALID_PHONE_NUMBER.message)));
                        }
                        const checkPhoneOperator = await phoneOperator.findOne({
                            where: {
                                code: sanitize,
                            },
                            raw: true,
                        });
                        if (!checkPhoneOperator) {
                            return Promise.reject(new Error(req.t(errorCodes.phoneOperatorPOError.PHONE_OPERATOR_NOT_FOUND.message)));
                        }
                        return Promise.resolve();
                    }
                    return Promise.resolve();
                })
                .customSanitizer(async (value, { req }) => {
                    if (value) {
                        await middleware.sanitizePhoneNumber(req, value);
                        return req.body.phone_number;
                    }
                    return Promise.resolve();
                }),
            body('province')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('city')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('district')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('vilage')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('postal_code')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message)),
            body('address')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
