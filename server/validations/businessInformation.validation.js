/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';
import Middleware from '../misc/middleware';

const phoneOperator = db.phone_operator;
const businessInformation = db.business_information;
const Supplier = db.supplier;

// const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'createBusinessInformation': {
        return [
            body('supplier_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Supplier.findOne({
                            where: {
                                supplier_id: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('siup_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return businessInformation.findOne({
                            where: {
                                siup_number: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('npwp_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return businessInformation.findOne({
                            where: {
                                npwp_number: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('province')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('city')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('district')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('vilage')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('postal_code')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message)),
            body('address')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('status')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'updateBusinessInformation': {
        return [
            body('supplier_id')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Supplier.findOne({
                            where: {
                                supplier_id: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('name')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('siup_number')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return businessInformation.findOne({
                            where: {
                                siup_number: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('npwp_number')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return businessInformation.findOne({
                            where: {
                                npwp_number: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('province')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('city')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('district')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('vilage')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('postal_code')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message)),
            body('address')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('status')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
