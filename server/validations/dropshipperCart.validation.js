/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';

const productVariant = db.product_variant;
const dropshipperCart = db.dropshipper_cart;

// const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'createDropshipperCart': {
        return [
            body('product_variant_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && productVariant.findOne({
                    where: {
                        product_variant_id: value,
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
        ];
    }

    case 'updateDropshipperCart': {
        return [
            body('product_variant_id')
                .optional().isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && productVariant.findOne({
                    where: {
                        product_variant_id: value,
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
        ];
    }

    case 'decreaseQuantityProduct': {
        return [
            body('dropshipper_cart_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && dropshipperCart.findOne({
                    where: {
                        dropshipper_cart_id: value,
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
