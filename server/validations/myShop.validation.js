/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';

const Dropshipper = db.dropshipper;

// const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'createMyShop': {
        return [
            body('shop_name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .matches(/^[a-zA-Z0-9 ]*$/).withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('contact')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'updateMyShop': {
        return [
            body('shop_name')
                .optional()
                .matches(/^[a-zA-Z0-9 ]*$/).withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('link')
                .optional()
                .isAlphanumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('contact')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
