/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';

const Product = db.product;
const Brand = db.brand;
const Category = db.category;
const Supplier = db.supplier;
const productVariant = db.product_variant;

// const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'createProduct': {
        return [
            body('brand_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && Brand.findOne({
                    where: {
                        brand_id: value,
                        status: 'active',
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('category_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && Category.findOne({
                    where: {
                        category_id: value,
                        status: 'active',
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('supplier_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && Supplier.findOne({
                    where: {
                        supplier_id: value,
                        status: 'active',
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            // body('sku')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
            //     .toUpperCase()
            //     .custom((value, { req }) => value && Product.findOne({
            //         where: {
            //             sku: value.toUpperCase(),
            //         },
            //     }).then((result) => {
            //         if (result) {
            //             return Promise.reject();
            //         }
            //         return Promise.resolve();
            //     })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('supplier_price')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('ppn_percentage')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('markup_percentage')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),

            body('variant')
                .toArray()
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            body('variant.*.sku')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => value && productVariant.findOne({
                    where: {
                        sku: value,
                    },
                }).then((result) => {
                    if (result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('variant.*.variant_name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('variant.*.variant_option')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('variant.*.variant_value')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('variant.*.quantity')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .customSanitizer((value) => {
                    if (value < 0) {
                        return 0;
                    }
                    return value;
                }),
            body('variant.*.remaining_quantity')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .customSanitizer((value) => {
                    if (value < 0) {
                        return 0;
                    }
                    return value;
                }),

            // body('quantity')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
            //     .custom((value, { req }) => {
            //         if (value < 0) {
            //             return Promise.reject();
            //         }
            //         return Promise.resolve();
            //     }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            // body('remaining_quantity')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
            //     .custom((value, { req }) => {
            //         if (value < 0) {
            //             return Promise.reject();
            //         }
            //         return Promise.resolve();
            //     }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            // body('sizes')
            //     .toArray()
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            // body('available_sizes')
            //     .toArray()
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            // body('colors')
            //     .toArray()
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            // body('available_colors')
            //     .toArray()
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            body('weight')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('condition')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('minimum_order')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('is_discount')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isBoolean().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_BOOLEAN.message)),
            body('discount_percentage')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('is_warranty')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isBoolean().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_BOOLEAN.message)),
            body('warranty_expired_at')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('description')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('status')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('is_disabled')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isBoolean().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_BOOLEAN.message)),
            body('is_coming_soon')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isBoolean().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_BOOLEAN.message)),
            body('os')
                .toArray()
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),

        ];
    }

    case 'updateProduct': {
        return [
            body('brand_id')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && Brand.findOne({
                    where: {
                        brand_id: value,
                        status: 'active',
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('category_id')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && Category.findOne({
                    where: {
                        category_id: value,
                        status: 'active',
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('supplier_id')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && Supplier.findOne({
                    where: {
                        supplier_id: value,
                        status: 'active',
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('sku')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .toUpperCase()
                .custom((value, { req }) => value && Product.findOne({
                    where: {
                        sku: value.toUpperCase(),
                    },
                }).then((result) => {
                    if (result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('name')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('supplier_price')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('ppn_percentage')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('markup_percentage')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('quantity')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('remaining_quantity')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('sizes')
                .toArray()
                .optional()
                .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            body('available_sizes')
                .toArray()
                .optional()
                .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            body('colors')
                .toArray()
                .optional()
                .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            body('available_colors')
                .toArray()
                .optional()
                .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            body('weight')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('condition')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('minimum_order')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('is_discount')
                .optional()
                .isBoolean().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_BOOLEAN.message)),
            body('discount_percentage')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('is_warranty')
                .optional()
                .isBoolean().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_BOOLEAN.message)),
            body('warranty_expired_at')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('description')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('status')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('is_disabled')
                .optional()
                .isBoolean().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_BOOLEAN.message)),
            body('is_coming_soon')
                .optional()
                .isBoolean().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_BOOLEAN.message)),
            body('os')
                .toArray()
                .optional()
                .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
        ];
    }

    case 'addCatalog': {
        return [
            body('product_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('price')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
        ];
    }

    case 'updateProductCatalog': {
        return [
            body('price')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
            body('discount')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value < 0) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.INVALID_VALUE.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
