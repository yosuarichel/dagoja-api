/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';

const City = db.city;
const Province = db.province;

// const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'createCity': {
        return [
            body('name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return City.findOne({
                            where: {
                                name: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('province_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Province.findOne({
                            where: {
                                province_id: value,
                            },
                        }).then((result) => {
                            if (!result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
        ];
    }

    case 'updateCity': {
        return [
            body('name')
                .optional().isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return City.findOne({
                            where: {
                                name: value,
                            },
                        }).then((result) => {
                            if (result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.ALREADY_IN_USE.message)),
            body('province_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Province.findOne({
                            where: {
                                province_id: value,
                            },
                        }).then((result) => {
                            if (!result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
