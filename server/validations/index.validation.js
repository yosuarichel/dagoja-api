import adminValidation from './admin.validation';
import settingValidation from './setting.validation';
import adminRoleValidation from './adminRole.validation';
import authValidation from './auth.validation';
import loginMethodValidation from './loginMethod.validation';
import appVersionValidation from './appVersion.validation';
import brandValidation from './brand.validation';
import productValidation from './product.validation';
import productGaleryValidation from './productGalery.validation';
import targetBonusValidation from './targetBonus.validation';
import affilialBonusValidation from './affilialBonus.validation';
import supplierValidation from './supplier.validation';
import categoryValidation from './category.validation';
import phoneOperatorValidation from './phoneOperator.validation';
import bannerTypeValidation from './bannerType.validation';
import bannerValidation from './banner.validation';
import occupationValidation from './occupation.validation';
import countryValidation from './country.validation';
import provinceValidation from './province.validation';
import cityValidation from './city.validation';
import districtValidation from './district.validation';
import villageValidation from './village.validation';
import bankValidation from './bank.validation';
import businessInfoValidation from './businessInformation.validation';
import ownerInfoValidation from './ownerInformation.validation';
import supplierLogValidation from './supplierLog.validation';
import regulationValidation from './regulation.validation';
import supplierNotificationValidation from './supplierNotification.validation';
import myShopValidation from './myShop.validation';
import dropshipperCartValidation from './dropshipperCart.validation';
import dropshipperCustomerValidation from './dropshipperCustomer.validation';
import paymentMethodValidation from './paymentMethod.validation';


export default {
    adminValidation,
    settingValidation,
    adminRoleValidation,
    authValidation,
    loginMethodValidation,
    appVersionValidation,
    brandValidation,
    productValidation,
    productGaleryValidation,
    targetBonusValidation,
    affilialBonusValidation,
    supplierValidation,
    categoryValidation,
    phoneOperatorValidation,
    bannerTypeValidation,
    bannerValidation,
    occupationValidation,
    countryValidation,
    provinceValidation,
    cityValidation,
    districtValidation,
    villageValidation,
    bankValidation,
    businessInfoValidation,
    ownerInfoValidation,
    supplierLogValidation,
    regulationValidation,
    supplierNotificationValidation,
    myShopValidation,
    dropshipperCartValidation,
    dropshipperCustomerValidation,
    paymentMethodValidation,
};
