/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';

const bannerType = db.banner_type;

// const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'createBanner': {
        return [
            body('name')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('banner_type_id')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && bannerType.findOne({
                    where: {
                        banner_type_id: value,
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('description')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('os')
                .toArray()
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            body('status')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'updateBanner': {
        return [
            body('name')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('banner_type_id')
                .optional()
                .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message))
                .custom((value, { req }) => value && bannerType.findOne({
                    where: {
                        banner_type_id: value,
                    },
                }).then((result) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                })).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
            body('description')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('os')
                .optional()
                .toArray()
                .isArray().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_ARRAY.message)),
            body('status')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
