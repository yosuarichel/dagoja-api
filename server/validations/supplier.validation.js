/* eslint-disable no-unused-vars */
/* eslint-disable newline-per-chained-call */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import errorCodes from '../errors/index.error';
import Middleware from '../misc/middleware';

const phoneOperator = db.phone_operator;
const Supplier = db.supplier;

// const Sequelize = require('sequelize');
const { body } = require('express-validator');

function validate(method) {
    switch (method) {
    case 'createSupplier': {
        return [
            // body('name')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('email')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('username')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('password')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('phone_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom(async (value, { req }) => {
                    if (value) {
                        const sanitize = await middleware.sanitizePhoneNumber(req, value);
                        if (!sanitize) {
                            return Promise.reject(new Error(req.t(errorCodes.validationVLError.INVALID_PHONE_NUMBER.message)));
                        }
                        const checkPhoneOperator = await phoneOperator.findOne({
                            where: {
                                code: sanitize,
                            },
                            raw: true,
                        });
                        if (!checkPhoneOperator) {
                            return Promise.reject(new Error(req.t(errorCodes.phoneOperatorPOError.PHONE_OPERATOR_NOT_FOUND.message)));
                        }
                        return Promise.resolve();
                    }
                    return Promise.resolve();
                })
                .customSanitizer(async (value, { req }) => {
                    if (value) {
                        await middleware.sanitizePhoneNumber(req, value);
                        return req.body.phone_number;
                    }
                    return Promise.resolve();
                }),
            // body('id_number')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('id_type')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('province')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('city')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('district')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('vilage')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('postal_code')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message)),
            // body('address')
            //     .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('status')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'updateSupplier': {
        return [
            // body('name')
            //     .optional()
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('email')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('username')
            //     .optional()
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('password')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('phone_number')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom(async (value, { req }) => {
                    if (value) {
                        const sanitize = await middleware.sanitizePhoneNumber(req, value);
                        if (!sanitize) {
                            return Promise.reject(new Error(req.t(errorCodes.validationVLError.INVALID_PHONE_NUMBER.message)));
                        }
                        const checkPhoneOperator = await phoneOperator.findOne({
                            where: {
                                code: sanitize,
                            },
                            raw: true,
                        });
                        if (!checkPhoneOperator) {
                            return Promise.reject(new Error(req.t(errorCodes.phoneOperatorPOError.PHONE_OPERATOR_NOT_FOUND.message)));
                        }
                        return Promise.resolve();
                    }
                    return Promise.resolve();
                })
                .customSanitizer(async (value, { req }) => {
                    if (value) {
                        await middleware.sanitizePhoneNumber(req, value);
                        return req.body.phone_number;
                    }
                    return Promise.resolve();
                }),
            // body('id_number')
            //     .optional()
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('id_type')
            //     .optional()
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('province')
            //     .optional()
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('city')
            //     .optional()
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('district')
            //     .optional()
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('vilage')
            //     .optional()
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            // body('postal_code')
            //     .optional()
            //     .isNumeric().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_INTEGER.message)),
            // body('address')
            //     .optional()
            //     .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
            body('status')
                .optional()
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message)),
        ];
    }

    case 'approveSupplier': {
        return [
            body('supplier_number')
                .not().isEmpty().withMessage((value, { req }) => req.t(errorCodes.validationVLError.REQUIRED.message))
                .isString().withMessage((value, { req }) => req.t(errorCodes.validationVLError.MUST_BE_STRING.message))
                .custom((value, { req }) => {
                    if (value) {
                        return Supplier.findOne({
                            where: {
                                supplier_number: value,
                            },
                        }).then((result) => {
                            if (!result) {
                                return Promise.reject();
                            }
                            return Promise.resolve();
                        });
                    }
                    return Promise.resolve();
                }).withMessage((value, { req }) => req.t(errorCodes.validationVLError.NOT_FOUND.message)),
        ];
    }

    default: {
        return [];
    }
    }
}

export default { validate };
