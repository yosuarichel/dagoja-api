/* eslint-disable max-len */
// import moment from 'moment';
import Sequelize from 'sequelize';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Product = db.product;
const productGalery = db.product_galery;
const Brand = db.brand;
const Category = db.category;
const productVariant = db.product_variant;
const _ = require('lodash');
// const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    return Product
        // .scope([
        //     { method: ['includeProductGaleryApp', productGalery] },
        //     { method: ['includeBrandApp', Brand] },
        //     { method: ['includeCategoryApp', Category] },
        //     { method: ['includeVariantApp', productVariant] },
        //     { method: ['normalizeAttributesApp'] },
        // ])
        .findOne({
            where: {
                product_id: id,
                os: {
                    [Sequelize.Op.overlap]: [req.useragent.platform.toLowerCase()],
                },
                status: 'active',
            },
            // attributes: [
            //     'warranty_expired_at',
            //     'deleted_at',
            //     'created_at',
            //     'updated_at',
            //     'product_id',
            //     'brand_id',
            //     'category_id',
            //     'supplier_id',
            //     'product_number',
            //     'name',
            //     'supplier_price',
            //     'ppn_percentage',
            //     'markup_percentage',
            //     'final_price',
            //     'quantity',
            //     'remaining_quantity',
            //     'weight',
            //     'condition',
            //     'minimum_order',
            //     'is_discount',
            //     'discount_percentage',
            //     'is_warranty',
            //     'description',
            //     'status',
            //     'is_disabled',
            //     'is_coming_soon',
            //     'os',
            //     [Sequelize.fn('SUM', Sequelize.col('variant.quantity')), 'variant_quantity'],
            //     [Sequelize.fn('SUM', Sequelize.col('variant.remaining_quantity')), 'variant_remaining_quantity'],
            // ],
            // group: ['product.product_id', 'variant.product_variant_id', 'brand.brand_id', 'category.category_id', 'galery.product_galery_id'],
            include: [
                {
                    model: Brand,
                    as: 'brand',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                    },
                    // attributes: [],
                }, {
                    model: Category,
                    as: 'category',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                    },
                    // attributes: [],
                }, {
                    model: productVariant,
                    as: 'variant',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at'],
                    },
                    // attributes: [],
                }, {
                    model: productGalery,
                    as: 'galery',
                    attributes: {
                        exclude: ['product_id', 'created_at', 'updated_at', 'deleted_at'],
                    },
                    // attributes: [],
                },
            ],
        })
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.productPRError.PRODUCT_NOT_FOUND);
            }
            if (result.dataValues.variant && result.dataValues.variant.length > 0) {
                const quantitySum = _.sumBy(result.variant, (x) => x.dataValues.quantity);
                // eslint-disable-next-line max-len
                const remainingQuantitySum = _.sumBy(result.variant, (x) => x.dataValues.remaining_quantity);
                Object.assign(result.dataValues, {
                    total_quantity: quantitySum,
                    total_remaining_quantity: remainingQuantitySum,
                });
            }
            req.product = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const response = new Response(req, res);
    return response.successResponse(200, req.product);
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'product_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
            os: {
                [Sequelize.Op.overlap]: [req.useragent.platform.toLowerCase()],
            },
            status: 'active',
        },
        include: [
            {
                model: Brand,
                as: 'brand',
                attributes: {
                    exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                },
            }, {
                model: Category,
                as: 'category',
                attributes: {
                    exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                },
            }, {
                model: productVariant,
                as: 'variant',
                attributes: {
                    exclude: ['created_at', 'updated_at', 'deleted_at'],
                },
            }, {
                model: productGalery,
                as: 'galery',
                limit: 1,
                attributes: {
                    exclude: ['product_id', 'created_at', 'updated_at', 'deleted_at'],
                },
            },
        ],
        distinct: true,
    };
    if (req.query.search) {
        Object.assign(option.where, {
            name: {
                [Sequelize.Op.iLike]: `%${req.query.search}%`,
            },
        });
    }
    if (req.query.category_id) {
        Object.assign(option.where, {
            category_id: req.query.category_id,
        });
    }
    if (req.query.start_price && req.query.end_price) {
        Object.assign(option.where, {
            final_price: {
                [Sequelize.Op.gte]: req.query.start_price,
                [Sequelize.Op.lte]: req.query.end_price,
            },
        });
    }
    return Product
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}
export default {
    load,
    get,
    list,
};
