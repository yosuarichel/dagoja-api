import Sequelize from 'sequelize';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';
// import helper from '../../misc/helper';

// const fs = require('fs');

const Banner = db.banner;
const bannerType = db.banner_type;
// const _ = require('lodash');
// const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    Banner
        .findOne({
            where: {
                banner_id: id,
                os: {
                    [Sequelize.Op.overlap]: [req.useragent.platform.toLowerCase()],
                },
            },
        })
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.bannerBNError.BANNER_NOT_FOUND);
            }
            req.banner = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const response = new Response(req, res);
    return response.successResponse(200, req.banner);
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'banner_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
            os: {
                [Sequelize.Op.overlap]: [req.useragent.platform.toLowerCase()],
            },
            status: 'active',
        },
        include: [],
        distinct: true,
        attributes: {
            exclude: ['description', 'image_name'],
        },
    };
    if (req.query.banner_type_id) {
        Object.assign(option.where, {
            banner_type_id: req.query.banner_type_id,
        });
    }
    return Banner
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
            { method: ['includeBannerTypeApp', bannerType] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.bannerBNError.BANNER_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}


export default {
    load,
    get,
    list,
};
