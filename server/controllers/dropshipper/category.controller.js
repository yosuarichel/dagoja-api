import Sequelize from 'sequelize';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Product = db.product;
// const productGalery = db.product_galery;
// const Brand = db.brand;
const Category = db.category;
// const productVariant = db.product_variant;
const dropshipperCatalog = db.dropshipper_catalog;
// const _ = require('lodash');
// const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');


function list(req, res) {
    const ordering = simpleOrdering(req, 'category_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    if (req.query.name) {
        option.where.name = req.query.name;
    }
    return Category
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.categoryCTError.CATEGORY_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function getCategoryOfCatalogProduct(req, res) {
    const { dropshipperData } = req;
    const option = {
        where: {
            dropshipper_id: dropshipperData.dropshipper_id,
        },
        raw: true,
        attributes: [
            [Sequelize.col('product.category.category_id'), 'category_id'],
            [Sequelize.col('product.category.name'), 'name'],
            [Sequelize.col('product.category.image_source'), 'image_source'],
        ],
        group: ['product.category.category_id'],
        include: [],
        distinct: true,
    };
    return dropshipperCatalog.scope([
        { method: ['includeCategoryOfProductApp', Product, Category] },
    ]).findAll(option).then((result) => {
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

async function getCategoryOfProduct(req, res) {
    const option = {
        where: {
        },
        raw: true,
        attributes: [
            [Sequelize.col('category.category_id'), 'category_id'],
            [Sequelize.col('category.name'), 'name'],
            [Sequelize.col('category.image_source'), 'image_source'],
        ],
        group: ['category.category_id'],
        include: [],
        distinct: true,
    };
    return Product.scope([
        { method: ['includeCategoryOfProductApp', Category] },
    ]).findAll(option).then((result) => {
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}


export default {
    list,
    getCategoryOfCatalogProduct,
    getCategoryOfProduct,
};
