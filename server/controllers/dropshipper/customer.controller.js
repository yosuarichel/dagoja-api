/* eslint-disable max-len */
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Sequelize = require('sequelize');

const dropshipperCustomer = db.dropshipper_customer;
const _ = require('lodash');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    dropshipperCustomer
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404,
                    errorCodes.dropshipperCustomerDCSError.DROPSHIPPER_CUSTOMER_NOT_FOUND);
            }
            req.dropshipperCustomerData = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function get(req, res) {
    const { dropshipperData } = req;
    return dropshipperCustomer.findOne({
        where: {
            dropshipper_customer_id: req.params.dropshipperCustomerId,
            dropshipper_id: dropshipperData.dropshipper_id,
        },
        raw: true,
    }).then((result) => {
        if (!result) {
            const response = new Response(req, res);
            return response.failResponse(404,
                errorCodes.dropshipperCustomerDCSError.DROPSHIPPER_CUSTOMER_NOT_FOUND);
        }
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

const create = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const { dropshipperData } = req;

    const dropshipperCustomerData = await dropshipperCustomer.count({
        where: {
            phone_number: {
                [Sequelize.Op.iLike]: `%${req.body.phone_number}%`,
            },
        },
        raw: true,
    });
    if (dropshipperCustomerData > 0) {
        const response = new Response(400, errorCodes.dropshipperCustomerDCSError.DROPSHIPPER_CUSTOMER_ALREADY_EXIST);
        return response.failResponse(req, res);
    }
    req.body.dropshipper_id = dropshipperData.dropshipper_id;
    return dropshipperCustomer.create(req.body)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

function update(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const { dropshipperData } = req;

    return dropshipperCustomer.findOne({
        where: {
            dropshipper_customer_id: req.params.dropshipperCustomerId,
            dropshipper_id: dropshipperData.dropshipper_id,
        },
    }).then(async (result) => {
        if (!result) {
            const response = new Response(404,
                errorCodes.dropshipperCustomerDCSError.DROPSHIPPER_CUSTOMER_NOT_FOUND);
            return response.failResponse(req, res);
        }
        return result.update(req.body).then(() => {
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        }).catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

function list(req, res) {
    const { dropshipperData } = req;
    const ordering = simpleOrdering(req, 'dropshipper_customer_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
            dropshipper_id: dropshipperData.dropshipper_id,
        },
        include: [],
        distinct: true,
    };
    if (req.query.search) {
        Object.assign(option.where, {
            [Sequelize.Op.or]: [{
                full_name: {
                    [Sequelize.Op.iLike]: `%${req.query.search}%`,
                },
            }, {
                phone_number: {
                    [Sequelize.Op.iLike]: `%${req.query.search}%`,
                },
            }, {
                address: {
                    [Sequelize.Op.iLike]: `%${req.query.search}%`,
                },
            }],
        });
    }
    return dropshipperCustomer
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function remove(req, res) {
    const { dropshipperData } = req;
    return dropshipperCustomer.findOne({
        where: {
            dropshipper_customer_id: req.params.dropshipperCustomerId,
            dropshipper_id: dropshipperData.dropshipper_id,
        },
    }).then(async (result) => {
        if (!result) {
            const response = new Response(req, res);
            return response.failResponse(404,
                errorCodes.dropshipperCustomerDCSError.DROPSHIPPER_CUSTOMER_NOT_FOUND);
        }
        return result.destroy()
            .then(() => {
                const response = new Response(req, res);
                return response.successResponse(200, 'OK');
            })
            .catch((e) => {
                const response = new Response(req, res);
                return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
            });
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

export default {
    load,
    get,
    create,
    update,
    list,
    remove,
};
