import moment from 'moment';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import helper from '../../misc/helper';
import Response from '../../response/response';


const Sequelize = require('sequelize');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const Dropshipper = db.dropshipper;
const dropshipperSession = db.dropshipper_session;
const { sequelize } = db;
const _ = require('lodash');
const { validationResult } = require('express-validator');
// const { simpleOrdering, simplePagination } = require('../../misc/misc');


const registerDropshipper = async (req, res) => {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        if (req.fileValidationError.length > 0) {
            req.fileValidationError.map((x) => errors.errors.push(x));
        }
    }
    if (!errors.isEmpty()) {
        if (!_.isEmpty(req.files)) {
            if (req.files.dropshipper_image) {
                fs.unlinkSync(req.files.dropshipper_image[0].path);
            }
            if (req.files.dropshipper_nik_image) {
                fs.unlinkSync(req.files.dropshipper_nik_image[0].path);
            }
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const dropshipperCount = await Dropshipper.count({
        where: {
            [Sequelize.Op.or]: [{
                email: {
                    [Sequelize.Op.iLike]: `%${req.body.email}%`,
                },
            }, {
                phone_number: req.body.phone_number,
            }],
        },
        raw: true,
    });
    if (dropshipperCount > 0) {
        if (!_.isEmpty(req.files)) {
            if (req.files.dropshipper_image) {
                fs.unlinkSync(req.files.dropshipper_image[0].path);
            }
            if (req.files.dropshipper_nik_image) {
                fs.unlinkSync(req.files.dropshipper_nik_image[0].path);
            }
        }
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.dropshipperDRError.DROPSHIPPER_ALREADY_EXIST);
    }
    const passHashed = crypto.createHash('sha256')
        .update(req.body.password)
        .digest('hex');

    let uploadDropshipperImage = null;
    let uploadDropshipperNIKImage = null;
    if (!_.isEmpty(req.files)) {
        if (req.files.dropshipper_image) {
            const filename = req.files.dropshipper_image[0].filename.split('.')[0];
            uploadDropshipperImage = await helper.uploadCloudinary(req.files.dropshipper_image[0].path, filename, 'dropshipper');
            if (!uploadDropshipperImage) {
                fs.unlinkSync(req.files.dropshipper_image[0].path);
                const response = new Response(req, res);
                return response.failResponse(400,
                    errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
            }
            req.body.profile_image_name = uploadDropshipperImage.public_id;
            req.body.profile_image_source = uploadDropshipperImage.secure_url;
        }
        if (req.files.dropshipper_nik_image) {
            const filename = req.files.dropshipper_nik_image[0].filename.split('.')[0];
            uploadDropshipperNIKImage = await helper.uploadCloudinary(req.files.dropshipper_nik_image[0].path, filename, 'dropshipper-nik');
            if (!uploadDropshipperNIKImage) {
                fs.unlinkSync(req.files.dropshipper_nik_image[0].path);
                const response = new Response(req, res);
                return response.failResponse(400,
                    errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
            }
            req.body.nik_image = uploadDropshipperNIKImage.public_id;
            req.body.nik_image_source = uploadDropshipperNIKImage.secure_url;
        }
    }

    req.body.email_token = crypto.createHash('sha512')
        .update(`${req.body.email}+${req.body.phone_number}+${Date.now()}`)
        .digest('hex');

    const payload = {
        email: req.body.email,
        password: passHashed,
        full_name: req.body.full_name,
        phone_number: req.body.phone_number,
        birth_date: req.body.birth_date,
        gender: req.body.gender,
        nik: req.body.id_number,
        occupation_id: req.body.occupation_id,
        is_email_verif: false,
        email_token: req.body.email_token,
        is_phone_number_verif: false,
        nik_image: req.body.nik_image,
        nik_image_source: req.body.nik_image_source,
        profile_image_name: req.body.profile_image_name,
        profile_image_source: req.body.profile_image_source,
        province: req.body.province,
        city: req.body.city,
        district: req.body.district,
        vilage: req.body.vilage,
        postal_code: req.body.postal_code,
        address: req.body.address,
        cash: 0,
        poin: 0,
        status: 'wait-verification',
    };
    const t = await sequelize.transaction();
    return Dropshipper.create(payload, {
        transaction: t,
    })
        .then(async (result) => {
            const dropshipperNumber = `DRP-${result.dropshipper_id}-${Date.now()}`;
            await Dropshipper.update({
                dropshipper_number: dropshipperNumber,
            }, {
                where: {
                    dropshipper_id: result.dropshipper_id,
                },
                transaction: t,
            });
            Object.assign(result.dataValues, {
                dropshipper_number: dropshipperNumber,
            });
            if (!_.isEmpty(req.files)) {
                if (req.files.dropshipper_image) {
                    fs.unlinkSync(req.files.dropshipper_image[0].path);
                }
                if (req.files.dropshipper_nik_image) {
                    fs.unlinkSync(req.files.dropshipper_nik_image[0].path);
                }
            }
            await helper.sendEmail('verification-email', {
                sender: 'noreply@dagojastudio.id',
                recipient: req.body.email,
                subject: 'Verification Account Dagoja Dropshipper',
                context: {
                    token: req.body.email_token,
                    url: `http://${req.headers.host}/api/v1.0/dropshipper/auth/verification/email/${req.body.email_token}`,
                    name: result.full_name,
                },
            });

            await t.commit();
            const response = new Response(req, res);
            return response.successResponse(200, {
                email: result.email,
                dropshipper_number: dropshipperNumber,
                status: result.status,
            });
        })
        .catch(async (e) => {
            await t.rolllback();
            if (!_.isEmpty(req.files)) {
                if (req.files.dropshipper_image) {
                    await helper.removeCloudinary(uploadDropshipperImage.public_id);
                    fs.unlinkSync(req.files.dropshipper_image[0].path);
                }
                if (req.files.dropshipper_nik_image) {
                    await helper.removeCloudinary(uploadDropshipperNIKImage.public_id);
                    fs.unlinkSync(req.files.dropshipper_nik_image[0].path);
                }
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

const loginDropshipper = async (req, res) => {
    const { settingData } = req;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const dropshipperData = await Dropshipper.findOne({
        where: {
            email: req.body.email,
        },
        raw: true,
    });
    if (!dropshipperData) {
        const response = new Response(req, res);
        return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
    }
    if (dropshipperData.status === 'suspend') {
        const response = new Response(req, res);
        return response.failResponse(401, errorCodes.authAUError.ACCOUNT_SUSPENDED);
    }
    if (dropshipperData.status === 'inactive') {
        const response = new Response(req, res);
        return response.failResponse(401, errorCodes.authAUError.INACTIVE_ACCOUNT);
    }
    if (dropshipperData.status === 'wait-verification') {
        const response = new Response(req, res);
        return response.failResponse(401, errorCodes.authAUError.WAIT_VERIFICATION);
    }

    const passHashed = crypto.createHash('sha256')
        .update(req.body.password)
        .digest('hex');
    if (dropshipperData.password !== passHashed) {
        const response = new Response(req, res);
        return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
    }

    const sessionHash = crypto.createHash('sha256')
        .update(`Dropshipper${dropshipperData.dropshipper_id}${dropshipperData.email}${Date.now()}`)
        .digest('hex');
    const now = moment(Date.now()).tz('Asia/Jakarta').format();
    const split = settingData.dropshipper_session_expiry.split(';');
    const valueExp = split[0];
    const intervalExp = split[1];
    const sessionExpiry = moment(now).add(valueExp, `${intervalExp}`).tz('Asia/Jakarta');
    const diff = sessionExpiry.diff(now) / 1000;

    const dropshipperSessionData = await dropshipperSession.findOne({
        where: {
            dropshipper_id: dropshipperData.dropshipper_id,
        },
        raw: true,
    });
    if (dropshipperSessionData) {
        await dropshipperSession.update({
            session: sessionHash,
            expiry_value: diff,
            expiry_date: moment(sessionExpiry).tz('Asia/Jakarta').format(),
            status: 'logged-in',
        }, {
            where: {
                dropshipper_id: dropshipperData.dropshipper_id,
            },
        });
    }
    if (!dropshipperSessionData) {
        await dropshipperSession.create({
            dropshipper_id: dropshipperData.dropshipper_id,
            session: sessionHash,
            expire_value: diff,
            expired_at: moment(sessionExpiry).tz('Asia/Jakarta').format(),
            status: 'logged-in',
        });
    }

    const token = jwt.sign({
        type: 'dropshipper',
        name: dropshipperData.full_name,
        email: dropshipperData.email,
        session: sessionHash,
        session_expiry: moment(sessionExpiry).tz('Asia/Jakarta').format(),
        status: dropshipperData.status,
    }, process.env.JWT_SECRET, { expiresIn: diff });
    res.cookie('accessToken', token, { maxAge: diff * 1000 });
    // const response = new Response(200, {
    //     email: dropshipperData.email,
    //     session: sessionHash,
    //     session_expiry: moment(sessionExpiry).tz('Asia/Jakarta').format(),
    //     status: dropshipperData.status,
    //     token,
    // });
    const response = new Response(req, res);
    return response.successResponse(200, {
        email: dropshipperData.email,
        session: sessionHash,
        session_expiry: moment(sessionExpiry).tz('Asia/Jakarta').format(),
        status: dropshipperData.status,
        token,
    });
};

const emailVerification = async (req, res) => {
    if (!req.params.token) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.authAUError.TOKEN_REQUIRED);
    }
    return Dropshipper.findOne({
        where: {
            email_token: req.params.token,
            is_email_verif: false,
            status: 'wait-verification',
        },
        raw: true,
    }).then(async (result) => {
        if (!result) {
            const response = new Response(req, res);
            return response.failResponse(404, errorCodes.authAUError.ACCOUNT_NOT_FOUND);
        }
        await Dropshipper.update({
            is_email_verif: true,
            status: 'active',
        }, {
            where: {
                email_token: req.params.token,
                is_email_verif: false,
                status: 'wait-verification',
            },
        });
        const response = new Response(req, res);
        return response.successResponse(200, 'Verification success, please login into your account');
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
};

const logoutDropshipper = async (req, res) => {
    const { dropshipperData } = req;
    return dropshipperSession.update({
        status: 'logged-out',
    }, {
        where: {
            dropshipper_id: dropshipperData.dropshipper_id,
            session: dropshipperData.session,
        },
    }).then(() => {
        const response = new Response(req, res);
        return response.successResponse(200, 'Logged Out');
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
};

const sendEmailVerification = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const t = await sequelize.transaction();

    return Dropshipper.findOne({
        where: {
            email: req.body.email,
            is_email_verif: false,
            status: 'wait-verification',
        },
        raw: true,
    }).then(async (result) => {
        if (!result) {
            const response = new Response(req, res);
            return response.failResponse(404, errorCodes.authAUError.ACCOUNT_NOT_FOUND);
        }
        req.body.email_token = crypto.createHash('sha512')
            .update(`${result.email}+${req.body.phone_number}+${Date.now()}`)
            .digest('hex');

        return Dropshipper.update({
            email_token: req.body.email_token,
        }, {
            where: {
                dropshipper_id: result.dropshipper_id,
            },
            transaction: t,
        }).then(async () => {
            await helper.sendEmail('verification-email', {
                sender: 'noreply@dagojastudio.id',
                recipient: result.email,
                subject: 'Verification Account Dagoja Dropshipper',
                context: {
                    token: req.body.email_token,
                    url: `http://${req.headers.host}/api/v1.0/dropshipper/auth/verification/email/${req.body.email_token}`,
                    name: result.full_name,
                },
            });
            await t.commit();
            const response = new Response(req, res);
            return response.successResponse(200, {
                email: result.email,
                dropshipper_number: result.dropshipper_number,
                status: result.status,
            });
        }).catch(async (e) => {
            await t.rollback();
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
};

const profileDropshipper = async (req, res) => {
    const { dropshipperData } = req;
    delete dropshipperData.password;
    delete dropshipperData.email_token;
    delete dropshipperData.session;
    const response = new Response(req, res);
    return response.successResponse(200, dropshipperData);
};


export default {
    registerDropshipper,
    loginDropshipper,
    emailVerification,
    logoutDropshipper,
    sendEmailVerification,
    profileDropshipper,
};
