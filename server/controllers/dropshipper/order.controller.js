/* eslint-disable max-len */
// import moment from 'moment';
// import Sequelize from 'sequelize';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Order = db.order;
const orderStatus = db.order_status;
// const Dropshipper = db.dropshipper;
const dropshipperCustomer = db.dropshipper_customer;
const orderProduct = db.order_product;
// const _ = require('lodash');
// const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    return Order
        .findOne({
            where: {
                order_id: id,
            },
            include: [
                {
                    model: dropshipperCustomer,
                    as: 'customer',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at'],
                    },
                }, {
                    model: orderStatus,
                    as: 'order_status',
                    order: [['updated_at', 'DESC']],
                }, {
                    model: orderProduct,
                    as: 'order_product',
                    attributes: {
                        include: ['galery', 'name', 'final_price', 'weight', 'variant_sku', 'variant_name', 'variant_option', 'purchased_quantity'],
                    },
                },
            ],
        })
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.orderORError.ORDER_NOT_FOUND);
            }
            req.order = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const response = new Response(200, req.order);
    return response.successResponse(req, res);
}

function list(req, res) {
    const { dropshipperData } = req;
    const ordering = simpleOrdering(req, 'order_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
            dropshipper_id: dropshipperData.dropshipper_id,
        },
        include: [
            {
                model: dropshipperCustomer,
                as: 'customer',
                attributes: {
                    exclude: ['created_at', 'updated_at', 'deleted_at'],
                },
            },
            {
                model: orderStatus,
                as: 'order_status',
                limit: 1,
                order: [['updated_at', 'DESC']],
            },
        ],
        distinct: true,
    };
    if (req.query.dropshipper_customer_id) {
        Object.assign(option.where, {
            dropshipper_customer_id: req.query.dropshipper_customer_id,
        });
    }
    if (req.query.order_number) {
        Object.assign(option.where, {
            order_number: req.query.order_number,
        });
    }
    if (req.query.delivery_option_id) {
        Object.assign(option.where, {
            delivery_option_id: req.query.delivery_option_id,
        });
    }

    return Order
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}
export default {
    load,
    get,
    list,
};
