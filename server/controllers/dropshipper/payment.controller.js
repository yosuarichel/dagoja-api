/* eslint-disable max-len */
import { DateTime } from 'luxon';
import Sequelize from 'sequelize';

import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import RequestFunction from '../../misc/request';
import Response from '../../response/response';


const dropshipperCart = db.dropshipper_cart;
const dropshipperCatalog = db.dropshipper_catalog;
const Product = db.product;
const productGalery = db.product_galery;
const Brand = db.brand;
const Category = db.category;
const productVariant = db.product_variant;
const Order = db.order;
const orderProduct = db.order_product;
const paymentMethod = db.payment_method;
const dropshipperCustomer = db.dropshipper_customer;
const _ = require('lodash');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');


async function createOrder(req, res) {
    const { dropshipperData } = req;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    return console.log('masuk');
    try {
        const cartData = await dropshipperCart
            .findOne({
                where: {
                    dropshipper_id: dropshipperData.dropshipper_id,
                },
            });
    } catch (e) {
        const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    }

    // return dropshipperCart
    //     .findOne({
    //         where: {
    //             dropshipper_id: dropshipperData.dropshipper_id,
    //         },
    //     })
    //     .then((result) => {
    //         if (!result) {
    //             const response = new Response(404,
    //                 errorCodes.dropshipperCartDCError.DROPSHIPPER_CART_NOT_FOUND);
    //             return response.failResponse(req, res);
    //         }
    //         const response = new Response(200, result);
    //         return response.successResponse(req, res);
    //     })
    //     .catch((e) => {
    //         const response = new Response(500, errorCodes.generalGEError.SOMETHING_WRONG);
    //         return response.failResponse(req, res, e);
    //     });
}

async function createPayment(req, res) {
    const { dropshipperData } = req;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const paymentMethodData = await paymentMethod.findOne({
        where: {
            code: req.body.payment_method_code,
            status: 'active',
        },
        raw: true,
    });
    if (!paymentMethodData) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.paymentMethodPMError.PAYMENT_METHOD_NOT_FOUND);
    }

    const cartData = await dropshipperCart
        .scope([
            { method: ['includeProductCartPaymentApp', req.useragent.platform.toLowerCase(), Product, Brand, Category, productGalery] },
            { method: ['includeProductVariantCartApp', productVariant] },
        ])
        .findAll({
            where: {
                dropshipper_id: dropshipperData.dropshipper_id,
            },
            order: [['dropshipper_cart_id', 'asc']],
            include: [],
            distinct: true,
        });
    const cartDataJson = JSON.parse(JSON.stringify(cartData));
    if (cartDataJson.length === 0) {
        const response = new Response(req, res);
        return response.failResponse(400,
            errorCodes.dropshipperCartDCError.DROPSHIPPER_CART_NOT_FOUND);
    }

    await cartDataJson.map((x) => {
        if (x.quantity > x.variant.remaining_quantity) {
            const response = new Response(req, res);
            return response.failResponse(400,
                errorCodes.dropshipperCartDCError.SOME_PRODUCT_IS_NOT_AVAIABLE);
        }
        if (x.product.status === 'inactive' || x.product.is_disabled) {
            const response = new Response(req, res);
            return response.failResponse(400,
                errorCodes.dropshipperCartDCError.SOME_PRODUCT_IS_NOT_AVAIABLE);
        }
        if (x.product.is_coming_soon) {
            const response = new Response(req, res);
            return response.failResponse(400,
                errorCodes.dropshipperCartDCError.SOME_PRODUCT_IS_NOT_AVAIABLE);
        }
        return true;
    });

    const customerData = await dropshipperCustomer.findOne({
        where: {
            dropshipper_customer_id: req.body.dropshipper_customer_id,
        },
        raw: true,
    });
    if (!customerData) {
        const response = new Response(req, res);
        return response.failResponse(400,
            errorCodes.dropshipperCustomerDCSError.DROPSHIPPER_CUSTOMER_NOT_FOUND);
    }

    // eslint-disable-next-line max-len
    const totalProductPrice = _.sum(cartDataJson.map((x) => x.quantity * x.product.final_price));
    const totalQuantity = _.sum(cartDataJson.map((x) => x.quantity));
    req.body.dropshipper_id = dropshipperData.dropshipper_id;
    req.body.total_item_price = totalProductPrice;
    req.body.total_price = totalProductPrice;
    req.body.delivery_fee = 0;
    req.body.payment_status = 'pending';
    req.body.payment_type = 'OVO';

    const dateNow = DateTime.local().toFormat('yyyyMMddHHmmss');
    const orderNumber = `ORDER-${req.body.dropshipper_id}-${paymentMethodData.payment_method_id}-${totalQuantity}-${dateNow}`;
    req.body.order_number = orderNumber;
    const createOrderData = await Order.create(req.body);
    const orderProductPayload = [];

    console.log(cartDataJson[0].product, '====================cart data');

    await cartDataJson.map((x) => orderProductPayload.push({
        order_id: createOrderData.order_id,
        product_id: x.product.product_id,
        variant_id: x.product_variant_id,
        brand_id: x.product.brand_id,
        category_id: x.product.category_id,
        supplier_id: x.product.supplier_id,

        purchased_quantity: x.quantity,

        brand_name: x.product.brand.name,
        brand_image_source: x.product.brand.image_source,

        category_name: x.product.category.name,
        category_image_source: x.product.category.image_source,

        product_number: x.product.product_number,
        name: x.product.name,
        supplier_price: x.product.supplier_price,
        ppn_percentage: x.product.ppn_percentage,
        markup_percentage: x.product.markup_percentage,
        final_price: x.product.final_price,
        quantity: x.product.quantity,
        remaining_quantity: x.product.remaining_quantity,
        weight: x.product.remaining_quantity,
        condition: x.product.condition,
        minimum_order: x.product.minimum_order,
        is_discount: x.product.is_discount,
        discount_percentage: x.product.discount_percentage,
        is_warranty: x.product.is_warranty,
        warranty_expired_at: x.product.warranty_expired_at,
        description: x.product.description,
        status: x.product.status,
        is_disabled: x.product.is_disabled,
        is_coming_soon: x.product.is_coming_soon,
        galery: x.product.galery,

        variant_sku: x.variant.sku,
        variant_name: x.variant.variant_name,
        variant_option: x.variant.variant_option,
        variant_value: x.variant.variant_value,
        variant_quantity: x.variant.quantity,
        variant_remaining_quantity: x.variant.remaining_quantity,
        os: x.product.os,
    }));

    // return console.log(orderProductPayload);
    await orderProduct.bulkCreate(orderProductPayload);
    // return console.log('sukses');

    const xendit = new RequestFunction().xenditNode();
    const { EWallet } = xendit;
    const walletTransaction = new EWallet({});

    return walletTransaction.createEWalletCharge({
        referenceID: orderNumber,
        currency: 'IDR',
        amount: req.body.total_price,
        checkoutMethod: 'ONE_TIME_PAYMENT',
        channelCode: 'ID_OVO',
        channelProperties: {
            mobileNumber: req.body.phone_number,
        },
    }).then(async (result) => {
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

export default {
    createOrder,
    createPayment,
};
