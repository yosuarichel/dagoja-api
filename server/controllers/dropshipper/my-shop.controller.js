import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Sequelize = require('sequelize');

const myShop = db.my_shop;
const targetBonus = db.target_bonus;
const _ = require('lodash');
const { validationResult } = require('express-validator');
// const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    myShop
        .scope([
            { method: ['includeTargetBonusApp', targetBonus] },
        ])
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.myShopMSError.MY_SHOP_NOT_FOUND);
            }
            req.myShopData = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const { dropshipperData } = req;
    return myShop
        .scope([
            { method: ['includeTargetBonusApp', targetBonus] },
        ])
        .findOne({
            where: {
                dropshipper_id: dropshipperData.dropshipper_id,
            },
        })
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.myShopMSError.MY_SHOP_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

const create = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const { settingData, dropshipperData } = req;
    const myShopData = await myShop.findOne({
        where: {
            [Sequelize.Op.or]: [{
                dropshipper_id: dropshipperData.dropshipper_id,
            }, {
                shop_name: req.body.shop_name,
            }],
        },
        raw: true,
    });
    if (myShopData) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.myShopMSError.MY_SHOP_ALREADY_EXIST);
    }
    const shopName = req.body.shop_name.toLowerCase().replace(' ', '-');
    req.body.dropshipper_id = dropshipperData.dropshipper_id;
    req.body.target_bonus_id = 1;
    req.body.link = `${settingData.shop_url}/${shopName}`;
    req.body.current_omzet = 0;
    return myShop.create(req.body)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

async function update(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const { dropshipperData, settingData } = req;

    req.body.droshipper_id = undefined;
    req.body.target_bonus_id = undefined;
    req.body.current_omzet = undefined;

    const option = {
        where: {
            dropshipper_id: {
                [Sequelize.Op.not]: dropshipperData.dropshipper_id,
            },
        },
        raw: true,
    };
    if (req.body.shop_name) {
        option.where.shop_name = req.body.shop_name;
    }
    if (req.body.link) {
        option.where.link = `${settingData.shop_url}/${req.body.link}`;
    }
    const myShopData = await myShop.findOne(option);
    if (myShopData) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.myShopMSError.MY_SHOP_ALREADY_EXIST);
    }
    return req.myShopData.update(req.body)
        .then(() => {
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}


export default {
    load,
    get,
    create,
    update,
};
