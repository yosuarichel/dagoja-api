import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import RequestFunction from '../../misc/request';
import Response from '../../response/response';


const dropshipperCart = db.dropshipper_cart;
const dropshipperCatalog = db.dropshipper_catalog;
const Product = db.product;
const productGalery = db.product_galery;
const Brand = db.brand;
const Category = db.category;
const productVariant = db.product_variant;
const Order = db.order;
const orderStatus = db.order_status;
const _ = require('lodash');
const Sequelize = require('sequelize');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

async function ovoCallback(req, res) {
    try {
        console.log(req.body);
        const orderData = await Order.findOne({
            where: {
                order_number: req.body.data.reference_id,
                payment_status: 'pending',
            },
        });
        if (!orderData) {
            const response = new Response(req, res);
            return response.successResponse(400, errorCodes.orderORError.ORDER_NOT_FOUND);
        }
        console.log(orderData);
        const cartData = await dropshipperCart
            .scope([
                { method: ['includeProductCartCallbackApp', Product, Brand, Category, productGalery] },
                { method: ['includeProductVariantCartApp', productVariant] },
            ])
            .findAll({
                where: {
                    dropshipper_id: orderData.dropshipper_id,
                },
                order: [['dropshipper_cart_id', 'asc']],
                include: [],
                distinct: true,
            });
        const cartDataJson = JSON.parse(JSON.stringify(cartData));
        console.log(cartDataJson)
        if (cartDataJson.length === 0) {
            const response = new Response(req, res);
            return response.failResponse(400,
                errorCodes.dropshipperCartDCError.DROPSHIPPER_CART_NOT_FOUND);
        }
        await orderStatus.create({
            order_id: orderData.order_id,
            status: 'process',
        });
        await orderData.update({
            payment_status: 'success',
        });
        const response = new Response(req, res);
        return response.successResponse(200, req.body);
    } catch (e) {
        console.log(e);
        const response = new Response(req, res);
        return response.successResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    }
}

export default {
    ovoCallback,
};
