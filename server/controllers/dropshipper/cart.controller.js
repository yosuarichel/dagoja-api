/* eslint-disable max-len */
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const dropshipperCart = db.dropshipper_cart;
const dropshipperCatalog = db.dropshipper_catalog;
const Product = db.product;
const productGalery = db.product_galery;
const Brand = db.brand;
const Category = db.category;
const productVariant = db.product_variant;
const _ = require('lodash');
const Sequelize = require('sequelize');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    dropshipperCart
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404,
                    errorCodes.dropshipperCartDCError.DROPSHIPPER_CART_NOT_FOUND);
            }
            req.dropshipperCartData = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const { dropshipperData } = req;

    return dropshipperCart
        .findOne({
            where: {
                dropshipper_cart_id: req.params.cartId,
                dropshipper_id: dropshipperData.dropshipper_id,
            },
        })
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404,
                    errorCodes.dropshipperCartDCError.DROPSHIPPER_CART_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function list(req, res) {
    const { dropshipperData } = req;
    const ordering = simpleOrdering(req, 'dropshipper_cart_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
            quantity: {
                [Sequelize.Op.gt]: 0,
            },
            dropshipper_id: dropshipperData.dropshipper_id,
        },
        include: [],
        distinct: true,
    };

    if (req.query.supplier_id) {
        option.where.supplier_id = req.query.supplier_id;
    }
    return dropshipperCart
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
            { method: ['includeProductCartApp', req.useragent.platform.toLowerCase(), Product, Brand, Category, productGalery] },
            { method: ['includeProductVariantCartApp', productVariant] },
        ])
        .findAndCountAll(option)
        .then(async (result) => {
            if (result.rows.length > 0) {
                // eslint-disable-next-line array-callback-return
                await result.rows.map((x) => {
                    Object.assign(x.dataValues, {
                        is_different_quantity: false,
                        is_disabled: false,
                        is_coming_soon: false,
                    });
                    if (x.quantity > x.variant.remaining_quantity) {
                        Object.assign(x.dataValues, {
                            is_different_quantity: true,
                        });
                    } else if (x.product.status === 'inactive' || x.product.is_disabled) {
                        Object.assign(x.dataValues, {
                            is_disabled: true,
                        });
                    } else if (x.product.is_coming_soon) {
                        Object.assign(x.dataValues, {
                            is_coming_soon: true,
                        });
                    }
                });
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

const increase = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const { dropshipperData } = req;

    const catalogProduct = await dropshipperCatalog.scope([
        { method: ['includeProductApp', req.useragent.platform.toLowerCase(), req.body.product_variant_id, Product, Brand, Category, productGalery, productVariant] },
    ]).findOne({
        where: {
            dropshipper_id: dropshipperData.dropshipper_id,
            status: 'active',
            '$product.status$': 'active',
            '$product.is_disabled$': false,
            '$product.is_coming_soon$': false,
            '$product.remaining_quantity$': {
                [Sequelize.Op.gt]: 0,
            },
            is_disabled: false,
            is_coming_soon: false,
        },
    });
    if (!catalogProduct) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.productPRError.PRODUCT_CATALOG_NOT_FOUND);
    }

    req.body.quantity = 1;
    req.body.dropshipper_id = dropshipperData.dropshipper_id;
    req.body.product_id = catalogProduct.product_id;

    // eslint-disable-next-line max-len
    const variantQuantity = catalogProduct.product.variant.length > 0 ? catalogProduct.product.variant[0].remaining_quantity : 0;
    return dropshipperCart.findOne({
        where: {
            dropshipper_id: dropshipperData.dropshipper_id,
            product_id: catalogProduct.product_id,
            product_variant_id: req.body.product_variant_id,
        },
    }).then(async (result) => {
        if (result) {
            if (result.quantity >= variantQuantity) {
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.dropshipperCartDCError.MAX_QUANTITY);
            }
            await result.increment('quantity');
            const response = new Response(req, res);
            return response.successResponse(200, result);
        }
        return dropshipperCart.create(req.body)
            .then(async (createdResult) => {
                const response = new Response(req, res);
                return response.successResponse(200, createdResult);
            })
            .catch(async (e) => {
                const response = new Response(req, res);
                return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
            });
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
};

const decrease = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const { dropshipperData } = req;

    return dropshipperCart
        .findOne({
            where: {
                dropshipper_id: dropshipperData.dropshipper_id,
                dropshipper_cart_id: req.body.dropshipper_cart_id,
            },
        }).then(async (result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.productPRError.PRODUCT_CATALOG_NOT_FOUND);
            }
            await result.decrement('quantity', {
                where: {
                    dropshipper_id: dropshipperData.dropshipper_id,
                    quantity: {
                        [Sequelize.Op.gt]: 1,
                    },
                },
            });
            const response = new Response(req, res);
            return response.successResponse(200, result);
        }).catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

function remove(req, res) {
    const { dropshipperData } = req;

    return dropshipperCart
        .findOne({
            where: {
                dropshipper_cart_id: req.params.cartId,
                dropshipper_id: dropshipperData.dropshipper_id,
            },
        })
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404,
                    errorCodes.dropshipperCartDCError.DROPSHIPPER_CART_NOT_FOUND);
            }
            return result.destroy()
                .then(() => {
                    const response = new Response(200, 'OK');
                    return response.successResponse(req, res);
                })
                .catch((e) => {
                    const response = new Response(req, res);
                    return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
                });
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

export default {
    load,
    get,
    increase,
    list,
    decrease,
    remove,
};
