// import moment from 'moment';
import Sequelize from 'sequelize';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Product = db.product;
const productGalery = db.product_galery;
const Brand = db.brand;
const Category = db.category;
const productVariant = db.product_variant;
const dropshipperCatalog = db.dropshipper_catalog;
const { sequelize } = db;
const _ = require('lodash');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

async function addToCatalog(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const { dropshipperData } = req;

    const t = await sequelize.transaction();

    const productData = await Product.scope([
        { method: ['includeProductGaleryApp', productGalery] },
        { method: ['includeBrandApp', Brand] },
        { method: ['includeCategoryApp', Category] },
        { method: ['includeVariantApp', productVariant] },
    ]).findOne({
        where: {
            product_number: req.body.product_number,
        },
    });
    if (!productData) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.productPRError.PRODUCT_NOT_FOUND);
    }
    const catalogData = await dropshipperCatalog.findOne({
        where: {
            product_id: productData.dataValues.product_id,
            dropshipper_id: dropshipperData.dropshipper_id,
        },
        raw: true,
    });
    if (catalogData) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.productPRError.PRODUCT_CATALOG_ALREADY_EXIST);
    }
    if (req.body.price < productData.dataValues.final_price) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.productPRError.THE_PRICE_IS_VERY_LOW);
    }
    // const totalQuantity = _.sumBy(productData.variant, (x) => x.dataValues.remaining_quantity);
    // console.log(totalQuantity)
    // if (productData.dataValues.remaining_quantity <= 0 && totalQuantity <= 0) {
    //     const response = new Response(400, errorCodes.productPRError.PRODUCT_OUT_OF_STOCK);
    //     return response.failResponse(req, res);
    // }
    // if (productData.status === 'inactive') {
    //     const response = new Response(400, errorCodes.productPRError.PRODUCT_INACTIVE);
    //     return response.failResponse(req, res);
    // }
    // if (productData.is_disabled) {
    //     const response = new Response(400, errorCodes.productPRError.PRODUCT_DISABLED);
    //     return response.failResponse(req, res);
    // }
    // if (productData.is_coming_soon) {
    //     const response = new Response(400, errorCodes.productPRError.PRODUCT_AVAILABLE_SOON);
    //     return response.failResponse(req, res);
    // }
    return dropshipperCatalog.create({
        dropshipper_id: dropshipperData.dropshipper_id,
        product_id: productData.dataValues.product_id,
        price: req.body.price,
        discount: 0,
        final_price: req.body.price,
        is_disabled: productData.dataValues.is_disabled,
        is_coming_soon: productData.dataValues.is_coming_soon,
        status: productData.dataValues.status,
    }, {
        transaction: t,
    }).then(async (result) => {
        await t.commit();
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch(async (e) => {
        await t.rollback();
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

async function updateProductCatalog(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const { dropshipperData } = req;

    const t = await sequelize.transaction();

    const catalogData = await dropshipperCatalog.findOne({
        where: {
            dropshipper_catalog_id: req.params.productCatalogId,
            dropshipper_id: dropshipperData.dropshipper_id,
        },
    });
    if (!catalogData) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.productPRError.PRODUCT_CATALOG_NOT_FOUND);
    }

    const productData = await Product.scope([
        { method: ['includeProductGaleryApp', productGalery] },
        { method: ['includeBrandApp', Brand] },
        { method: ['includeCategoryApp', Category] },
        { method: ['includeVariantApp', productVariant] },
    ]).findOne({
        where: {
            product_id: catalogData.product_id,
        },
    });
    if (!productData) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.productPRError.PRODUCT_NOT_FOUND);
    }
    if (req.body.price < productData.dataValues.final_price) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.productPRError.THE_PRICE_IS_VERY_LOW);
    }
    // const totalQuantity = _.sumBy(productData.variant, (x) => x.dataValues.remaining_quantity);
    // console.log(totalQuantity);
    // if (productData.dataValues.remaining_quantity <= 0 && totalQuantity <= 0) {
    //     const response = new Response(400, errorCodes.productPRError.PRODUCT_OUT_OF_STOCK);
    //     return response.failResponse(req, res);
    // }
    // if (productData.status === 'inactive') {
    //     const response = new Response(400, errorCodes.productPRError.PRODUCT_INACTIVE);
    //     return response.failResponse(req, res);
    // }
    // if (productData.is_disabled) {
    //     const response = new Response(400, errorCodes.productPRError.PRODUCT_DISABLED);
    //     return response.failResponse(req, res);
    // }
    // if (productData.is_coming_soon) {
    //     const response = new Response(400, errorCodes.productPRError.PRODUCT_AVAILABLE_SOON);
    //     return response.failResponse(req, res);
    // }
    let productPrice = req.body.price || catalogData.price;
    // eslint-disable-next-line max-len
    const maxDiscount = Math.floor((((productPrice - productData.final_price) / productPrice) * 100));
    if (req.body.discount > maxDiscount) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.productPRError.DISCOUNT_IS_VERY_HIGH);
    }
    const discountPrice = productPrice * (Number(req.body.discount) / 100);
    if (req.body.discount >= 0 && req.body.discount <= maxDiscount) {
        productPrice -= discountPrice;
    }
    if (req.body.discount === 0) {
        productPrice = catalogData.price;
    }
    return catalogData.update({
        final_price: productPrice,
        discount: req.body.discount,
        price: req.body.price,
    }, {
        transaction: t,
    }).then(async (result) => {
        await t.commit();
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch(async (e) => {
        await t.rollback();
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

async function removeProductCatalog(req, res) {
    const { dropshipperData } = req;
    const catalogData = await dropshipperCatalog.findOne({
        where: {
            dropshipper_catalog_id: req.params.productCatalogId,
            dropshipper_id: dropshipperData.dropshipper_id,
        },
    });
    if (!catalogData) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.productPRError.PRODUCT_CATALOG_NOT_FOUND);
    }

    return catalogData.destroy()
        .then(() => {
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function productCatalogDetail(req, res) {
    const { dropshipperData } = req;
    return dropshipperCatalog
        .scope([
            { method: ['includeProductDetailApp', req.useragent.platform.toLowerCase(), Product, Brand, Category, productGalery, productVariant] },
        ])
        .findOne({
            where: {
                dropshipper_catalog_id: req.params.productCatalogId,
                dropshipper_id: dropshipperData.dropshipper_id,
            },
        }).then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.productPRError.PRODUCT_NOT_FOUND);
            }
            if (result.dataValues.product.variant && result.dataValues.product.variant.length > 0) {
                const quantitySum = _.sumBy(result.product.variant, (x) => x.dataValues.quantity);
                // eslint-disable-next-line max-len
                const remainingQuantitySum = _.sumBy(result.product.variant, (x) => x.dataValues.remaining_quantity);
                Object.assign(result.dataValues, {
                    total_quantity: quantitySum,
                    total_remaining_quantity: remainingQuantitySum,
                });
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        }).catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function listProductCatalog(req, res) {
    const { dropshipperData } = req;
    const ordering = simpleOrdering(req, 'dropshipper_catalog_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
            dropshipper_id: dropshipperData.dropshipper_id,
        },
        include: [],
        distinct: true,
    };
    if (req.query.search) {
        Object.assign(option.where, {
            '$product.name$': {
                [Sequelize.Op.iLike]: `%${req.query.search}%`,
            },
        });
    }
    if (req.query.category_id) {
        Object.assign(option.where, {
            '$product.category_id$': req.query.category_id,
        });
    }
    if (req.query.status) {
        Object.assign(option.where, {
            '$product.status$': req.query.status,
        });
    }
    if (req.query.is_disabled) {
        Object.assign(option.where, {
            '$product.is_disabled$': req.query.is_disabled,
        });
    }
    if (req.query.coming_soon) {
        Object.assign(option.where, {
            '$product.coming_soon$': req.query.coming_soon,
        });
    }
    if (req.query.start_price && req.query.end_price) {
        Object.assign(option.where, {
            '$product.final_price$': {
                [Sequelize.Op.gte]: req.query.start_price,
                [Sequelize.Op.lte]: req.query.end_price,
            },
        });
    }
    return dropshipperCatalog.scope([
        { method: ['ordering', ordering] },
        { method: ['pagination', req.query.pagination, pagination] },
        { method: ['includeProductListApp', req.useragent.platform.toLowerCase(), Product, Brand, Category, productGalery] },
    ]).findAndCountAll(option).then((result) => {
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

async function catalogMaxDiscount(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const { dropshipperData } = req;

    const catalogData = await dropshipperCatalog.findOne({
        where: {
            dropshipper_catalog_id: req.params.productCatalogId,
            dropshipper_id: dropshipperData.dropshipper_id,
        },
    });
    if (!catalogData) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.productPRError.PRODUCT_CATALOG_NOT_FOUND);
    }

    const productData = await Product.scope([
        { method: ['includeProductGaleryApp', productGalery] },
        { method: ['includeBrandApp', Brand] },
        { method: ['includeCategoryApp', Category] },
        { method: ['includeVariantApp', productVariant] },
    ]).findOne({
        where: {
            product_id: catalogData.product_id,
        },
    });
    if (!productData) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.productPRError.PRODUCT_NOT_FOUND);
    }
    if (req.body.price < productData.dataValues.final_price) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.productPRError.THE_PRICE_IS_VERY_LOW);
    }

    const productPrice = catalogData.price;
    // eslint-disable-next-line max-len
    const maxDiscount = Math.floor((((productPrice - productData.final_price) / productPrice) * 100));
    const response = new Response(req, res);
    return response.successResponse(200, {
        max_discount: maxDiscount,
    });
}

export default {
    addToCatalog,
    updateProductCatalog,
    removeProductCatalog,
    productCatalogDetail,
    listProductCatalog,
    catalogMaxDiscount,
};
