import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Request from '../../misc/request';
import Response from '../../response/response';


const Sequelize = require('sequelize');

const paymentMethod = db.payment_method;
// const _ = require('lodash');
// const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

const requestFunction = new Request();


function load(req, res, next, id) {
    paymentMethod
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404,
                    errorCodes.paymentMethodPMError.PAYMENT_METHOD_NOT_FOUND);
            }
            req.paymentMethodData = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function get(req, res) {
    const { paymentMethodData } = req;
    const response = new Response(req, res);
    return response.successResponse(200, paymentMethodData);
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'payment_method_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
            os: {
                [Sequelize.Op.overlap]: [req.useragent.platform.toLowerCase()],
            },
        },
        include: [],
        distinct: true,
    };
    if (req.query.name) {
        Object.assign(option.where, {
            name: {
                [Sequelize.Op.iLike]: `%${req.query.name}%`,
            },
        });
    }
    if (req.query.payment_method_category) {
        Object.assign(option.where, {
            payment_method_category: {
                [Sequelize.Op.iLike]: `%${req.query.payment_method_category}%`,
            },
        });
    }
    if (req.query.code) {
        Object.assign(option.where, {
            code: req.query.code,
        });
    }
    return paymentMethod
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function getPaymentChannels(req, res) {
    requestFunction.xenditRequest(`/${process.env.PAYMENT_CHANNEL}`, {
        method: 'GET',
        json: true,
    }).then((result) => {
        const response = new Response(req, res);
        return response.successResponse(200, result.body);
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

export default {
    load,
    get,
    list,
    getPaymentChannels,
};
