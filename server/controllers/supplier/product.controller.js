// import moment from 'moment';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Product = db.product;
const productGalery = db.product_galery;
const Brand = db.brand;
const Category = db.category;
const Sequelize = require('sequelize');
// const _ = require('lodash');
// const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');


function get(req, res) {
    const { supplierData } = req;
    if (!req.params.productId) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.productPRError.PRODUCT_ID_REQUIRED);
    }
    const option = {
        where: {
        },
        attributes: {
            exclude: ['markup_percentage', 'final_price'],
        },
        include: [],
        distinct: true,
    };
    option.where.supplier_id = supplierData.supplier_id;
    option.where.product_id = req.params.productId;
    return Product
        .scope([
            { method: ['includeProductGaleryApp', req.query.product_galery_id, productGalery] },
            { method: ['includeBrandApp', req.query.brand_id, Brand] },
            { method: ['includeCategoryApp', req.query.category_id, Category] },
        ])
        .findOne(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.productPRError.PRODUCT_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function list(req, res) {
    const { supplierData } = req;
    const ordering = simpleOrdering(req, 'product_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
        },
        attributes: {
            exclude: ['markup_percentage', 'final_price'],
        },
        include: [],
        distinct: true,
    };
    option.where.supplier_id = supplierData.supplier_id;
    if (req.query.search) {
        Object.assign(option.where, {
            [Sequelize.Op.or]: [{
                name: {
                    [Sequelize.Op.iLike]: `%${req.query.search}%`,
                },
            }, {
                product_number: {
                    [Sequelize.Op.iLike]: `%${req.query.search}%`,
                },
            }, {
                sku: {
                    [Sequelize.Op.iLike]: `%${req.query.search}%`,
                },
            }],
        });
    }
    if (req.query.status) {
        option.where.status = req.query.status;
    }
    if (req.query.is_out_of_stock === 'true') {
        Object.assign(option.where, {
            remaining_quantity: 0,
        });
    }
    return Product
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
            { method: ['includeProductGaleryApp', req.query.product_galery_id, productGalery] },
            { method: ['includeBrandApp', req.query.brand_id, Brand] },
            { method: ['includeCategoryApp', req.query.category_id, Category] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function stats(req, res) {
    const { supplierData } = req;
    const allProductCount = await Product.count({
        where: {
            supplier_id: supplierData.supplier_id,
        },
        raw: true,
    });
    const availableProductCount = await Product.count({
        where: {
            supplier_id: supplierData.supplier_id,
            remaining_quantity: {
                [Sequelize.Op.gt]: 0,
            },
            status: 'active',
        },
        raw: true,
    });
    const outOfStockProductCount = await Product.count({
        where: {
            supplier_id: supplierData.supplier_id,
            remaining_quantity: 0,
        },
        raw: true,
    });

    const response = new Response(req, res);
    return response.successResponse(200, {
        all_product_count: allProductCount,
        available_product_count: availableProductCount,
        out_of_stock_product_count: outOfStockProductCount,
    });
}

export default {
    get,
    list,
    stats,
};
