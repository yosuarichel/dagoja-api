/* eslint-disable max-len */
import moment from 'moment';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import helper from '../../misc/helper';
import Response from '../../response/response';


const Sequelize = require('sequelize');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const Supplier = db.supplier;
const supplierSession = db.supplier_session;
const businessInformation = db.business_information;
const ownerInformation = db.owner_information;
const supplierLog = db.supplier_log;
const _ = require('lodash');
const { validationResult } = require('express-validator');
// const { simpleOrdering, simplePagination } = require('../../misc/misc');

const registerSupplier = async (req, res) => {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        if (req.fileValidationError.length > 0) {
            req.fileValidationError.map((x) => errors.errors.push(x));
        }
    }
    if (!errors.isEmpty()) {
        if (!_.isEmpty(req.files)) {
            if (req.files.supplier_image) {
                fs.unlinkSync(req.files.supplier_image[0].path);
            }
            if (req.files.business_siup_image) {
                fs.unlinkSync(req.files.business_siup_image[0].path);
            }
            if (req.files.business_npwp_image) {
                fs.unlinkSync(req.files.business_npwp_image[0].path);
            }
            if (req.files.owner_nik_image) {
                fs.unlinkSync(req.files.owner_nik_image[0].path);
            }
            if (req.files.owner_npwp_image) {
                fs.unlinkSync(req.files.owner_npwp_image[0].path);
            }
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const supplierCount = await Supplier.count({
        where: {
            [Sequelize.Op.or]: [{
                phone_number: req.body.account_phone_number,
            }, {
                email: req.body.account_email,
            }],
        },
        raw: true,
    });
    if (supplierCount > 0) {
        if (!_.isEmpty(req.files)) {
            if (req.files.supplier_image) {
                fs.unlinkSync(req.files.supplier_image[0].path);
            }
            if (req.files.business_siup_image) {
                fs.unlinkSync(req.files.business_siup_image[0].path);
            }
            if (req.files.business_npwp_image) {
                fs.unlinkSync(req.files.business_npwp_image[0].path);
            }
            if (req.files.owner_nik_image) {
                fs.unlinkSync(req.files.owner_nik_image[0].path);
            }
            if (req.files.owner_npwp_image) {
                fs.unlinkSync(req.files.owner_npwp_image[0].path);
            }
        }
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.supplierSPError.SUPPLIER_ALREADY_EXIST);
    }
    const passHashed = crypto.createHash('sha256')
        .update(req.body.account_password)
        .digest('hex');

    let uploadProfileImage = null;
    let uploadBusinessSIUPImage = null;
    let uploadBusinessNPWPImage = null;
    let uploadOwnerNIKImage = null;
    let uploadOwnerNPWPImage = null;
    if (!_.isEmpty(req.files)) {
        if (req.files.supplier_image) {
            const filename = req.files.supplier_image[0].filename.split('.')[0];
            uploadProfileImage = await helper.uploadCloudinary(req.files.supplier_image[0].path, filename, 'supplier');
            if (!uploadProfileImage) {
                fs.unlinkSync(req.files.supplier_image[0].path);
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
            }
            req.body.supplier_image_name = uploadProfileImage.public_id;
            req.body.supplier_image_source = uploadProfileImage.secure_url;
        }
        if (req.files.business_siup_image) {
            const filename = req.files.business_siup_image[0].filename.split('.')[0];
            uploadBusinessSIUPImage = await helper.uploadCloudinary(req.files.business_siup_image[0].path, filename, 'business-siup');
            if (!uploadBusinessSIUPImage) {
                fs.unlinkSync(req.files.business_siup_image[0].path);
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
            }
            req.body.business_siup_image_name = uploadBusinessSIUPImage.public_id;
            req.body.business_siup_image_source = uploadBusinessSIUPImage.secure_url;
        }
        if (req.files.business_npwp_image) {
            const filename = req.files.business_npwp_image[0].filename.split('.')[0];
            uploadBusinessNPWPImage = await helper.uploadCloudinary(req.files.business_npwp_image[0].path, filename, 'business-npwp');
            if (!uploadBusinessNPWPImage) {
                fs.unlinkSync(req.files.business_npwp_image[0].path);
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
            }
            req.body.business_npwp_image_name = uploadBusinessNPWPImage.public_id;
            req.body.business_npwp_image_source = uploadBusinessNPWPImage.secure_url;
        }
        if (req.files.owner_nik_image) {
            const filename = req.files.owner_nik_image[0].filename.split('.')[0];
            uploadOwnerNIKImage = await helper.uploadCloudinary(req.files.owner_nik_image[0].path, filename, 'owner-nik');
            if (!uploadOwnerNIKImage) {
                fs.unlinkSync(req.files.owner_nik_image[0].path);
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
            }
            req.body.owner_nik_image_name = uploadOwnerNIKImage.public_id;
            req.body.owner_nik_image_source = uploadOwnerNIKImage.secure_url;
        }
        if (req.files.owner_npwp_image) {
            const filename = req.files.owner_npwp_image[0].filename.split('.')[0];
            uploadOwnerNPWPImage = await helper.uploadCloudinary(req.files.owner_npwp_image[0].path, filename, 'owner-npwp');
            if (!uploadOwnerNPWPImage) {
                fs.unlinkSync(req.files.owner_npwp_image[0].path);
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
            }
            req.body.owner_npwp_image_name = uploadOwnerNPWPImage.public_id;
            req.body.owner_npwp_image_source = uploadOwnerNPWPImage.secure_url;
        }
    }

    req.body.is_email_verif = false;
    req.body.is_phone_number_verif = false;
    req.body.password = passHashed;
    req.body.account_status = 'wait-approval';
    const accountPayload = {
        email: req.body.account_email,
        phone_number: req.body.account_phone_number,
        is_email_verif: false,
        is_phone_number_verif: false,
        password: passHashed,
        status: 'wait-approval',
        supplier_image_name: req.body.supplier_image_name || null,
        supplier_image_source: req.body.supplier_image_source || null,
    };
    const businessPayload = {
        name: req.body.business_name,
        siup_number: req.body.business_siup_number,
        npwp_number: req.body.business_npwp_number,
        status: 'wait-verification',
        siup_image_name: req.body.business_siup_image_name || null,
        siup_image_source: req.body.business_siup_image_source || null,
        npwp_image_name: req.body.business_npwp_image_name || null,
        npwp_image_source: req.body.business_npwp_image_source || null,
        province: req.body.business_province,
        city: req.body.business_city,
        district: req.body.business_district,
        village: req.body.business_village,
        postal_code: req.body.business_postal_code,
        address: req.body.business_address,
    };
    const ownerPayload = {
        name: req.body.owner_name,
        occupation_id: req.body.owner_occupation_id,
        nik_number: req.body.owner_nik_number,
        npwp_number: req.body.owner_npwp_number,
        status: 'wait-verification',
        nik_image_name: req.body.owner_nik_image_name || null,
        nik_image_source: req.body.owner_nik_image_source || null,
        npwp_image_name: req.body.owner_npwp_image_name || null,
        npwp_image_source: req.body.owner_npwp_image_source || null,
        province: req.body.owner_province,
        city: req.body.owner_city,
        district: req.body.owner_district,
        village: req.body.owner_village,
        postal_code: req.body.owner_postal_code,
        address: req.body.owner_address,
        bank_id: req.body.owner_bank_id,
        bank_account: req.body.owner_bank_account,
    };

    return Supplier.create(accountPayload)
        .then(async (result) => {
            const data = result.dataValues;
            const supplierNumber = `SPL-${result.supplier_id}-${Date.now()}`;
            await Supplier.update({
                supplier_number: supplierNumber,
            }, {
                where: {
                    supplier_id: result.supplier_id,
                },
            });
            await supplierLog.create({
                supplier_id: data.supplier_id,
                type: 'authentication',
                action: 'Registration account',
            });
            businessPayload.supplier_id = data.supplier_id;
            await businessInformation.create(businessPayload);

            ownerPayload.supplier_id = data.supplier_id;
            await ownerInformation.create(ownerPayload);

            Object.assign(data, {
                supplier_number: supplierNumber,
            });
            if (!_.isEmpty(req.files)) {
                if (req.files.supplier_image) {
                    fs.unlinkSync(req.files.supplier_image[0].path);
                }
                if (req.files.business_siup_image) {
                    fs.unlinkSync(req.files.business_siup_image[0].path);
                }
                if (req.files.business_npwp_image) {
                    fs.unlinkSync(req.files.business_npwp_image[0].path);
                }
                if (req.files.owner_nik_image) {
                    fs.unlinkSync(req.files.owner_nik_image[0].path);
                }
                if (req.files.owner_npwp_image) {
                    fs.unlinkSync(req.files.owner_npwp_image[0].path);
                }
            }
            delete data.password;
            delete data.supplier_id;
            const response = new Response(req, res);
            return response.successResponse(200, {
                supplier_number: data.supplier_number,
                email: data.email,
                phone_number: data.phone_number,
                status: data.status,
            });
        })
        .catch(async (e) => {
            if (!_.isEmpty(req.files)) {
                if (req.files.supplier_image) {
                    await helper.removeCloudinary(uploadProfileImage.public_id);
                    fs.unlinkSync(req.files.supplier_image[0].path);
                }
                if (req.files.business_siup_image) {
                    await helper.removeCloudinary(uploadBusinessSIUPImage.public_id);
                    fs.unlinkSync(req.files.business_siup_image[0].path);
                }
                if (req.files.business_npwp_image) {
                    await helper.removeCloudinary(uploadBusinessNPWPImage.public_id);
                    fs.unlinkSync(req.files.business_npwp_image[0].path);
                }
                if (req.files.owner_nik_image) {
                    await helper.removeCloudinary(uploadOwnerNIKImage.public_id);
                    fs.unlinkSync(req.files.owner_nik_image[0].path);
                }
                if (req.files.owner_npwp_image) {
                    await helper.removeCloudinary(uploadOwnerNPWPImage.public_id);
                    fs.unlinkSync(req.files.owner_npwp_image[0].path);
                }
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

const loginSupplier = async (req, res) => {
    const { settingData } = req;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const supplierData = await Supplier.findOne({
        where: {
            email: req.body.email,
        },
        raw: true,
    });
    if (!supplierData) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.authAUError.INVALID_CREDENTIAL);
    }

    const passHashed = crypto.createHash('sha256')
        .update(req.body.password)
        .digest('hex');
    if (supplierData.password !== passHashed) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.authAUError.INVALID_CREDENTIAL);
    }
    if (supplierData.status === 'suspend') {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.authAUError.ACCOUNT_SUSPENDED);
    }
    if (supplierData.status === 'inactive') {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.authAUError.INACTIVE_ACCOUNT);
    }
    if (supplierData.status === 'wait-approval') {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.authAUError.WAIT_APPROVAL);
    }

    const sessionHash = crypto.createHash('sha256')
        .update(`Supplier${supplierData.supplier_id}${supplierData.username}${Date.now()}`)
        .digest('hex');
    const now = moment(Date.now()).tz('Asia/Jakarta').format();
    const split = settingData.supplier_session_expiry.split(';');
    const valueExp = split[0];
    const intervalExp = split[1];
    const sessionExpiry = moment(now).add(valueExp, `${intervalExp}`).tz('Asia/Jakarta');
    const diff = sessionExpiry.diff(now) / 1000;

    const supplierSessionData = await supplierSession.findOne({
        where: {
            supplier_id: supplierData.supplier_id,
        },
        raw: true,
    });
    if (supplierSessionData) {
        await supplierSession.update({
            session: sessionHash,
            expiry_value: diff,
            expiry_date: moment(sessionExpiry).tz('Asia/Jakarta').format(),
            status: 'logged-in',
        }, {
            where: {
                supplier_id: supplierData.supplier_id,
            },
        });
    }
    if (!supplierSessionData) {
        await supplierSession.create({
            supplier_id: supplierData.supplier_id,
            session: sessionHash,
            expire_value: diff,
            expired_at: moment(sessionExpiry).tz('Asia/Jakarta').format(),
            status: 'logged-in',
        });
    }

    const token = jwt.sign({
        type: 'supplier',
        name: supplierData.name,
        email: supplierData.email,
        username: supplierData.username,
        session: sessionHash,
        session_expiry: moment(sessionExpiry).tz('Asia/Jakarta').format(),
        status: supplierData.status,
    }, process.env.JWT_SECRET, { expiresIn: diff });
    res.cookie('accessToken', token, { maxAge: diff * 1000 });
    const response = new Response(req, res);
    return response.successResponse(200, {
        email: supplierData.email,
        session: sessionHash,
        session_expiry: moment(sessionExpiry).tz('Asia/Jakarta').format(),
        status: supplierData.status,
        token,
    });
};

const logoutSupplier = async (req, res) => {
    const { supplierData } = req;
    return supplierSession.update({
        status: 'logged-out',
    }, {
        where: {
            supplier_id: supplierData.supplier_id,
            session: supplierData.session,
        },
    }).then(() => {
        const response = new Response(req, res);
        return response.successResponse(200, 'Logged Out');
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
};


export default {
    registerSupplier,
    loginSupplier,
    logoutSupplier,
};
