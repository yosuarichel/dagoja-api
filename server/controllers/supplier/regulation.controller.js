/* eslint-disable max-len */
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Regulation = db.regulation;
// const _ = require('lodash');
const Sequelize = require('sequelize');
// const { validationResult } = require('express-validator');
// const { simpleOrdering, simplePagination } = require('../../misc/misc');


function list(req, res) {
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    if (req.params.type) {
        option.where.type = {
            [Sequelize.Op.iLike]: `%${req.params.type}%`,
        };
    }
    return Regulation
        .findOne(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.regulationRGError.REGULATION_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

export default {
    list,
};
