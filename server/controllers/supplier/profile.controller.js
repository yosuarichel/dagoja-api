/* eslint-disable max-len */
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Supplier = db.supplier;
const businessInformation = db.business_information;
const ownerInformation = db.owner_information;
// const _ = require('lodash');
// const Sequelize = require('sequelize');
// const { validationResult } = require('express-validator');
// const { simpleOrdering, simplePagination } = require('../../misc/misc');


function profileAccount(req, res) {
    const { supplierData } = req;
    return Supplier
        .findOne({
            where: {
                supplier_id: supplierData.supplier_id,
            },
            attributes: ['email', 'phone_number', 'supplier_number', 'status'],
            raw: true,
        })
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.supplierSPError.SUPPLIER_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function profileBusiness(req, res) {
    const { supplierData } = req;
    return businessInformation
        .findOne({
            where: {
                supplier_id: supplierData.supplier_id,
            },
            attributes: {
                exclude: ['created_at', 'updated_at', 'deleted_at', 'business_information_id'],
            },
            raw: true,
        })
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.businessInformationBIError.BUSINESS_INFO_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function profileOwner(req, res) {
    const { supplierData } = req;
    return ownerInformation
        .findOne({
            where: {
                supplier_id: supplierData.supplier_id,
            },
            attributes: {
                exclude: ['created_at', 'updated_at', 'deleted_at', 'owner_information_id'],
            },
            raw: true,
        })
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.ownerInformationOIError.OWNER_INFO_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function status(req, res) {
    const { supplierData } = req;
    const responseObj = {
        profile_account_status: null,
        profile_business_status: null,
        profile_owner_status: null,
    };
    const profileData = await Supplier
        .findOne({
            where: {
                supplier_id: supplierData.supplier_id,
            },
            attributes: ['email', 'phone_number', 'supplier_number', 'status'],
            raw: true,
        });
    if (!profileData) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.supplierSPError.SUPPLIER_NOT_FOUND);
    }
    responseObj.profile_account_status = profileData.status;

    const businessData = await businessInformation
        .findOne({
            where: {
                supplier_id: supplierData.supplier_id,
            },
            attributes: {
                exclude: ['created_at', 'updated_at', 'deleted_at', 'business_information_id'],
            },
            raw: true,
        });
    if (!businessData) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.businessInformationBIError.BUSINESS_INFO_NOT_FOUND);
    }
    responseObj.profile_business_status = businessData.status;

    const ownerData = await ownerInformation
        .findOne({
            where: {
                supplier_id: supplierData.supplier_id,
            },
            attributes: {
                exclude: ['created_at', 'updated_at', 'deleted_at', 'owner_information_id'],
            },
            raw: true,
        });
    if (!ownerData) {
        const response = new Response(req, res);
        return response.failResponse(404, errorCodes.ownerInformationOIError.OWNER_INFO_NOT_FOUND);
    }
    responseObj.profile_owner_status = ownerData.status;

    const response = new Response(req, res);
    return response.successResponse(200, responseObj);
}

export default {
    profileAccount,
    profileBusiness,
    profileOwner,
    status,
};
