/* eslint-disable max-len */
import moment from 'moment';

import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const supplierTransaction = db.supplier_transaction;
const supplierTransactionLog = db.supplier_transaction_log;
// const _ = require('lodash');
const Sequelize = require('sequelize');
// const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');


async function balance(req, res) {
    const { supplierData } = req;
    let plusBalance = 0;
    let minBalance = 0;
    try {
        const plusBalanceData = await supplierTransaction.findOne({
            where: {
                supplier_id: supplierData.supplier_id,
                op: 'plus',
            },
            attributes: ['supplier_id', [Sequelize.fn('sum', Sequelize.col('transaction')), 'balance']],
            group: ['supplier_id'],
            raw: true,
            order: Sequelize.literal('balance DESC'),
        });
        if (plusBalanceData) {
            plusBalance = plusBalanceData.balance;
        }
        const minBalanceData = await supplierTransaction.findOne({
            where: {
                supplier_id: supplierData.supplier_id,
                op: 'min',
            },
            attributes: ['supplier_id', [Sequelize.fn('sum', Sequelize.col('transaction')), 'balance']],
            group: ['supplier_id'],
            raw: true,
            order: Sequelize.literal('balance DESC'),
        });
        if (minBalanceData) {
            minBalance = minBalanceData.balance;
        }
    } catch (e) {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    }


    const finaleBalance = Number(plusBalance) - Number(minBalance);

    const response = new Response(req, res);
    return response.successResponse(200, {
        balance: finaleBalance,
    });
}

async function history(req, res) {
    const { supplierData } = req;

    const ordering = simpleOrdering(req, 'supplier_transaction_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    option.where.supplier_id = supplierData.supplier_id;
    if (req.query.search) {
        Object.assign(option.where, {
            [Sequelize.Op.or]: [{
                transaction_number: {
                    [Sequelize.Op.iLike]: `%${req.query.search}%`,
                },
            }, {
                description: {
                    [Sequelize.Op.iLike]: `%${req.query.search}%`,
                },
            }],
        });
    }
    if (req.query.start_date && req.query.end_date) {
        option.where.settlement_at = {
            [Sequelize.Op.gte]: moment(req.query.start_date).tz('Asia/Jakarta').startOf('day').format(),
            [Sequelize.Op.lte]: moment(req.query.end_date).tz('Asia/Jakarta').endOf('day').format(),
        };
    }
    return supplierTransaction
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function historyDetail(req, res) {
    const { supplierData } = req;

    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    option.where.supplier_id = supplierData.supplier_id;
    option.where.supplier_transaction_id = req.params.transactionId;
    return supplierTransaction
        .scope([
            { method: ['includeSupplierTransactionLogApp', supplierTransactionLog] },
        ])
        .findOne(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.transactionTRError.TRANSACTION_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

export default {
    balance,
    history,
    historyDetail,
};
