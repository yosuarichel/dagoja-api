/* eslint-disable max-len */
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';

const affilialBonus = db.affilial_bonus;
const _ = require('lodash');
const Sequelize = require('sequelize');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    affilialBonus
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.affilialBonusABError.AFFILIAL_BONUS_NOT_FOUND);
            }
            req.affilialBonusData = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const response = new Response(req, res);
    return response.successResponse(200, req.affilialBonusData);
}

const create = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const affilialBonusCount = await affilialBonus.count({
        where: {
            name: req.body.name,
        },
        raw: true,
    });
    if (affilialBonusCount > 0) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.affilialBonusABError.AFFILIAL_BONUS_ALREADY_EXIST);
    }

    return affilialBonus.create(req.body)
        .then(async (result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

async function update(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    if (req.body.name) {
        const affilialBonusCount = await affilialBonus.count({
            where: {
                affilial_bonus_id: {
                    [Sequelize.Op.notIn]: req.affilialBonusData.affilial_bonus_id,
                },
                name: req.body.name,
            },
            raw: true,
        });
        if (affilialBonusCount > 0) {
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.affilialBonusABError.TARGET_BONUS_ALREADY_EXIST);
        }
    }

    return req.affilialBonusData.update(req.body)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'affilial_bonus_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    return affilialBonus
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.affilialBonusABError.AFFILIAL_BONUS_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function remove(req, res) {
    const { affilialBonusData } = req;
    return affilialBonusData.destroy()
        .then(() => {
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

export default {
    load,
    get,
    create,
    update,
    list,
    remove,
};
