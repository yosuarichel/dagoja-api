/* eslint-disable max-len */
import Sequelize from 'sequelize';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import helper from '../../misc/helper';
import Response from '../../response/response';

const fs = require('fs');

const Category = db.category;
const _ = require('lodash');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    Category
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.categoryCTError.CATEGORY_NOT_FOUND);
            }
            req.category = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const response = new Response(req, res);
    return response.successResponse(200, req.category);
}

const create = async (req, res) => {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        errors.errors.push(req.fileValidationError);
    }
    if (!errors.isEmpty()) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const categoryData = await Category.count({
        where: {
            name: {
                [Sequelize.Op.iLike]: `%${req.body.name}%`,
            },
        },
        raw: true,
    });
    if (categoryData > 0) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.categoryCTError.CATEGORY_ALREADY_EXIST);
    }

    let upload = null;
    if (req.file) {
        const filename = req.file.filename.split('.')[0];
        upload = await helper.uploadCloudinary(req.file.path, filename, 'category');
        if (!upload) {
            fs.unlinkSync(req.file.path);
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
        }
        req.body.image_name = upload.public_id;
        req.body.image_source = upload.secure_url;
    }

    return Category.create(req.body)
        .then(async (result) => {
            if (req.file) {
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            if (req.file) {
                await helper.removeCloudinary(upload.public_id);
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

async function update(req, res) {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        errors.errors.push(req.fileValidationError);
    }
    if (!errors.isEmpty()) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const categoryData = await Category.count({
        where: {
            category_id: {
                [Sequelize.Op.not]: req.params.categoryId,
            },
            name: {
                [Sequelize.Op.iLike]: `%${req.body.name}%`,
            },
        },
        raw: true,
    });
    if (categoryData > 0) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.categoryCTError.CATEGORY_ALREADY_EXIST);
    }

    let upload = null;
    if (req.file) {
        const filename = req.file.filename.split('.')[0];
        if (req.category.image_name) {
            const removeImage = await helper.removeCloudinary(req.category.image_name);
            if (!removeImage) {
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_REMOVE_FILE_FROM_CDN);
            }
        }
        upload = await helper.uploadCloudinary(req.file.path, filename, 'category');
        if (!upload) {
            fs.unlinkSync(req.file.path);
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
        }

        req.body.image_name = upload.public_id;
        req.body.image_source = upload.secure_url;
    }

    return req.category.update(req.body)
        .then((result) => {
            if (req.file) {
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            if (req.file) {
                await helper.removeCloudinary(upload.public_id);
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'category_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    return Category
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.categoryCTError.CATEGORY_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function remove(req, res) {
    const { category } = req;
    if (category.image_name) {
        const removeImage = await helper.removeCloudinary(category.image_name);
        if (!removeImage) {
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_REMOVE_FILE_FROM_CDN);
        }
    }
    return category.destroy()
        .then(() => {
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

export default {
    load,
    get,
    create,
    update,
    list,
    remove,
};
