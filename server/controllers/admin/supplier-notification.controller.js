/* eslint-disable max-len */
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import helper from '../../misc/helper';
import Response from '../../response/response';


const fs = require('fs');

const supplierNotification = db.supplier_notification;
const supplierNotificationParticipant = db.supplier_notification_participant;
const Supplier = db.supplier;
const _ = require('lodash');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    supplierNotification
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404,
                    errorCodes.supplierNotificationSNError.SUPPLIER_NOTIFICATION_NOT_FOUND);
            }
            req.supplierNotificationData = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function broadcastNotification(req, res) {
    let notificationData = null;
    try {
        const getNotification = await supplierNotification.findOne({
            where: {
                supplier_notification_id: req.body.notification_id,
            },
            raw: true,
        });
        if (getNotification) {
            notificationData = getNotification;
        }
    } catch (e) {
        console.log('error get notif data', e);
    }
    if (!notificationData) {
        console.log('not found');
    }

    let supplierIds = [];
    if (notificationData.is_global) {
        try {
            const getSupplier = await Supplier.findAll({
                where: {
                    status: 'active',
                },
                raw: true,
                attributes: ['supplier_id'],
            });
            if (getSupplier) {
                supplierIds = getSupplier;
            }
        } catch (e) {
            console.log('error get user data', e);
        }
    }

    // setInterval(async () => {
    //     // eslint-disable-next-line no-plusplus
    //     for (let i = 0; i < 100000; i++) {
    //         supplierNotificationParticipant.create({
    //             supplier_id: i,
    //             is_read: false,
    //             is_remove: false,
    //         });
    //     }
    // }, 5 * 1000);
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < 100000; i++) {
        supplierNotificationParticipant.create({
            supplier_id: i,
            is_read: false,
            is_remove: false,
        });
    }

    console.log('done');
}

function get(req, res) {
    const response = new Response(req, res);
    return response.successResponse(200, req.supplierNotificationData);
}

const create = async (req, res) => {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        if (req.fileValidationError.length > 0) {
            req.fileValidationError.map((x) => errors.errors.push(x));
        }
    }
    if (!errors.isEmpty()) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    let upload = null;
    if (req.file) {
        const filename = req.file.filename.split('.')[0];
        upload = await helper.uploadCloudinary(req.file.path, filename, 'supplier-notification');
        if (!upload) {
            fs.unlinkSync(req.file.path);
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
        }
        req.body.image_name = upload.public_id;
        req.body.image_source = upload.secure_url;
    }

    return supplierNotification.create(req.body)
        .then(async (result) => {
            if (req.file) {
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            if (req.file) {
                await helper.removeCloudinary(upload.public_id);
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

async function update(req, res) {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        if (req.fileValidationError.length > 0) {
            req.fileValidationError.map((x) => errors.errors.push(x));
        }
    }
    if (!errors.isEmpty()) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    let upload = null;
    if (req.file) {
        const filename = req.file.filename.split('.')[0];
        if (req.supplierNotificationData.image_name) {
            const removeImage = await helper.removeCloudinary(
                req.supplierNotificationData.image_name,
            );
            if (!removeImage) {
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_REMOVE_FILE_FROM_CDN);
            }
        }
        upload = await helper.uploadCloudinary(req.file.path, filename, 'supplier-notification');
        if (!upload) {
            fs.unlinkSync(req.file.path);
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
        }
        req.body.image_name = upload.public_id;
        req.body.image_source = upload.secure_url;
    }

    return req.supplierNotificationData.update(req.body)
        .then((result) => {
            if (req.file) {
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            if (req.file) {
                await helper.removeCloudinary(upload.public_id);
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'supplier_notification_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    return supplierNotification
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404,
                    errorCodes.supplierNotificationSNError.SUPPLIER_NOTIFICATION_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function remove(req, res) {
    const { supplierNotificationData } = req;
    if (supplierNotificationData.image_name) {
        const removeImage = await helper.removeCloudinary(supplierNotificationData.image_name);
        if (!removeImage) {
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_REMOVE_FILE_FROM_CDN);
        }
    }
    return supplierNotificationData.destroy()
        .then(() => {
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

export default {
    load,
    get,
    create,
    update,
    list,
    remove,
    broadcastNotification,
};
