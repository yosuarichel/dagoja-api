/* eslint-disable max-len */
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';

// const Sequelize = require('sequelize');

const adminRole = db.admin_role;
const _ = require('lodash');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    adminRole
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.adminRoleARError.ADMIN_ROLE_NOT_FOUND);
            }
            req.adminRoleData = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const response = new Response(req, res);
    return response.successResponse(200, req.adminRoleData);
}

const create = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const adminRoleData = await adminRole.count({
        where: {
            name: req.body.name,
        },
        raw: true,
    });
    if (adminRoleData > 0) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.adminRoleARError.ADMIN_ROLE_ALREADY_EXIST);
    }
    return adminRole.create(req.body)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

function update(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    return req.adminRole.update(req.body)
        .then((result) => {
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'admin_role_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    return adminRole
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.adminRoleARError.ADMIN_ROLE_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function remove(req, res) {
    const { adminRoleData } = req;
    return adminRoleData.destroy()
        .then(() => {
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

export default {
    load,
    get,
    create,
    update,
    list,
    remove,
};
