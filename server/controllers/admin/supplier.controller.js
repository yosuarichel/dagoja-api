/* eslint-disable max-len */
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import helper from '../../misc/helper';
import Response from '../../response/response';


const Sequelize = require('sequelize');
const crypto = require('crypto');
const fs = require('fs');

const Supplier = db.supplier;
const businessInformation = db.business_information;
const ownerInformation = db.owner_information;
const supplierLog = db.supplier_log;
const _ = require('lodash');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    Supplier
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.supplierSPError.SUPPLIER_NOT_FOUND);
            }
            req.supplier = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const response = new Response(req, res);
    return response.successResponse(200, req.supplier);
}

const create = async (req, res) => {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        errors.errors.push(req.fileValidationError);
    }
    if (!errors.isEmpty()) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const supplierCount = await Supplier.count({
        where: {
            [Sequelize.Op.or]: [{
                username: req.body.username,
            }, {
                phone_number: req.body.phone_number,
            }],
        },
        raw: true,
    });
    if (supplierCount > 0) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.supplierSPError.SUPPLIER_ALREADY_EXIST);
    }
    const passHashed = crypto.createHash('sha256')
        .update(req.body.password)
        .digest('hex');

    let upload = null;
    if (req.file) {
        const filename = req.file.filename.split('.')[0];
        upload = await helper.uploadCloudinary(req.file.path, filename, 'supplier');
        if (!upload) {
            fs.unlinkSync(req.file.path);
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
        }
        req.body.profile_image_name = upload.public_id;
        req.body.profile_image_source = upload.secure_url;
    }

    req.body.is_email_verif = !!req.body.email;
    req.body.is_phone_number_verif = !!req.body.phone_number;
    req.body.password = passHashed;
    return Supplier.create(req.body)
        .then(async (result) => {
            const supplierNumber = `SPL-${result.supplier_id}-${Date.now()}`;
            await Supplier.update({
                supplier_number: supplierNumber,
            }, {
                where: {
                    supplier_id: result.supplier_id,
                },
            });
            Object.assign(result, {
                supplier_number: supplierNumber,
            });
            if (req.file) {
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            if (req.file) {
                await helper.removeCloudinary(upload.public_id);
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

async function update(req, res) {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        errors.errors.push(req.fileValidationError);
    }
    if (!errors.isEmpty()) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    let upload = null;
    if (req.file) {
        const filename = req.file.filename.split('.')[0];
        if (req.supplier.profile_image_name) {
            const removeImage = await helper.removeCloudinary(req.admin.profile_image_name);
            if (!removeImage) {
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_REMOVE_FILE_FROM_CDN);
            }
        }
        upload = await helper.uploadCloudinary(req.file.path, filename, 'admin-profile');
        if (!upload) {
            fs.unlinkSync(req.file.path);
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
        }
        req.body.profile_image_name = upload.public_id;
        req.body.profile_image_source = upload.secure_url;
    }
    req.body.supplier_number = undefined;
    return req.supplier.update(req.body)
        .then((result) => {
            if (req.file) {
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            if (req.file) {
                await helper.removeCloudinary(upload.public_id);
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'supplier_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    return Supplier
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.supplierSPError.SUPPLIER_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function remove(req, res) {
    const { supplier } = req;
    if (supplier.profile_image_name) {
        const removeImage = await helper.removeCloudinary(supplier.profile_image_name);
        if (!removeImage) {
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_REMOVE_FILE_FROM_CDN);
        }
    }
    return supplier.destroy()
        .then(() => {
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

async function approve(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }
    const supplierData = await Supplier.findOne({
        where: {
            supplier_number: req.body.supplier_number,
            status: 'wait-approval',
        },
        raw: true,
    });
    if (!supplierData) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.supplierSPError.SUPPLIER_NOT_FOUND);
    }
    const businessInfoData = await businessInformation.findOne({
        where: {
            supplier_id: supplierData.supplier_id,
            status: 'wait-verification',
        },
        raw: true,
    });
    if (!businessInfoData.status) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.businessInformationBIError.BUSINESS_INFO_NOT_FOUND);
    }
    const ownerInfoData = await ownerInformation.findOne({
        where: {
            supplier_id: supplierData.supplier_id,
            status: 'wait-verification',
        },
        raw: true,
    });
    if (!ownerInfoData.status) {
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.businessInformationBIError.BUSINESS_INFO_NOT_FOUND);
    }
    await Supplier.update({
        status: 'active',
    }, {
        where: {
            supplier_number: req.body.supplier_number,
            status: 'wait-approval',
        },
        raw: true,
    });
    await businessInformation.update({
        status: 'verified',
    }, {
        where: {
            supplier_id: supplierData.supplier_id,
            status: 'wait-verification',
        },
        raw: true,
    });
    await ownerInformation.update({
        status: 'verified',
    }, {
        where: {
            supplier_id: supplierData.supplier_id,
            status: 'wait-verification',
        },
        raw: true,
    });
    await supplierLog.create({
        supplier_id: supplierData.supplier_id,
        type: 'registration',
        action: 'Registration approved by system',
    });
    const response = new Response(req, res);
    return response.successResponse(200, 'Approved');
}

export default {
    load,
    get,
    create,
    update,
    list,
    remove,
    approve,
};
