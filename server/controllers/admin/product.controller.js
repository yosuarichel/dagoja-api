/* eslint-disable max-len */
import moment from 'moment';
// import Sequelize from 'sequelize';
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import Response from '../../response/response';


const Product = db.product;
const productGalery = db.product_galery;
const Brand = db.brand;
const Category = db.category;
const productVariant = db.product_variant;
const _ = require('lodash');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    Product
        .scope([
            { method: ['includeProductGalery', productGalery] },
            { method: ['includeBrand', Brand] },
            { method: ['includeCategory', Category] },
            { method: ['includeVariant', productVariant] },
        ])
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.productPRError.PRODUCT_NOT_FOUND);
            }
            if (result.dataValues.variant && result.dataValues.variant.length > 0) {
                const quantitySum = _.sumBy(result.variant, (x) => x.dataValues.quantity);
                // eslint-disable-next-line max-len
                const remainingQuantitySum = _.sumBy(result.variant, (x) => x.dataValues.remaining_quantity);
                Object.assign(result.dataValues, {
                    total_quantity: quantitySum,
                    total_remaining_quantity: remainingQuantitySum,
                });
            }
            req.product = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const response = new Response(req, res);
    return response.successResponse(200, req.product);
}

const create = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    // const productVariantCount = await productVariant.count({
    //     where: {
    //         sku: req.body.sku,
    //     },
    //     raw: true,
    // });
    // if (productVariantCount > 0) {
    //     const response = new Response(400, errorCodes.productPRError.PRODUCT_ALREADY_EXIST);
    //     return response.failResponse(req, res);
    // }

    // Format Product Number = DGJproduct_id-brand_id-category_id
    const prefix = 'P';
    const initialLength = 3;
    const initialProductName = req.body.name.substr(0, initialLength).toUpperCase();
    const productWeight = req.body.weight;

    if (!req.body.ppn_percentage) {
        req.body.ppn_percentage = 0;
    }
    if (!req.body.markup_percentage) {
        req.body.markup_percentage = req.body.ppn_percentage;
    }
    if (req.body.warranty_expired_at) {
        req.body.warranty_expired_at = moment(req.body.warranty_expired_at).tz('Asia/Jakarta').format();
    }
    const markup = req.body.markup_percentage;
    const productPrice = (Number(req.body.supplier_price) / ((100 - markup) / 100));
    const priceAfterPPN = productPrice + (productPrice * (req.body.ppn_percentage / 100));
    req.body.final_price = priceAfterPPN.toFixed(0);
    req.body.quantity = _.sumBy(req.body.variant, (variant) => variant.quantity);
    req.body.remaining_quantity = _.sumBy(req.body.variant,
        (variant) => variant.remaining_quantity);

    return db.sequelize.transaction((t) => Product.create(req.body, {
        transaction: t,
    }).then(async (result) => {
        const productCode = `${prefix}${result.product_id}-${initialProductName}-${productWeight}-DGJ`;
        await Product.update({
            product_number: productCode,
        }, {
            where: {
                product_id: result.product_id,
            },
            transaction: t,
        });

        const variantPayload = [];
        req.body.variant.map((x) => variantPayload.push({
            product_id: result.product_id,
            sku: x.sku,
            variant_name: x.variant_name,
            variant_option: x.variant_option,
            variant_value: x.variant_value,
            quantity: x.quantity,
            remaining_quantity: x.remaining_quantity,
        }));
        await productVariant.bulkCreate(variantPayload, {
            transaction: t,
        });

        Object.assign(result, {
            product_number: productCode,
        });
        return result;
    })).then((result) => {
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
};

async function update(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    if (req.body.warranty_expired_at) {
        req.body.warranty_expired_at = moment(req.body.warranty_expired_at).tz('Asia/Jakarta').format();
    }

    if (!req.body.supplier_price) {
        req.body.supplier_price = req.product.supplier_price;
    }
    if (!req.body.ppn_percentage) {
        req.body.ppn_percentage = req.product.ppn_percentage;
    }
    if (!req.body.markup_percentage) {
        req.body.markup_percentage = req.product.markup_percentage;
    }
    if (!req.body.markup_percentage) {
        req.body.markup_percentage = req.product.markup_percentage;
    }
    req.body.quantity = undefined;
    req.body.remaining_quantity = undefined;

    const productVariantData = await productVariant.findAll({
        where: {
            product_id: req.product.product_id,
        },
        raw: true,
    });

    if (req.body.variant.length > 0) {
        req.body.quantity = _.sumBy(productVariantData, (variant) => variant.quantity);
        req.body.remaining_quantity = _.sumBy(productVariantData,
            (variant) => variant.remaining_quantity);
    }

    const markup = req.body.markup_percentage;
    const productPrice = (Number(req.body.supplier_price) / ((100 - markup) / 100));
    const priceAfterPPN = productPrice + (productPrice * (req.body.ppn_percentage / 100));
    req.body.final_price = priceAfterPPN.toFixed(0);

    return db.sequelize.transaction((t) => req.product.update(req.body, {
        transaction: t,
    }).then(async (result) => {
        const variantPayload = [];
        req.body.variant.map((x) => variantPayload.push({
            product_id: req.product.product_id,
            sku: x.sku,
            variant_name: x.variant_name,
            variant_option: x.variant_option,
            variant_value: x.variant_value,
            quantity: x.quantity,
            remaining_quantity: x.remaining_quantity,
        }));
        await productVariant.bulkCreate(variantPayload, {
            transaction: t,
        });
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    }));
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'product_id');
    const pagination = simplePagination(req);
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    return Product
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
            { method: ['includeProductGalery', productGalery] },
            { method: ['includeBrand', Brand] },
            { method: ['includeCategory', Category] },
            { method: ['includeVariant', productVariant] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.productPRError.PRODUCT_NOT_FOUND);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function remove(req, res) {
    const { product } = req;
    return product.destroy()
        .then(async () => {
            await productGalery.destroy({
                where: {
                    product_id: product.product_id,
                },
            });
            await productVariant.destroy({
                where: {
                    product_id: product.product_id,
                },
            });
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function updatePrice(req, res) {
    const { product } = req;
    const productData = JSON.parse(JSON.stringify(product));
    req.body.supplier_price = Number(req.body.supplier_price || productData.supplier_price);
    req.body.ppn_percentage = Number(req.body.ppn_percentage || productData.ppn_percentage);
    req.body.markup_percentage = Number(req.body.markup_percentage || productData.markup_percentage);

    const productPrice = (Number(req.body.supplier_price) / ((100 - req.body.markup_percentage) / 100));
    const priceAfterPPN = productPrice + (productPrice * (req.body.ppn_percentage / 100));
    req.body.final_price = priceAfterPPN.toFixed(0);
    // return console.log(req.body)
    return product.update({
        supplier_price: req.body.supplier_price,
        ppn_percentage: req.body.ppn_percentage,
        markup_percentage: req.body.markup_percentage,
        final_price: req.body.final_price,
    }).then(async (result) => {
        const response = new Response(req, res);
        return response.successResponse(200, result);
    }).catch((e) => {
        const response = new Response(req, res);
        return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
    });
}

export default {
    load,
    get,
    create,
    update,
    list,
    remove,
    updatePrice,
};
