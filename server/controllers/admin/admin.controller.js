/* eslint-disable max-len */
import errorCodes from '../../errors/index.error';
import db from '../../../config/sequelize';
import helper from '../../misc/helper';
import Response from '../../response/response';

const Sequelize = require('sequelize');
const crypto = require('crypto');
const fs = require('fs');

const Admin = db.admin;
const adminRole = db.admin_role;
const _ = require('lodash');
const { validationResult } = require('express-validator');
const { simpleOrdering, simplePagination } = require('../../misc/misc');

function load(req, res, next, id) {
    Admin
        .findByPk(id)
        .then((result) => {
            if (!result) {
                const response = new Response(req, res);
                return response.failResponse(404, errorCodes.adminADError.ADMIN_NOT_FOUND);
            }
            req.admin = result;
            return next();
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function get(req, res) {
    const response = new Response(req, res);
    return response.successResponse(200, req.admin);
}

const create = async (req, res) => {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        errors.errors.push(req.fileValidationError);
    }
    if (!errors.isEmpty()) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    const adminRoleCount = await adminRole.count({
        where: {
            admin_role_id: req.body.admin_role_id,
        },
        raw: true,
    });
    if (adminRoleCount === 0) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.adminRoleARError.ADMIN_ROLE_NOT_FOUND);
    }
    const adminData = await Admin.count({
        where: {
            email: {
                [Sequelize.Op.iLike]: `%${req.body.email}%`,
            },
        },
        raw: true,
    });
    if (adminData > 0) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const response = new Response(req, res);
        return response.failResponse(400, errorCodes.adminADError.ADMIN_ALREADY_REGISTERED);
    }

    let upload = null;
    if (req.file) {
        const filename = req.file.filename.split('.')[0];
        upload = await helper.uploadCloudinary(req.file.path, filename, 'admin-profile');
        if (!upload) {
            fs.unlinkSync(req.file.path);
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
        }
        req.body.profile_image_name = upload.public_id;
        req.body.profile_image_source = upload.secure_url;
    }

    const passHashed = crypto.createHash('sha256')
        .update(req.body.password)
        .digest('hex');
    req.body.password = passHashed;
    return Admin.create(req.body)
        .then(async (result) => {
            if (req.file) {
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            if (req.file) {
                await helper.removeCloudinary(upload.public_id);
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
};

async function update(req, res) {
    const errors = validationResult(req);
    if (req.fileValidationError) {
        errors.errors.push(req.fileValidationError);
    }
    if (!errors.isEmpty()) {
        if (req.file) {
            fs.unlinkSync(req.file.path);
        }
        const data = _(errors.errors)
            .groupBy('param')
            .mapValues((group) => _.map(group, 'msg'))
            .value();
        const response = new Response(req, res);
        return response.failResponse(422, data);
    }

    let upload = null;
    if (req.file) {
        const filename = req.file.filename.split('.')[0];
        if (req.admin.profile_image_name) {
            const removeImage = await helper.removeCloudinary(req.admin.profile_image_name);
            if (!removeImage) {
                const response = new Response(req, res);
                return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_REMOVE_FILE_FROM_CDN);
            }
        }
        upload = await helper.uploadCloudinary(req.file.path, filename, 'admin-profile');
        if (!upload) {
            fs.unlinkSync(req.file.path);
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_UPLOAD_FILE_TO_CDN);
        }
        req.body.profile_image_name = upload.public_id;
        req.body.profile_image_source = upload.secure_url;
    }
    if (req.body.password) {
        const passHashed = crypto.createHash('sha256')
            .update(req.body.password)
            .digest('hex');
        req.body.password = passHashed;
    }
    return req.admin.update(req.body)
        .then((result) => {
            if (req.file) {
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.successResponse(200, result);
        })
        .catch(async (e) => {
            if (req.file) {
                await helper.removeCloudinary(upload.public_id);
                fs.unlinkSync(req.file.path);
            }
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

function list(req, res) {
    const ordering = simpleOrdering(req, 'admin_id');
    const pagination = simplePagination(req);
    const response = new Response(req, res);
    const option = {
        where: {
        },
        include: [],
        distinct: true,
    };
    return Admin
        .scope([
            { method: ['ordering', ordering] },
            { method: ['pagination', req.query.pagination, pagination] },
            { method: ['includeAdminRole', req.query.admin_role_id, adminRole] },
        ])
        .findAndCountAll(option)
        .then((result) => {
            if (!result) {
                return response.failResponse(404, errorCodes.adminADError.ADMIN_NOT_FOUND);
            }
            return response.successResponse(200, result);
        })
        .catch((e) => response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e));
}

async function remove(req, res) {
    const { admin } = req;
    if (admin.profile_image_name) {
        const removeImage = await helper.removeCloudinary(admin.profile_image_name);
        if (!removeImage) {
            const response = new Response(req, res);
            return response.failResponse(400, errorCodes.generalGEError.UPLOAD.FAILED_REMOVE_FILE_FROM_CDN);
        }
    }
    return admin.destroy()
        .then(() => {
            const response = new Response(req, res);
            return response.successResponse(200, 'OK');
        })
        .catch((e) => {
            const response = new Response(req, res);
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        });
}

export default {
    load,
    get,
    create,
    update,
    list,
    remove,
};
