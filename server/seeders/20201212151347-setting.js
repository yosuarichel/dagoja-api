
require('babel-polyfill');

const fs = require('fs');

module.exports = {
    // eslint-disable-next-line no-unused-vars
    up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
        const newData = [];
        const rawdata = fs.readFileSync('server/masterdata/setting.json');
        const settings = JSON.parse(rawdata);
        await Promise.all(settings.map(async (setting) => {
            const seedData = {
                about: setting.about,
                supplier_session_expiry: setting.supplier_session_expiry,
                dropshipper_session_expiry: setting.dropshipper_session_expiry,
                admin_session_expiry: setting.admin_session_expiry,
                android_version_id: setting.android_version_id,
                android_version_update_message: setting.android_version_update_message,
                android_is_maintenance: setting.android_is_maintenance,
                android_maintenance_message: setting.android_maintenance_message,
                ios_version_id: setting.ios_version_id,
                ios_version_update_message: setting.ios_version_update_message,
                ios_is_maintenance: setting.ios_is_maintenance,
                ios_maintenance_message: setting.ios_maintenance_message,
                shop_url: setting.shop_url,
                created_at: new Date(),
                updated_at: new Date(),
            };
            // eslint-disable-next-line max-len
            // if (newData.findIndex((x) => x.name === seedData.name) === -1) newData.push(seedData);
            newData.push(seedData);
        }));

        return queryInterface.bulkInsert('setting', newData);
    },

    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    },
};
