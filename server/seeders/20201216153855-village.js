
require('babel-polyfill');

const fs = require('fs');

module.exports = {
    // eslint-disable-next-line no-unused-vars
    up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronivillage.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
        const newData = [];
        const rawdata = fs.readFileSync('server/masterdata/village.json');
        const villages = JSON.parse(rawdata);
        await Promise.all(villages.map(async (village) => {
            const seedData = {
                name: village.name,
                district_id: village.district_id,
                postal_code: village.postal_code,
                created_at: new Date(),
                updated_at: new Date(),
            };
            // eslint-disable-next-line max-len
            // if (newData.findIndex((x) => x.name === seedData.name) === -1) newData.push(seedData);
            newData.push(seedData);
        }));

        return queryInterface.bulkInsert('village', newData);
    },

    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronivillage.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    },
};
