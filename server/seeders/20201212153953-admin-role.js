
require('babel-polyfill');

const fs = require('fs');

module.exports = {
    // eslint-disable-next-line no-unused-vars
    up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
        const newData = [];
        const rawdata = fs.readFileSync('server/masterdata/admin-role.json');
        const adminRoles = JSON.parse(rawdata);
        await Promise.all(adminRoles.map(async (adminRole) => {
            const seedData = {
                name: adminRole.name,
                created_at: new Date(),
                updated_at: new Date(),
            };
            // eslint-disable-next-line max-len
            // if (newData.findIndex((x) => x.name === seedData.name) === -1) newData.push(seedData);
            newData.push(seedData);
        }));

        return queryInterface.bulkInsert('admin_role', newData);
    },

    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    },
};
