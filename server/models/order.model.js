import moment from 'moment';

export default (sequelize, DataTypes) => {
    const Order = sequelize.define('order', {
        order_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        dropshipper_id: {
            type: DataTypes.BIGINT,
        },
        dropshipper_customer_id: {
            type: DataTypes.BIGINT,
        },
        delivery_option_id: {
            type: DataTypes.BIGINT,
        },
        order_number: {
            type: DataTypes.STRING,
        },
        delivery_fee: {
            type: DataTypes.DOUBLE,
        },
        total_item_price: {
            type: DataTypes.DOUBLE,
        },
        total_price: {
            type: DataTypes.DOUBLE,
        },
        payment_status: {
            type: DataTypes.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'order',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),
        },
    });
    Order.associate = (models) => {
        // associations can be defined here
        Order.hasMany(models.order_product, {
            foreignKey: 'order_id',
            as: 'order_product',
        });
        Order.belongsTo(models.dropshipper, {
            foreignKey: 'dropshipper_id',
            as: 'dropshipper',
        });
        Order.belongsTo(models.dropshipper_customer, {
            foreignKey: 'dropshipper_customer_id',
            as: 'customer',
        });
        Order.hasMany(models.order_status, {
            foreignKey: 'order_id',
            as: 'order_status',
        });
    };
    return Order;
};
