import moment from 'moment';

export default (sequelize, DataTypes) => {
    const Banner = sequelize.define('banner', {
        banner_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        name: {
            type: DataTypes.STRING,
        },
        description: {
            type: DataTypes.TEXT,
        },
        banner_type_id: {
            type: DataTypes.INTEGER,
        },
        image_name: {
            type: DataTypes.STRING,
        },
        image_source: {
            type: DataTypes.STRING,
        },
        os: {
            type: DataTypes.ARRAY(DataTypes.STRING),
        },
        status: {
            type: DataTypes.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'banner',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),

            includeBannerTypeApp: (bannerType) => ({
                include: [{
                    model: bannerType,
                    as: 'banner_type',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at'],
                    },
                }],
            }),
        },
    });
    Banner.associate = (models) => {
        // associations can be defined here
        Banner.belongsTo(models.banner_type, {
            foreignKey: 'banner_type_id',
            as: 'banner_type',
        });
    };
    return Banner;
};
