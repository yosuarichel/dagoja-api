/* eslint-disable max-len */
import moment from 'moment';
import Sequelize from 'sequelize';

export default (sequelize, DataTypes) => {
    const dropshipperCatalog = sequelize.define('dropshipper_catalog', {
        dropshipper_catalog_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        dropshipper_id: {
            type: DataTypes.BIGINT,
        },
        product_id: {
            type: DataTypes.BIGINT,
        },
        price: {
            type: DataTypes.DOUBLE,
        },
        discount: {
            type: DataTypes.INTEGER,
        },
        final_price: {
            type: DataTypes.DOUBLE,
        },
        is_disabled: {
            type: DataTypes.BOOLEAN,
        },
        is_coming_soon: {
            type: DataTypes.BOOLEAN,
        },
        status: {
            type: DataTypes.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'dropshipper_catalog',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),

            // Scopes for App
            includeProductListApp: (osParam, product, brand, category, galery) => ({
                include: [{
                    model: product,
                    as: 'product',
                    where: {
                        os: {
                            [Sequelize.Op.overlap]: [osParam],
                        },
                    },
                    attributes: {
                        exclude: ['description', 'supplier_price', 'ppn_percentage', 'markup_percentage', 'weight', 'condition', 'minimum_order', 'is_warranty'],
                    },
                    include: [{
                        model: brand,
                        as: 'brand',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                        },
                    }, {
                        model: category,
                        as: 'category',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                        },
                    }, {
                        model: galery,
                        as: 'galery',
                        attributes: {
                            exclude: ['product_id', 'created_at', 'updated_at', 'deleted_at'],
                        },
                    }],
                }],
            }),
            includeProductDetailApp: (osParam, product, brand, category, galery, variant) => ({
                include: [{
                    model: product,
                    as: 'product',
                    where: {
                        os: {
                            [Sequelize.Op.overlap]: [osParam],
                        },
                    },
                    attributes: {
                        exclude: ['supplier_price', 'ppn_percentage', 'markup_percentage'],
                    },
                    include: [{
                        model: brand,
                        as: 'brand',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                        },
                    }, {
                        model: category,
                        as: 'category',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                        },
                    }, {
                        model: galery,
                        as: 'galery',
                        attributes: {
                            exclude: ['product_id', 'created_at', 'updated_at', 'deleted_at'],
                        },
                    }, {
                        model: variant,
                        as: 'variant',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at'],
                        },
                        // attributes: [],
                    }],
                }],
            }),
            includeCategoryOfProductApp: (product, category) => ({
                include: [{
                    model: product,
                    as: 'product',
                    attributes: [],
                    include: [{
                        model: category,
                        as: 'category',
                        attributes: [],
                    }],
                }],
            }),
            includeProductApp: (osParam, variantId, product, brand, category, galery, variant) => ({
                include: [{
                    model: product,
                    as: 'product',
                    where: {
                        os: {
                            [Sequelize.Op.overlap]: [osParam],
                        },
                    },
                    attributes: {
                        exclude: ['description', 'supplier_price', 'ppn_percentage', 'markup_percentage', 'weight', 'condition', 'minimum_order', 'is_warranty'],
                    },
                    include: [{
                        model: brand,
                        as: 'brand',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                        },
                    }, {
                        model: category,
                        as: 'category',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                        },
                    }, {
                        model: galery,
                        as: 'galery',
                        attributes: {
                            exclude: ['product_id', 'created_at', 'updated_at', 'deleted_at'],
                        },
                    }, {
                        model: variant,
                        as: 'variant',
                        where: {
                            product_variant_id: variantId,
                        },
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at'],
                        },
                        // attributes: [],
                    }],
                }],
            }),
            // End scopes for App
        },
    });
    dropshipperCatalog.associate = (models) => {
        // associations can be defined here
        dropshipperCatalog.belongsTo(models.product, {
            foreignKey: 'product_id',
            as: 'product',
        });
    };
    return dropshipperCatalog;
};
