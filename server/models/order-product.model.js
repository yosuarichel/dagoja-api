import moment from 'moment';

export default (sequelize, DataTypes) => {
    const orderStatus = sequelize.define('order_product', {
        order_product_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        order_id: {
            type: DataTypes.BIGINT,
        },
        product_id: {
            type: DataTypes.BIGINT,
        },
        variant_id: {
            type: DataTypes.BIGINT,
        },
        brand_id: {
            type: DataTypes.BIGINT,
        },
        category_id: {
            type: DataTypes.BIGINT,
        },
        supplier_id: {
            type: DataTypes.BIGINT,
        },

        purchased_quantity: {
            type: DataTypes.BIGINT,
        },

        brand_name: {
            type: DataTypes.STRING,
        },
        brand_image_source: {
            type: DataTypes.STRING,
        },

        category_name: {
            type: DataTypes.STRING,
        },
        category_image_source: {
            type: DataTypes.STRING,
        },

        product_number: {
            type: DataTypes.STRING,
        },
        name: {
            type: DataTypes.STRING,
        },
        supplier_price: {
            type: DataTypes.DOUBLE,
        },
        ppn_percentage: {
            type: DataTypes.DOUBLE,
        },
        markup_percentage: {
            type: DataTypes.DOUBLE,
        },
        final_price: {
            type: DataTypes.DOUBLE,
        },
        quantity: {
            type: DataTypes.INTEGER,
        },
        remaining_quantity: {
            type: DataTypes.INTEGER,
        },
        weight: {
            type: DataTypes.DOUBLE,
        },
        condition: {
            type: DataTypes.STRING,
        },
        minimum_order: {
            type: DataTypes.INTEGER,
        },
        is_discount: {
            type: DataTypes.BOOLEAN,
        },
        discount_percentage: {
            type: DataTypes.DOUBLE,
        },
        is_warranty: {
            type: DataTypes.BOOLEAN,
        },
        warranty_expired_at: {
            type: DataTypes.DATE,
        },
        galery: {
            type: DataTypes.ARRAY(DataTypes.JSON),
        },
        description: {
            type: DataTypes.TEXT,
        },
        status: {
            type: DataTypes.STRING,
        },
        is_disabled: {
            type: DataTypes.BOOLEAN,
        },
        is_coming_soon: {
            type: DataTypes.BOOLEAN,
        },
        os: {
            type: DataTypes.ARRAY(DataTypes.STRING),
        },

        variant_sku: {
            type: DataTypes.STRING,
        },
        variant_name: {
            type: DataTypes.STRING,
        },
        variant_option: {
            type: DataTypes.STRING,
        },
        variant_value: {
            type: DataTypes.STRING,
        },
        variant_quantity: {
            type: DataTypes.INTEGER,
        },
        variant_remaining_quantity: {
            type: DataTypes.INTEGER,
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'order_product',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),
        },
    });
    orderStatus.associate = () => {
        // associations can be defined here
    };
    return orderStatus;
};
