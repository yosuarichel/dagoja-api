import moment from 'moment';
import Sequelize from 'sequelize';

export default (sequelize, DataTypes) => {
    const dropshipperCart = sequelize.define('dropshipper_cart', {
        dropshipper_cart_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        dropshipper_id: {
            type: DataTypes.BIGINT,
        },
        product_id: {
            type: DataTypes.BIGINT,
        },
        product_variant_id: {
            type: DataTypes.BIGINT,
        },
        quantity: {
            type: DataTypes.INTEGER,
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'dropshipper_cart',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),
            includeProductCartApp: (osParam, product, brand, category, galery) => ({
                include: [{
                    model: product,
                    as: 'product',
                    where: {
                        os: {
                            [Sequelize.Op.overlap]: [osParam],
                        },
                    },
                    attributes: {
                        exclude: ['description', 'supplier_price', 'ppn_percentage', 'markup_percentage', 'weight', 'condition', 'minimum_order', 'is_warranty'],
                    },
                    include: [{
                        model: brand,
                        as: 'brand',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                        },
                    }, {
                        model: category,
                        as: 'category',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                        },
                    }, {
                        model: galery,
                        as: 'galery',
                        attributes: {
                            exclude: ['product_id', 'created_at', 'updated_at', 'deleted_at'],
                        },
                    }],
                }],
            }),
            includeProductVariantCartApp: (variant) => ({
                include: [{
                    model: variant,
                    as: 'variant',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at'],
                    },
                }],
            }),

            includeProductCartPaymentApp: (osParam, product, brand, category, galery) => ({
                include: [{
                    model: product,
                    as: 'product',
                    attributes: {
                        exclude: [],
                    },
                    include: [{
                        model: brand,
                        as: 'brand',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                        },
                    }, {
                        model: category,
                        as: 'category',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                        },
                    }, {
                        model: galery,
                        as: 'galery',
                        attributes: {
                            exclude: ['product_id', 'created_at', 'updated_at', 'deleted_at'],
                        },
                    }],
                }],
            }),

            includeProductCartCallbackApp: (product, brand, category, galery) => ({
                include: [{
                    model: product,
                    as: 'product',
                    attributes: {
                        exclude: ['description', 'supplier_price', 'ppn_percentage', 'markup_percentage', 'weight', 'condition', 'minimum_order', 'is_warranty'],
                    },
                    include: [{
                        model: brand,
                        as: 'brand',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                        },
                    }, {
                        model: category,
                        as: 'category',
                        attributes: {
                            exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                        },
                    }, {
                        model: galery,
                        as: 'galery',
                        attributes: {
                            exclude: ['product_id', 'created_at', 'updated_at', 'deleted_at'],
                        },
                    }],
                }],
            }),
        },
    });
    dropshipperCart.associate = (models) => {
        // associations can be defined here
        dropshipperCart.belongsTo(models.product, {
            foreignKey: 'product_id',
            as: 'product',
        });
        dropshipperCart.belongsTo(models.product_variant, {
            foreignKey: 'product_variant_id',
            as: 'variant',
        });
    };
    return dropshipperCart;
};
