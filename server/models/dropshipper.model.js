import moment from 'moment';

export default (sequelize, DataTypes) => {
    const Dropshipper = sequelize.define('dropshipper', {
        dropshipper_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        dropshipper_number: {
            type: DataTypes.STRING,
        },
        email: {
            type: DataTypes.STRING,
        },
        password: {
            type: DataTypes.STRING,
        },
        full_name: {
            type: DataTypes.STRING,
        },
        phone_number: {
            type: DataTypes.STRING,
        },
        birth_date: {
            type: DataTypes.STRING,
        },
        gender: {
            type: DataTypes.STRING,
        },
        nik: {
            type: DataTypes.STRING,
        },
        nik_image: {
            type: DataTypes.STRING,
        },
        nik_image_source: {
            type: DataTypes.STRING,
        },
        occupation_id: {
            type: DataTypes.INTEGER,
        },
        is_email_verif: {
            type: DataTypes.BOOLEAN,
        },
        email_token: {
            type: DataTypes.STRING,
        },
        is_phone_number_verif: {
            type: DataTypes.BOOLEAN,
        },
        profile_image_name: {
            type: DataTypes.STRING,
        },
        profile_image_source: {
            type: DataTypes.STRING,
        },
        province: {
            type: DataTypes.STRING,
        },
        city: {
            type: DataTypes.STRING,
        },
        district: {
            type: DataTypes.STRING,
        },
        vilage: {
            type: DataTypes.STRING,
        },
        postal_code: {
            type: DataTypes.STRING,
        },
        address: {
            type: DataTypes.STRING,
        },
        cash: {
            type: DataTypes.DOUBLE,
        },
        poin: {
            type: DataTypes.DOUBLE,
        },
        status: {
            type: DataTypes.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'dropshipper',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),
        },
    });
    Dropshipper.associate = () => {
        // associations can be defined here
    };
    return Dropshipper;
};
