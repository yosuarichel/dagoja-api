import moment from 'moment';
// import Sequelize from 'sequelize';

module.exports = (sequelize, DataTypes) => {
    const product = sequelize.define('product', {
        product_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        brand_id: {
            type: DataTypes.BIGINT,
        },
        category_id: {
            type: DataTypes.BIGINT,
        },
        supplier_id: {
            type: DataTypes.BIGINT,
        },
        product_number: {
            type: DataTypes.STRING,
        },
        name: {
            type: DataTypes.STRING,
        },
        supplier_price: {
            type: DataTypes.DOUBLE,
        },
        ppn_percentage: {
            type: DataTypes.DOUBLE,
        },
        markup_percentage: {
            type: DataTypes.DOUBLE,
        },
        final_price: {
            type: DataTypes.DOUBLE,
        },
        quantity: {
            type: DataTypes.INTEGER,
        },
        remaining_quantity: {
            type: DataTypes.INTEGER,
        },
        weight: {
            type: DataTypes.DOUBLE,
        },
        condition: {
            type: DataTypes.STRING,
        },
        minimum_order: {
            type: DataTypes.INTEGER,
        },
        is_discount: {
            type: DataTypes.BOOLEAN,
        },
        discount_percentage: {
            type: DataTypes.DOUBLE,
        },
        is_warranty: {
            type: DataTypes.BOOLEAN,
        },
        warranty_expired_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('warranty_expired_at')) {
                    const dateText = this.getDataValue('warranty_expired_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        description: {
            type: DataTypes.TEXT,
        },
        status: {
            type: DataTypes.STRING,
        },
        is_disabled: {
            type: DataTypes.BOOLEAN,
        },
        is_coming_soon: {
            type: DataTypes.BOOLEAN,
        },
        os: {
            type: DataTypes.ARRAY(DataTypes.STRING),
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'product',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),

            // Scope for CMS
            includeProductGalery: (productGalery) => ({
                include: [{
                    model: productGalery,
                    as: 'galery',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at'],
                    },
                }],
            }),
            includeBrand: (brand) => ({
                include: [{
                    model: brand,
                    as: 'brand',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                    },
                }],
            }),
            includeCategory: (category) => ({
                include: [{
                    model: category,
                    as: 'category',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                    },
                }],
            }),
            includeVariant: (variant) => ({
                include: [{
                    model: variant,
                    as: 'variant',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at'],
                    },
                }],
            }),
            // End scope for CMS

            // Scopes for App
            includeProductGaleryApp: (productGalery) => ({
                include: [{
                    model: productGalery,
                    as: 'galery',
                    attributes: {
                        exclude: ['product_id', 'created_at', 'updated_at', 'deleted_at'],
                    },
                }],
            }),
            includeBrandApp: (brand) => ({
                include: [{
                    model: brand,
                    as: 'brand',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at', 'code', 'status', 'image_name'],
                    },
                }],
            }),
            includeCategoryApp: (category) => ({
                include: [{
                    model: category,
                    as: 'category',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at', 'status', 'image_name'],
                    },
                }],
            }),
            includeVariantApp: (variant) => ({
                include: [{
                    model: variant,
                    as: 'variant',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at'],
                    },
                }],
            }),
            removeAllUnusedAttributesApp: () => ({
                attributes: {
                    exclude: ['description', 'supplier_price', 'ppn_percentage', 'markup_percentage', 'weight', 'condition', 'minimum_order', 'is_warranty'],
                },
            }),
            normalizeAttributesApp: () => ({
                attributes: {
                    exclude: ['supplier_price', 'ppn_percentage', 'markup_percentage'],
                },
            }),
            // End scope for App

            includeCategoryOfProductApp: (category) => ({
                include: [{
                    model: category,
                    as: 'category',
                    attributes: [],
                }],
            }),

        },
    });
    product.associate = (models) => {
        // associations can be defined here
        product.hasMany(models.product_galery, {
            foreignKey: 'product_id',
            as: 'galery',
        });
        product.belongsTo(models.brand, {
            foreignKey: 'brand_id',
            as: 'brand',
        });
        product.belongsTo(models.category, {
            foreignKey: 'category_id',
            as: 'category',
        });
        product.hasMany(models.product_variant, {
            foreignKey: 'product_id',
            as: 'variant',
        });
    };
    return product;
};
