import moment from 'moment';

export default (sequelize, DataTypes) => {
    const dropshipperCustomer = sequelize.define('dropshipper_customer', {
        dropshipper_customer_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        dropshipper_id: {
            type: DataTypes.BIGINT,
        },
        full_name: {
            type: DataTypes.STRING,
        },
        phone_number: {
            type: DataTypes.STRING,
        },
        province: {
            type: DataTypes.STRING,
        },
        city: {
            type: DataTypes.STRING,
        },
        district: {
            type: DataTypes.STRING,
        },
        vilage: {
            type: DataTypes.STRING,
        },
        postal_code: {
            type: DataTypes.STRING,
        },
        address: {
            type: DataTypes.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'dropshipper_customer',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),
            // includeSupplierTransactionLogApp: (log) => ({
            //     include: [{
            //         model: log,
            //         as: 'log',
            //     }],
            // }),
        },
        indexes: [
            {
                unique: false,
                fields: ['full_name', 'phone_number', 'address'],
            },
        ],
    });
    dropshipperCustomer.associate = () => {
        // associations can be defined here
        // dropshipperCustomer.hasOne(models.dropshipper_customer_log, {
        //     foreignKey: 'dropshipper_customer_id',
        //     as: 'log',
        // });
    };
    return dropshipperCustomer;
};
