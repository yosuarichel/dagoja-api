import moment from 'moment';

export default (sequelize, DataTypes) => {
    const dropshipperTransaction = sequelize.define('dropshipper_transaction', {
        dropshipper_transaction_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        dropshipper_id: {
            type: DataTypes.BIGINT,
        },
        transaction_number: {
            type: DataTypes.STRING,
        },
        transaction: {
            type: DataTypes.DOUBLE,
        },
        balance_before: {
            type: DataTypes.DOUBLE,
        },
        balance_after: {
            type: DataTypes.DOUBLE,
        },
        type: {
            type: DataTypes.STRING,
        },
        description: {
            type: DataTypes.TEXT,
        },
        op: {
            type: DataTypes.STRING,
        },
        status: {
            type: DataTypes.STRING,
        },
        settlement_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('settlement_at')) {
                    const dateText = this.getDataValue('settlement_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'dropshipper_transaction',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),
            // includeSupplierTransactionLogApp: (log) => ({
            //     include: [{
            //         model: log,
            //         as: 'log',
            //     }],
            // }),
        },
    });
    dropshipperTransaction.associate = () => {
        // associations can be defined here
        // dropshipperTransaction.hasOne(models.dropshipper_transaction_log, {
        //     foreignKey: 'dropshipper_transaction_id',
        //     as: 'log',
        // });
    };
    return dropshipperTransaction;
};
