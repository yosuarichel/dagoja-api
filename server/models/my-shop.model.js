import moment from 'moment';

export default (sequelize, DataTypes) => {
    const myShop = sequelize.define('my_shop', {
        my_shop_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        dropshipper_id: {
            type: DataTypes.BIGINT,
        },
        target_bonus_id: {
            type: DataTypes.INTEGER,
        },
        shop_name: {
            type: DataTypes.STRING,
        },
        link: {
            type: DataTypes.STRING,
        },
        contact: {
            type: DataTypes.STRING,
        },
        current_omzet: {
            type: DataTypes.DOUBLE,
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'my_shop',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),

            includeTargetBonusApp: (targetBonus) => ({
                include: [{
                    model: targetBonus,
                    as: 'target_bonus',
                    attributes: {
                        exclude: ['created_at', 'updated_at', 'deleted_at'],
                    },
                }],
            }),
        },
    });
    myShop.associate = (models) => {
        // associations can be defined here
        myShop.belongsTo(models.target_bonus, {
            foreignKey: 'target_bonus_id',
            as: 'target_bonus',
        });
    };
    return myShop;
};
