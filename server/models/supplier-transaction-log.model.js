import moment from 'moment';

export default (sequelize, DataTypes) => {
    const supplierTransaction = sequelize.define('supplier_transaction_log', {
        supplier_transaction_log_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        supplier_transaction_id: {
            type: DataTypes.BIGINT,
        },
        sender_bank: {
            type: DataTypes.STRING,
        },
        sender_account: {
            type: DataTypes.STRING,
        },
        recipient_bank: {
            type: DataTypes.STRING,
        },
        recipient_account: {
            type: DataTypes.STRING,
        },
        product_id: {
            type: DataTypes.BIGINT,
        },
        deleted_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('deleted_at')) {
                    const dateText = this.getDataValue('deleted_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        created_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('created_at')) {
                    const dateText = this.getDataValue('created_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
        updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
            get() {
                if (this.getDataValue('updated_at')) {
                    const dateText = this.getDataValue('updated_at');
                    return moment(dateText).tz('Asia/Jakarta').format();
                }
                return null;
            },
        },
    }, {
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'supplier_transaction_log',
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        scopes: {
            ordering: (ordering) => ({
                order: [
                    [ordering.orderBy, ordering.orderType],
                ],
            }),
            pagination: (param, pagination) => (param !== 'false' ? {
                offset: pagination.page,
                limit: pagination.row,
            } : {}),
        },
    });
    supplierTransaction.associate = () => {
        // associations can be defined here
    };
    return supplierTransaction;
};
