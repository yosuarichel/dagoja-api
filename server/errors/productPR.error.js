export default {
    PRODUCT_NOT_FOUND: {
        code: 'PR001',
        message: 'PRODUCT_NOT_FOUND',
    },
    PRODUCT_ALREADY_EXIST: {
        code: 'PR002',
        message: 'PRODUCT_ALREADY_EXIST',
    },
    PRODUCT_ID_REQUIRED: {
        code: 'PR003',
        message: 'PRODUCT_ID_REQUIRED',
    },
    PRODUCT_OUT_OF_STOCK: {
        code: 'PR004',
        message: 'PRODUCT_OUT_OF_STOCK',
    },
    PRODUCT_INACTIVE: {
        code: 'PR005',
        message: 'PRODUCT_INACTIVE',
    },
    PRODUCT_DISABLED: {
        code: 'PR006',
        message: 'PRODUCT_DISABLED',
    },
    PRODUCT_AVAILABLE_SOON: {
        code: 'PR007',
        message: 'PRODUCT_AVAILABLE_SOON',
    },
    PRODUCT_CATALOG_NOT_FOUND: {
        code: 'PR008',
        message: 'PRODUCT_CATALOG_NOT_FOUND',
    },
    PRODUCT_CATALOG_ALREADY_EXIST: {
        code: 'PR009',
        message: 'PRODUCT_CATALOG_ALREADY_EXIST',
    },
    DISCOUNT_IS_VERY_HIGH: {
        code: 'PR010',
        message: 'DISCOUNT_IS_VERY_HIGH',
    },
    THE_PRICE_IS_VERY_LOW: {
        code: 'PR011',
        message: 'THE_PRICE_IS_VERY_LOW',
    },
    THE_PRICE_IS_VERY_HIGH: {
        code: 'PR012',
        message: 'THE_PRICE_IS_VERY_HIGH',
    },
};
