export default {
    TARGET_BONUS_NOT_FOUND: {
        code: 'TB001',
        message: 'TARGET_BONUS_NOT_FOUND',
    },
    TARGET_BONUS_ALREADY_EXIST: {
        code: 'TB002',
        message: 'TARGET_BONUS_ALREADY_EXIST',
    },
};
