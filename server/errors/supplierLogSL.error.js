export default {
    SUPPLIER_LOG_NOT_FOUND: {
        code: 'SL001',
        message: 'SUPPLIER_LOG_NOT_FOUND',
    },
    SUPPLIER_LOG_ALREADY_EXIST: {
        code: 'SL002',
        message: 'SUPPLIER_LOG_ALREADY_EXIST',
    },
};
