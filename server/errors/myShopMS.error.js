export default {
    MY_SHOP_NOT_FOUND: {
        code: 'MS001',
        message: 'MY_SHOP_NOT_FOUND',
    },
    MY_SHOP_ALREADY_EXIST: {
        code: 'MS002',
        message: 'MY_SHOP_ALREADY_EXIST',
    },
};
