export default {
    BUSINESS_INFO_NOT_FOUND: {
        code: 'BI001',
        message: 'BUSINESS_INFO_NOT_FOUND',
    },
    BUSINESS_INFO_ALREADY_EXIST: {
        code: 'BI002',
        message: 'BUSINESS_INFO_ALREADY_EXIST',
    },
};
