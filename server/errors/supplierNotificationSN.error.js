export default {
    SUPPLIER_NOTIFICATION_NOT_FOUND: {
        code: 'SN001',
        message: 'SUPPLIER_NOTIFICATION_NOT_FOUND',
    },
    SUPPLIER_NOTIFICATION_ALREADY_EXIST: {
        code: 'SN002',
        message: 'SUPPLIER_NOTIFICATION_ALREADY_EXIST',
    },
};
