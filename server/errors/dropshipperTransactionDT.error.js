export default {
    DROPSHIPPER_TRANSACTION_NOT_FOUND: {
        code: 'DT001',
        message: 'DROPSHIPPER_TRANSACTION_NOT_FOUND',
    },
    DROPSHIPPER_TRANSACTION_ALREADY_EXIST: {
        code: 'DT002',
        message: 'DROPSHIPPER_TRANSACTION_ALREADY_EXIST',
    },
};
