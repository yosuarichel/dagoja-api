export default {
    COUNTRY_NOT_FOUND: {
        code: 'CR001',
        message: 'COUNTRY_NOT_FOUND',
    },
    COUNTRY_ALREADY_EXIST: {
        code: 'CR002',
        message: 'COUNTRY_ALREADY_EXIST',
    },
};
