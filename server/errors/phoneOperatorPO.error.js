export default {
    PHONE_OPERATOR_NOT_FOUND: {
        code: 'PO001',
        message: 'PHONE_OPERATOR_NOT_FOUND',
    },
    PHONE_OPERATOR_ALREADY_EXIST: {
        code: 'PO002',
        message: 'PHONE_OPERATOR_ALREADY_EXIST',
    },
};
