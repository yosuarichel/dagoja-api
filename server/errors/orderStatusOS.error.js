export default {
    ORDER_STATUS_NOT_FOUND: {
        code: 'OS001',
        message: 'ORDER_STATUS_NOT_FOUND',
    },
    ORDER_STATUS_ALREADY_EXIST: {
        code: 'OS002',
        message: 'ORDER_STATUS_ALREADY_EXIST',
    },
};
