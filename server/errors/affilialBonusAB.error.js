export default {
    AFFILIAL_BONUS_NOT_FOUND: {
        code: 'AB001',
        message: 'AFFILIAL_BONUS_NOT_FOUND',
    },
    AFFILIAL_BONUS_ALREADY_EXIST: {
        code: 'AB002',
        message: 'AFFILIAL_BONUS_ALREADY_EXIST',
    },
};
