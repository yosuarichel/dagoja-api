export default {
    ORDER_NOT_FOUND: {
        code: 'OR001',
        message: 'ORDER_NOT_FOUND',
    },
    ORDER_ALREADY_EXIST: {
        code: 'OR002',
        message: 'ORDER_ALREADY_EXIST',
    },
};
