export default {
    DISTRICT_NOT_FOUND: {
        code: 'DS001',
        message: 'DISTRICT_NOT_FOUND',
    },
    DISTRICT_ALREADY_EXIST: {
        code: 'DS002',
        message: 'DISTRICT_ALREADY_EXIST',
    },
};
