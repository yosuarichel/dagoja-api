export default {
    PAYMENT_METHOD_NOT_FOUND: {
        code: 'PM001',
        message: 'PAYMENT_METHOD_NOT_FOUND',
    },
    PAYMENT_METHOD_ALREADY_EXIST: {
        code: 'PM002',
        message: 'PAYMENT_METHOD_ALREADY_EXIST',
    },
};
