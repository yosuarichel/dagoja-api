export default {
    VILLAGE_NOT_FOUND: {
        code: 'VI001',
        message: 'VILLAGE_NOT_FOUND',
    },
    VILLAGE_ALREADY_EXIST: {
        code: 'VI002',
        message: 'VILLAGE_ALREADY_EXIST',
    },
};
