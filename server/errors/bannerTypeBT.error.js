export default {
    BANNER_TYPE_NOT_FOUND: {
        code: 'BT001',
        message: 'BANNER_TYPE_NOT_FOUND',
    },
    BANNER_TYPE_ALREADY_EXIST: {
        code: 'BT002',
        message: 'BANNER_TYPE_ALREADY_EXIST',
    },
};
