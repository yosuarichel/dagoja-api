export default {
    REGULATION_NOT_FOUND: {
        code: 'RG001',
        message: 'REGULATION_NOT_FOUND',
    },
    REGULATION_ALREADY_EXIST: {
        code: 'RG002',
        message: 'REGULATION_ALREADY_EXIST',
    },
};
