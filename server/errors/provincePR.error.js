export default {
    PROVINCE_NOT_FOUND: {
        code: 'PR001',
        message: 'PROVINCE_NOT_FOUND',
    },
    PROVINCE_ALREADY_EXIST: {
        code: 'PR002',
        message: 'PROVINCE_ALREADY_EXIST',
    },
};
