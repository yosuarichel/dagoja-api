export default {
    SUPPLIER_NOT_FOUND: {
        code: 'SP001',
        message: 'SUPPLIER_NOT_FOUND',
    },
    SUPPLIER_ALREADY_EXIST: {
        code: 'SP002',
        message: 'SUPPLIER_ALREADY_EXIST',
    },
};
