import generalGEError from './generalGE.error';
import adminADError from './adminAD.error';
import validationVLError from './validationVL.error';
import settingSTError from './settingST.error';
import adminRoleARError from './adminRoleAR.error';
import authAUError from './authAU.error';
import loginMethodLMError from './loginMethodLM.error';
import appVersionAVError from './appVersionAV.error';
import brandBRError from './brandBR.error';
import categoryCTError from './categoryCT.error';
import productPRError from './productPR.error';
import productGaleryPGError from './productGaleryPG.error';
import targetBonusTBError from './targetBonusTB.error';
import affilialBonusABError from './affilialBonusAB.error';
import supplierSPError from './supplierSP.error';
import phoneOperatorPOError from './phoneOperatorPO.error';
import bannerBNError from './bannerBN.error';
import bannerTypeBTError from './bannerTypeBT.error';
import dropshipperDRError from './dropshipperDR.error';
import occupationOCError from './occupationOC.error';
import countryCRError from './countryCR.error';
import provincePRError from './provincePR.error';
import cityCYError from './cityCY.error';
import districtDSError from './districtDS.error';
import villageVIError from './villageVI.error';
import bankBKError from './bankBK.error';
import businessInformationBIError from './businessInformationBI.error';
import ownerInformationOIError from './ownerInformationOI.error';
import supplierLogSLError from './supplierLogSL.error';
import regulationRGError from './regulationRG.error';
import transactionTRError from './trasnactionTR.error';
import supplierNotificationSNError from './supplierNotificationSN.error';
import myShopMSError from './myShopMS.error';
import dropshipperTransactionDTError from './dropshipperTransactionDT.error';
import dropshipperCartDCError from './dropshipperCartDC.error';
import dropshipperCustomerDCSError from './dropshipperCustomerDCS.error';
import paymentMethodPMError from './paymentMethodPM.error';
import orderORError from './orderOR.error';
import orderStatusOSError from './orderStatusOS.error';


export default {
    generalGEError,
    adminADError,
    validationVLError,
    settingSTError,
    adminRoleARError,
    authAUError,
    loginMethodLMError,
    appVersionAVError,
    brandBRError,
    categoryCTError,
    productPRError,
    productGaleryPGError,
    targetBonusTBError,
    affilialBonusABError,
    supplierSPError,
    phoneOperatorPOError,
    bannerBNError,
    bannerTypeBTError,
    dropshipperDRError,
    occupationOCError,
    countryCRError,
    provincePRError,
    cityCYError,
    districtDSError,
    villageVIError,
    bankBKError,
    businessInformationBIError,
    ownerInformationOIError,
    supplierLogSLError,
    regulationRGError,
    transactionTRError,
    supplierNotificationSNError,
    myShopMSError,
    dropshipperTransactionDTError,
    dropshipperCartDCError,
    dropshipperCustomerDCSError,
    paymentMethodPMError,
    orderORError,
    orderStatusOSError,
};
