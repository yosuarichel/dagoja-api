export default {
    DROPSHIPPER_NOT_FOUND: {
        code: 'DR001',
        message: 'DROPSHIPPER_NOT_FOUND',
    },
    DROPSHIPPER_ALREADY_EXIST: {
        code: 'DR002',
        message: 'DROPSHIPPER_ALREADY_EXIST',
    },
};
