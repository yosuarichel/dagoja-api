export default {
    TRANSACTION_NOT_FOUND: {
        code: 'TR001',
        message: 'TRANSACTION_NOT_FOUND',
    },
    TRANSACTION_ALREADY_EXIST: {
        code: 'TR002',
        message: 'TRANSACTION_ALREADY_EXIST',
    },
};
