export default {
    BANK_NOT_FOUND: {
        code: 'BK001',
        message: 'BANK_NOT_FOUND',
    },
    BANK_ALREADY_EXIST: {
        code: 'BK002',
        message: 'BANK_ALREADY_EXIST',
    },
};
