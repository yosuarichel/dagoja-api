export default {
    DROPSHIPPER_CART_NOT_FOUND: {
        code: 'DC001',
        message: 'DROPSHIPPER_CART_NOT_FOUND',
    },
    DROPSHIPPER_CART_ALREADY_EXIST: {
        code: 'DC002',
        message: 'DROPSHIPPER_CART_ALREADY_EXIST',
    },
    MAX_QUANTITY: {
        code: 'DC003',
        message: 'MAX_QUANTITY',
    },
    SOME_PRODUCT_IS_NOT_AVAIABLE: {
        code: 'DC004',
        message: 'SOME_PRODUCT_IS_NOT_AVAIABLE',
    },
};
