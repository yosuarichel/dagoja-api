export default {
    NOT_AUTHORIZED: {
        code: 'AU001',
        message: 'NOT_AUTHORIZED',
    },
    ACCOUNT_NOT_FOUND: {
        code: 'AU002',
        message: 'ACCOUNT_NOT_FOUND',
    },
    SESSION_REQUIRED: {
        code: 'AU003',
        message: 'SESSION_REQUIRED',
    },
    ACCOUNT_SUSPENDED: {
        code: 'AU004',
        message: 'ACCOUNT_SUSPENDED',
    },
    INVALID_CREDENTIAL: {
        code: 'AU005',
        message: 'INVALID_CREDENTIAL',
    },
    SESSION_EXPIRED: {
        code: 'AU006',
        message: 'SESSION_EXPIRED',
    },
    INACTIVE_ACCOUNT: {
        code: 'AU007',
        message: 'INACTIVE_ACCOUNT',
    },
    WAIT_APPROVAL: {
        code: 'AU008',
        message: 'WAIT_APPROVAL',
    },
    WAIT_VERIFICATION: {
        code: 'AU009',
        message: 'WAIT_VERIFICATION',
    },
    TOKEN_REQUIRED: {
        code: 'AU010',
        message: 'TOKEN_REQUIRED',
    },
};
