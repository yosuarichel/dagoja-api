export default {
    OCCUPATION_NOT_FOUND: {
        code: 'OC001',
        message: 'OCCUPATION_NOT_FOUND',
    },
    OCCUPATION_ALREADY_EXIST: {
        code: 'OC002',
        message: 'OCCUPATION_ALREADY_EXIST',
    },
};
