export default {
    CITY_NOT_FOUND: {
        code: 'CY001',
        message: 'CITY_NOT_FOUND',
    },
    CITY_ALREADY_EXIST: {
        code: 'CY002',
        message: 'CITY_ALREADY_EXIST',
    },
};
