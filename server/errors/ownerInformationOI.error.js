export default {
    OWNER_INFO_NOT_FOUND: {
        code: 'OI001',
        message: 'OWNER_INFO_NOT_FOUND',
    },
    OWNER_INFO_ALREADY_EXIST: {
        code: 'OI002',
        message: 'OWNER_INFO_ALREADY_EXIST',
    },
};
