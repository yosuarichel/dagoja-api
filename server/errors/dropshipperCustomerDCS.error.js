export default {
    DROPSHIPPER_CUSTOMER_NOT_FOUND: {
        code: 'DCS001',
        message: 'DROPSHIPPER_CUSTOMER_NOT_FOUND',
    },
    DROPSHIPPER_CUSTOMER_ALREADY_EXIST: {
        code: 'DCS002',
        message: 'DROPSHIPPER_CUSTOMER_ALREADY_EXIST',
    },
};
