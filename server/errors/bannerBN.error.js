export default {
    BANNER_NOT_FOUND: {
        code: 'BN001',
        message: 'BANNER_NOT_FOUND',
    },
    BANNER_ALREADY_EXIST: {
        code: 'BN002',
        message: 'BANNER_ALREADY_EXIST',
    },
};
