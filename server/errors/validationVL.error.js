export default {
    REQUIRED: {
        code: 'VAL001',
        message: 'REQUIRED',
    },
    MUST_BE_STRING: {
        code: 'VAL002',
        message: 'MUST_BE_STRING',
    },
    MUST_BE_INTEGER: {
        code: 'VAL003',
        message: 'MUST_BE_INTEGER',
    },
    MUST_BE_BOOLEAN: {
        code: 'VAL004',
        message: 'MUST_BE_BOOLEAN',
    },
    MUST_BE_EMAIL: {
        code: 'VAL005',
        message: 'MUST_BE_EMAIL',
    },
    MUST_BE_ENUM: {
        code: 'VAL006',
        message: 'MUST_BE_ENUM',
    },
    MUST_BE_ARRAY: {
        code: 'VAL007',
        message: 'MUST_BE_ARRAY',
    },
    ALREADY_IN_USE: {
        code: 'VAL008',
        message: 'ALREADY_IN_USE',
    },
    INVALID_VALUE: {
        code: 'VAL009',
        message: 'INVALID_VALUE',
    },
    NOT_FOUND: {
        code: 'VAL010',
        message: 'NOT_FOUND',
    },
    INVALID_PHONE_NUMBER: {
        code: 'VAL011',
        message: 'INVALID_PHONE_NUMBER',
    },
    VALUE_NOT_MATCH: {
        code: 'VAL012',
        message: 'VALUE_NOT_MATCH',
    },
    NOT_MATCH_MIN_CHARACTER: {
        code: 'VAL013',
        message: 'NOT_MATCH_MIN_CHARACTER',
    },
    NOT_MATCH_MAX_CHARACTER: {
        code: 'VAL014',
        message: 'NOT_MATCH_MAX_CHARACTER',
    },
};
