module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('dropshipper', {
        dropshipper_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        dropshipper_number: {
            type: Sequelize.STRING,
        },
        email: {
            type: Sequelize.STRING,
        },
        password: {
            type: Sequelize.STRING,
        },
        full_name: {
            type: Sequelize.STRING,
        },
        phone_number: {
            type: Sequelize.STRING,
        },
        birth_date: {
            type: Sequelize.STRING,
        },
        gender: {
            type: Sequelize.STRING,
        },
        nik: {
            type: Sequelize.STRING,
        },
        nik_image: {
            type: Sequelize.STRING,
        },
        nik_image_source: {
            type: Sequelize.STRING,
        },
        occupation_id: {
            type: Sequelize.INTEGER,
        },
        is_email_verif: {
            type: Sequelize.BOOLEAN,
        },
        email_token: {
            type: Sequelize.STRING,
        },
        is_phone_number_verif: {
            type: Sequelize.BOOLEAN,
        },
        profile_image_name: {
            type: Sequelize.STRING,
        },
        profile_image_source: {
            type: Sequelize.STRING,
        },
        province: {
            type: Sequelize.STRING,
        },
        city: {
            type: Sequelize.STRING,
        },
        district: {
            type: Sequelize.STRING,
        },
        vilage: {
            type: Sequelize.STRING,
        },
        postal_code: {
            type: Sequelize.STRING,
        },
        address: {
            type: Sequelize.STRING,
        },
        cash: {
            type: Sequelize.DOUBLE,
        },
        poin: {
            type: Sequelize.DOUBLE,
        },
        status: {
            type: Sequelize.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('dropshipper'),
};
