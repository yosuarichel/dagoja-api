module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('supplier_log', {
        supplier_log_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        supplier_id: {
            type: Sequelize.BIGINT,
        },
        type: {
            type: Sequelize.STRING,
        },
        action: {
            type: Sequelize.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('supplier_log'),
};
