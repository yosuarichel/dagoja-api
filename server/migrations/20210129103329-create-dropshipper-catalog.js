module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('dropshipper_catalog', {
        dropshipper_catalog_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        dropshipper_id: {
            type: Sequelize.BIGINT,
        },
        product_id: {
            type: Sequelize.BIGINT,
        },
        price: {
            type: Sequelize.DOUBLE,
        },
        discount: {
            type: Sequelize.INTEGER,
        },
        final_price: {
            type: Sequelize.DOUBLE,
        },
        is_disabled: {
            type: Sequelize.BOOLEAN,
        },
        is_coming_soon: {
            type: Sequelize.BOOLEAN,
        },
        status: {
            type: Sequelize.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('dropshipper_catalog'),
};
