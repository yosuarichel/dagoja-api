module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('my_shop', {
        my_shop_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        dropshipper_id: {
            type: Sequelize.BIGINT,
        },
        target_bonus_id: {
            type: Sequelize.INTEGER,
        },
        shop_name: {
            type: Sequelize.STRING,
        },
        link: {
            type: Sequelize.STRING,
        },
        contact: {
            type: Sequelize.STRING,
        },
        current_omzet: {
            type: Sequelize.DOUBLE,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('my_shop'),
};
