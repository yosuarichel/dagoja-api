module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('dropshipper_cart', {
        dropshipper_cart_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        dropshipper_id: {
            type: Sequelize.BIGINT,
        },
        product_id: {
            type: Sequelize.BIGINT,
        },
        product_variant_id: {
            type: Sequelize.BIGINT,
        },
        quantity: {
            type: Sequelize.INTEGER,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('dropshipper_cart'),
};
