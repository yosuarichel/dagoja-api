module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('supplier_transaction_log', {
        supplier_transaction_log_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        supplier_transaction_id: {
            type: Sequelize.BIGINT,
        },
        sender_bank: {
            type: Sequelize.STRING,
        },
        sender_account: {
            type: Sequelize.STRING,
        },
        recipient_bank: {
            type: Sequelize.STRING,
        },
        recipient_account: {
            type: Sequelize.STRING,
        },
        product_id: {
            type: Sequelize.BIGINT,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('supplier_transaction_log'),
};
