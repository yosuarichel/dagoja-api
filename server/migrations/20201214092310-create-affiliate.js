module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('affiliate', {
        affiliate_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        parent_id: {
            type: Sequelize.BIGINT,
        },
        child_id: {
            type: Sequelize.BIGINT,
        },
        level: {
            type: Sequelize.INTEGER,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('affiliate'),
};
