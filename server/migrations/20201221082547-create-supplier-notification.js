module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('supplier_notification', {
        supplier_notification_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        title: {
            type: Sequelize.STRING,
        },
        content: {
            type: Sequelize.TEXT,
        },
        image_name: {
            type: Sequelize.STRING,
        },
        image_source: {
            type: Sequelize.STRING,
        },
        is_global: {
            type: Sequelize.BOOLEAN,
        },
        is_segmented: {
            type: Sequelize.BOOLEAN,
        },
        segmented_type: {
            type: Sequelize.STRING,
        },
        status: {
            type: Sequelize.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('supplier_notification'),
};
