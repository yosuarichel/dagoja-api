module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('supplier', {
        supplier_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        supplier_number: {
            type: Sequelize.STRING,
        },
        name: {
            type: Sequelize.STRING,
        },
        email: {
            type: Sequelize.STRING,
        },
        username: {
            type: Sequelize.STRING,
        },
        password: {
            type: Sequelize.STRING,
        },
        phone_number: {
            type: Sequelize.STRING,
        },
        id_number: {
            type: Sequelize.STRING,
        },
        id_type: {
            type: Sequelize.STRING,
        },
        is_email_verif: {
            type: Sequelize.BOOLEAN,
        },
        is_phone_number_verif: {
            type: Sequelize.BOOLEAN,
        },
        profile_image_name: {
            type: Sequelize.STRING,
        },
        profile_image_source: {
            type: Sequelize.STRING,
        },
        province: {
            type: Sequelize.STRING,
        },
        city: {
            type: Sequelize.STRING,
        },
        district: {
            type: Sequelize.STRING,
        },
        vilage: {
            type: Sequelize.STRING,
        },
        postal_code: {
            type: Sequelize.STRING,
        },
        address: {
            type: Sequelize.STRING,
        },
        status: {
            type: Sequelize.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('supplier'),
};
