module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('order', {
        order_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        dropshipper_id: {
            type: Sequelize.BIGINT,
        },
        dropshipper_customer_id: {
            type: Sequelize.BIGINT,
        },
        delivery_option_id: {
            type: Sequelize.BIGINT,
        },
        order_number: {
            type: Sequelize.STRING,
        },
        delivery_fee: {
            type: Sequelize.DOUBLE,
        },
        total_item_price: {
            type: Sequelize.DOUBLE,
        },
        total_price: {
            type: Sequelize.DOUBLE,
        },
        payment_status: {
            type: Sequelize.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('order'),
};
