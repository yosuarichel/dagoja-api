module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('supplier_transaction', {
        supplier_transaction_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        supplier_id: {
            type: Sequelize.BIGINT,
        },
        transaction_number: {
            type: Sequelize.STRING,
        },
        transaction: {
            type: Sequelize.DOUBLE,
        },
        balance_before: {
            type: Sequelize.DOUBLE,
        },
        balance_after: {
            type: Sequelize.DOUBLE,
        },
        type: {
            type: Sequelize.STRING,
        },
        description: {
            type: Sequelize.TEXT,
        },
        op: {
            type: Sequelize.STRING,
        },
        status: {
            type: Sequelize.STRING,
        },
        settlement_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('supplier_transaction'),
};
