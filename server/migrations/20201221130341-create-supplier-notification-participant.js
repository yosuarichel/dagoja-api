module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('supplier_notification_participant', {
        supplier_notification_participant_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        supplier_id: {
            type: Sequelize.BIGINT,
        },
        is_read: {
            type: Sequelize.BOOLEAN,
        },
        is_remove: {
            type: Sequelize.BOOLEAN,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('supplier_notification_participant'),
};
