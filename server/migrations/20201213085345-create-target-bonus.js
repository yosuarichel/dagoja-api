module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('target_bonus', {
        target_bonus_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        name: {
            type: Sequelize.STRING,
        },
        target: {
            type: Sequelize.DOUBLE,
        },
        bonus_percentage: {
            type: Sequelize.DOUBLE,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('target_bonus'),
};
