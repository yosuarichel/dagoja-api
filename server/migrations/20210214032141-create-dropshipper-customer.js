module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('dropshipper_customer', {
        dropshipper_customer_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        dropshipper_id: {
            type: Sequelize.BIGINT,
        },
        full_name: {
            type: Sequelize.STRING,
        },
        phone_number: {
            type: Sequelize.STRING,
        },
        province: {
            type: Sequelize.STRING,
        },
        city: {
            type: Sequelize.STRING,
        },
        district: {
            type: Sequelize.STRING,
        },
        vilage: {
            type: Sequelize.STRING,
        },
        postal_code: {
            type: Sequelize.STRING,
        },
        address: {
            type: Sequelize.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('dropshipper_customer'),
};
