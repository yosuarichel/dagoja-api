module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('owner_information', {
        owner_information_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        supplier_id: {
            type: Sequelize.BIGINT,
        },
        name: {
            type: Sequelize.STRING,
        },
        occupation_id: {
            type: Sequelize.INTEGER,
        },
        nik_number: {
            type: Sequelize.STRING,
        },
        nik_image_name: {
            type: Sequelize.STRING,
        },
        nik_image_source: {
            type: Sequelize.STRING,
        },
        npwp_number: {
            type: Sequelize.STRING,
        },
        npwp_image_name: {
            type: Sequelize.STRING,
        },
        npwp_image_source: {
            type: Sequelize.STRING,
        },
        bank_id: {
            type: Sequelize.INTEGER,
        },
        bank_account: {
            type: Sequelize.STRING,
        },
        province: {
            type: Sequelize.STRING,
        },
        city: {
            type: Sequelize.STRING,
        },
        district: {
            type: Sequelize.STRING,
        },
        village: {
            type: Sequelize.STRING,
        },
        postal_code: {
            type: Sequelize.STRING,
        },
        address: {
            type: Sequelize.STRING,
        },
        status: {
            type: Sequelize.STRING,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('owner_information'),
};
