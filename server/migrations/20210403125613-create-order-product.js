module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('order_product', {
        order_product_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        order_id: {
            type: Sequelize.BIGINT,
        },
        product_id: {
            type: Sequelize.BIGINT,
        },
        variant_id: {
            type: Sequelize.BIGINT,
        },
        brand_id: {
            type: Sequelize.BIGINT,
        },
        category_id: {
            type: Sequelize.BIGINT,
        },
        supplier_id: {
            type: Sequelize.BIGINT,
        },

        purchased_quantity: {
            type: Sequelize.BIGINT,
        },

        brand_name: {
            type: Sequelize.STRING,
        },
        brand_image_source: {
            type: Sequelize.STRING,
        },

        category_name: {
            type: Sequelize.STRING,
        },
        category_image_source: {
            type: Sequelize.STRING,
        },

        product_number: {
            type: Sequelize.STRING,
        },
        name: {
            type: Sequelize.STRING,
        },
        supplier_price: {
            type: Sequelize.DOUBLE,
        },
        ppn_percentage: {
            type: Sequelize.DOUBLE,
        },
        markup_percentage: {
            type: Sequelize.DOUBLE,
        },
        final_price: {
            type: Sequelize.DOUBLE,
        },
        quantity: {
            type: Sequelize.INTEGER,
        },
        remaining_quantity: {
            type: Sequelize.INTEGER,
        },
        weight: {
            type: Sequelize.DOUBLE,
        },
        condition: {
            type: Sequelize.STRING,
        },
        minimum_order: {
            type: Sequelize.INTEGER,
        },
        is_discount: {
            type: Sequelize.BOOLEAN,
        },
        discount_percentage: {
            type: Sequelize.DOUBLE,
        },
        is_warranty: {
            type: Sequelize.BOOLEAN,
        },
        warranty_expired_at: {
            type: Sequelize.DATE,
        },
        galery: {
            type: Sequelize.ARRAY(Sequelize.JSON),
        },
        description: {
            type: Sequelize.TEXT,
        },
        status: {
            type: Sequelize.STRING,
        },
        is_disabled: {
            type: Sequelize.BOOLEAN,
        },
        is_coming_soon: {
            type: Sequelize.BOOLEAN,
        },
        os: {
            type: Sequelize.ARRAY(Sequelize.STRING),
        },

        variant_sku: {
            type: Sequelize.STRING,
        },
        variant_name: {
            type: Sequelize.STRING,
        },
        variant_option: {
            type: Sequelize.STRING,
        },
        variant_value: {
            type: Sequelize.STRING,
        },
        variant_quantity: {
            type: Sequelize.INTEGER,
        },
        variant_remaining_quantity: {
            type: Sequelize.INTEGER,
        },

        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('order_product'),
};
