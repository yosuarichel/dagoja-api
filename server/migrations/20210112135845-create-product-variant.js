module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('product_variant', {
        product_variant_id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        product_id: {
            type: Sequelize.BIGINT,
        },
        sku: {
            type: Sequelize.STRING,
        },
        variant_name: {
            type: Sequelize.STRING,
        },
        variant_option: {
            type: Sequelize.STRING,
        },
        variant_value: {
            type: Sequelize.STRING,
        },
        quantity: {
            type: Sequelize.INTEGER,
        },
        remaining_quantity: {
            type: Sequelize.INTEGER,
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    }),
    // eslint-disable-next-line no-unused-vars
    down: (queryInterface, Sequelize) => queryInterface.dropTable('product_variant'),
};
