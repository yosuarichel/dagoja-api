/* eslint-disable camelcase */
/* eslint-disable max-len */
// import moment from 'moment';
// import db from '../../config/sequelize';
// import request from '../model/request.schema';

require('dotenv').config();

// const CryptoJS = require('crypto-js');
// const rp = require('request-promise-core');
// const jwt = require('jsonwebtoken');
const nodeMailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const cloudinary = require('cloudinary').v2;

const uploadCloudinary = async (path, fileName, folder, tags) => {
    cloudinary.config({
        cloud_name: process.env.CLOUDINARY_NAME,
        api_key: process.env.CLOUDINARY_API_KEY,
        api_secret: process.env.CLOUDINARY_API_SECRET,
    });
    const options = {
        public_id: fileName,
        unique_filename: false,
        folder: `public/original/${folder}`,
    };
    if (tags) {
        options.tags = tags;
    }
    // console.log(options);
    return cloudinary.uploader.upload(
        path,
        options, // directory and tags are optional
    ).then((result) => {
        const data = result;
        const x = data.public_id.split('/').length;
        data.unique_name = data.public_id.split('/')[x - 1];
        return data;
    }).catch(() => null);
};

const removeCloudinary = async (publicId) => {
    cloudinary.config({
        cloud_name: process.env.CLOUDINARY_NAME,
        api_key: process.env.CLOUDINARY_API_KEY,
        api_secret: process.env.CLOUDINARY_API_SECRET,
    });
    return cloudinary.uploader.destroy(
        publicId,
    ).then((result) => result).catch(() => null);
};

const sendEmail = async (layout, {
    sender, recipient, subject, context,
}) => {
    const transporter = nodeMailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        secure: true,
        auth: {
            user: process.env.EMAIL_USERNAME, // generated ethereal user
            pass: process.env.EMAIL_PASSWORD, // generated ethereal password
        },
    });
    const options = {
        viewEngine: {
            extname: '.hbs', // handlebars extension
            layoutsDir: './server/resources/templates/email/', // location of handlebars templates
            // defaultLayout: 'linigame-report', // name of main template
            defaultLayout: layout,
            partialsDir: './server/resources/templates/email/', // location of your subtemplates aka. header, footer etc
        },
        viewPath: './server/resources/templates/email/',
        extName: '.hbs',
    };
    transporter.use('compile', hbs(options));

    const mailData = {
        from: sender,
        // to: ['yosuasmjtk86@gmail.com', 'albert.darmali@codify.id', 'jason.japutra@codify.id'],
        to: recipient,
        subject,
        template: layout,
        context,
    };
    return transporter.sendMail(mailData);
};


export default {
    uploadCloudinary,
    removeCloudinary,
    sendEmail,
};
