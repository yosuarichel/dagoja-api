/* eslint-disable camelcase */
/* eslint-disable max-len */
import db from '../../config/sequelize';
import Response from '../response/response';
import errorCodes from '../errors/index.error';
// import request from '../model/request.schema';

require('dotenv').config();

const Setting = db.setting;

class Initialization {
    static async getSetting(req, res, next) {
        const response = new Response(req, res);
        let settingData = null;
        try {
            settingData = await Setting.findByPk(1, {
                raw: true,
            });
        } catch (e) {
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        }
        if (!settingData) {
            return response.failResponse(404, errorCodes.settingSTError.SETTING_NOT_FOUND);
        }
        req.settingData = settingData;
        return next();
    }
}

export default Initialization;
