/* eslint-disable camelcase */
/* eslint-disable max-len */
import moment from 'moment';
import db from '../../config/sequelize';
import Response from '../response/response';
import errorCodes from '../errors/index.error';
// import request from '../model/request.schema';

require('dotenv').config();

const adminSession = db.admin_session;
const Admin = db.admin;
const Supplier = db.supplier;
const supplierSession = db.supplier_session;
const Dropshipper = db.dropshipper;
const dropshipperSession = db.dropshipper_session;
// const phoneOperator = db.phone_operator;
// const CryptoJS = require('crypto-js');
// const rp = require('request-promise-core');
const jwt = require('jsonwebtoken');

class CustomMiddleware {
    static async checkTokenJwt(req, res, next, types) {
        const response = new Response(req, res);
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            const jwtToken = req.headers.authorization.split(' ')[1];
            return jwt.verify(jwtToken, process.env.JWT_SECRET, (err, decoded) => {
                if (err) {
                    Object.assign(errorCodes.authAUError.NOT_AUTHORIZED, {
                        detail: err.name,
                    });
                    return response.failResponse(401, errorCodes.authAUError.NOT_AUTHORIZED);
                }
                if (!decoded.session) {
                    return response.failResponse(401, errorCodes.authAUError.NOT_AUTHORIZED);
                }
                if (!types.includes((decoded.type).toLowerCase())) {
                    return response.failResponse(401, errorCodes.authAUError.NOT_AUTHORIZED);
                }
                // if (decoded.session !== req.sessionPlatformData.session) {
                //     const response = new Response(401, errorCodes.authAUError.NOT_AUTHORIZED);
                //     return response.failResponse(req, res);
                // }
                req.decoded = decoded;
                return next();
            });
        }
        return response.failResponse(401, errorCodes.authAUError.NOT_AUTHORIZED);
    }

    static async checkAdminSession(req, res, next) {
        const { settingData } = req;
        const response = new Response(req, res);
        let sessionAdmin = null;
        if (req.body.session) {
            sessionAdmin = req.body.session;
        }
        if (req.query.session) {
            sessionAdmin = req.query.session;
        }
        if (req.headers.session) {
            sessionAdmin = req.headers.session;
        }
        if (req.decoded.session) {
            sessionAdmin = req.decoded.session;
        }
        if (!sessionAdmin) {
            return response.failResponse(400, errorCodes.authAUError.SESSION_REQUIRED);
        }

        let sessionAdminData = null;
        try {
            sessionAdminData = await adminSession.findOne({
                where: {
                    session: sessionAdmin,
                },
            });
        } catch (e) {
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        }
        if (!sessionAdminData) {
            return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
        }

        let adminData = null;
        try {
            adminData = await Admin.findOne({
                where: {
                    admin_id: sessionAdminData.admin_id,
                },
                raw: true,
            });
        } catch (e) {
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        }
        if (!adminData) {
            return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
        }

        Object.assign(adminData, {
            session: sessionAdminData.session,
        });
        req.adminData = adminData;

        const now = moment(Date.now()).tz('Asia/Jakarta').format();
        const expiry = moment(sessionAdminData.expiry_date).tz('Asia/Jakarta');
        const diff = expiry.diff(now);
        if (diff <= 0) {
            return response.failResponse(401, errorCodes.authAUError.SESSION_EXPIRED);
        }
        const split = settingData.admin_session_expiry.split(';');
        const valueExp = split[0];
        const intervalExp = split[1];
        const sessionExpiry = moment(now).add(valueExp, `${intervalExp}`).tz('Asia/Jakarta');
        const diffNew = sessionExpiry.diff(now) / 1000;

        await sessionAdminData.update({
            expire_value: diffNew,
            expired_at: moment(sessionExpiry).tz('Asia/Jakarta').format(),
        });
        return next();
    }

    static async sanitizePhoneNumber(req, value) {
        let phone = value;
        const str = phone.toString();
        const check1 = str.substr(0, 1);
        const check3 = str.substr(0, 2);
        if (check1 === '+') {
            const check2 = str.substr(1, 2);
            if (check2 === '62') {
                phone = `0${str.substr(3)}`;
            } else {
                // const response = new Response(400, errorCodes.validationVLError.INVALID_PHONE_NUMBER);
                // return response.failResponse(req, res);
                return null;
            }
        }
        if (check3 === '62') {
            phone = `0${str.substr(2)}`;
        }
        if (req.body.phone_number) {
            req.body.phone_number = phone;
        }
        if (req.query.phone_number) {
            req.query.phone_number = phone;
        }
        const codePhone = phone.substr(0, 4).toString();
        return codePhone;
    }

    static async checkSupplierSession(req, res, next) {
        const { settingData } = req;
        const response = new Response(req, res);
        let sessionSupplier = null;
        if (req.body.session) {
            sessionSupplier = req.body.session;
        }
        if (req.query.session) {
            sessionSupplier = req.query.session;
        }
        if (req.headers.session) {
            sessionSupplier = req.headers.session;
        }
        if (req.decoded.session) {
            sessionSupplier = req.decoded.session;
        }
        if (!sessionSupplier) {
            return response.failResponse(400, errorCodes.authAUError.SESSION_REQUIRED);
        }

        let sessionSupplierData = null;
        try {
            sessionSupplierData = await supplierSession.findOne({
                where: {
                    session: sessionSupplier,
                },
            });
        } catch (e) {
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        }
        if (!sessionSupplierData) {
            return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
        }
        if (sessionSupplierData.status === 'logged-out') {
            return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
        }

        const now = moment(Date.now()).tz('Asia/Jakarta').format();
        const expiry = moment(sessionSupplierData.expiry_date).tz('Asia/Jakarta');
        const diff = expiry.diff(now);
        if (diff <= 0) {
            return response.failResponse(401, errorCodes.authAUError.SESSION_EXPIRED);
        }

        let supplierData = null;
        try {
            supplierData = await Supplier.findOne({
                where: {
                    supplier_id: sessionSupplierData.supplier_id,
                },
                raw: true,
            });
        } catch (e) {
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        }
        if (!supplierData) {
            return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
        }
        if (supplierData.status === 'inactive') {
            return response.failResponse(401, errorCodes.authAUError.ACCOUNT_SUSPENDED);
        }

        Object.assign(supplierData, {
            session: sessionSupplierData.session,
        });
        req.supplierData = supplierData;

        const split = settingData.supplier_session_expiry.split(';');
        const valueExp = split[0];
        const intervalExp = split[1];
        const sessionExpiry = moment(now).add(valueExp, `${intervalExp}`).tz('Asia/Jakarta');
        const diffNew = sessionExpiry.diff(now) / 1000;

        await sessionSupplierData.update({
            expire_value: diffNew,
            expired_at: moment(sessionExpiry).tz('Asia/Jakarta').format(),
        });
        return next();
    }

    static async checkDropshipperSession(req, res, next) {
        const { settingData } = req;
        const response = new Response(req, res);
        let sessionDropshipper = null;
        if (req.body.session) {
            sessionDropshipper = req.body.session;
        }
        if (req.query.session) {
            sessionDropshipper = req.query.session;
        }
        if (req.headers.session) {
            sessionDropshipper = req.headers.session;
        }
        if (req.decoded.session) {
            sessionDropshipper = req.decoded.session;
        }
        if (!sessionDropshipper) {
            return response.failResponse(400, errorCodes.authAUError.SESSION_REQUIRED);
        }

        let sessionDropshipperData = null;
        try {
            sessionDropshipperData = await dropshipperSession.findOne({
                where: {
                    session: sessionDropshipper,
                },
            });
        } catch (e) {
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        }
        if (!sessionDropshipperData) {
            return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
        }
        if (sessionDropshipperData.status === 'logged-out') {
            return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
        }
        const now = moment(Date.now()).tz('Asia/Jakarta').format();
        const expiry = moment(sessionDropshipperData.expiry_date).tz('Asia/Jakarta');
        const diff = expiry.diff(now);
        if (diff <= 0) {
            return response.failResponse(401, errorCodes.authAUError.SESSION_EXPIRED);
        }

        let dropshipperData = null;
        try {
            dropshipperData = await Dropshipper.findOne({
                where: {
                    dropshipper_id: sessionDropshipperData.dropshipper_id,
                },
                raw: true,
            });
        } catch (e) {
            return response.failResponse(500, errorCodes.generalGEError.SOMETHING_WRONG, e);
        }
        if (!dropshipperData) {
            return response.failResponse(401, errorCodes.authAUError.INVALID_CREDENTIAL);
        }
        if (dropshipperData.status === 'inactive') {
            return response.failResponse(401, errorCodes.authAUError.ACCOUNT_SUSPENDED);
        }

        Object.assign(dropshipperData, {
            session: sessionDropshipperData.session,
        });
        req.dropshipperData = dropshipperData;

        const split = settingData.dropshipper_session_expiry.split(';');
        const valueExp = split[0];
        const intervalExp = split[1];
        const sessionExpiry = moment(now).add(valueExp, `${intervalExp}`).tz('Asia/Jakarta');
        const diffNew = sessionExpiry.diff(now) / 1000;

        await sessionDropshipperData.update({
            expire_value: diffNew,
            expired_at: moment(sessionExpiry).tz('Asia/Jakarta').format(),
        });
        return next();
    }
}

export default CustomMiddleware;
