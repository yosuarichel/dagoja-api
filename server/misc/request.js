import rp from 'request-promise-native';

const Xendit = require('xendit-node');

const xenditInit = new Xendit({
    secretKey: process.env.XENDIT_KEY_TOKEN,
});

class Request {
    constructor() {
        this.options = {
            resolveWithFullResponse: true,
        };
        this.xenditInit = null;
    }

    async xenditRequest(path, options) {
        Object.assign(this.options, options);
        Object.assign(this.options, {
            auth: {
                user: process.env.XENDIT_KEY_TOKEN,
                pass: '',
            },
        });
        return new Promise((resolve, reject) => rp(`${process.env.XENDIT_URL}${path}`, this.options).then((result) => resolve(result)).catch((error) => reject(error)));
    }

    xenditNode() {
        this.xenditInit = xenditInit;
        return this.xenditInit;
    }
}

export default Request;
