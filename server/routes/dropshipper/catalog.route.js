import express from 'express';
import productCtrl from '../../controllers/dropshipper/catalog.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], productCtrl.listProductCatalog)
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
                validation.productValidation.validate('addCatalog'),
            ], productCtrl.addToCatalog);

        this.router.route('/:productCatalogId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], productCtrl.productCatalogDetail)
            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
                validation.productValidation.validate('updateProductCatalog'),
            ], productCtrl.updateProductCatalog)
            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], productCtrl.removeProductCatalog);

        this.router.route('/:productCatalogId/max-discount')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], productCtrl.catalogMaxDiscount);

        return this.router;
    }
}

export default Route;
