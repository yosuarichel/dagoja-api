import express from 'express';
import bannerCtrl from '../../controllers/dropshipper/banner.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], bannerCtrl.list);

        this.router.route('/:bannerId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], bannerCtrl.get);

        /** Load when API with Id route parameter is hit */
        this.router.param('bannerId', bannerCtrl.load);

        return this.router;
    }
}

export default Route;
