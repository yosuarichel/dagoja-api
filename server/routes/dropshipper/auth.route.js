import express from 'express';
import authCtrl from '../../controllers/dropshipper/auth.controller';
import validation from '../../validations/index.validation';
import upload from '../../../config/multer/upload';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/register')
            .post([
                upload.fields([{
                    name: 'dropshipper_image',
                    maxCount: 1,
                }, {
                    name: 'dropshipper_nik_image',
                    maxCount: 1,
                }]),
                validation.authValidation.validate('registerDropshipper'),
            ], authCtrl.registerDropshipper);

        this.router.route('/login')
            .post([
                validation.authValidation.validate('loginDropshipper'),
            ], authCtrl.loginDropshipper);

        this.router.route('/verification/email/:token')
            .get([
            ], authCtrl.emailVerification);

        this.router.route('/logout')
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], authCtrl.logoutDropshipper);

        this.router.route('/verification/email')
            .post([
                validation.authValidation.validate('sendEmailDropshipper'),
            ], authCtrl.sendEmailVerification);

        this.router.route('/profile')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], authCtrl.profileDropshipper);

        return this.router;
    }
}

export default Route;
