import express from 'express';
import occupationCtrl from '../../controllers/dropshipper/occupation.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], occupationCtrl.list);

        return this.router;
    }
}

export default Route;
