import express from 'express';
import productCtrl from '../../controllers/dropshipper/product.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], productCtrl.list);

        this.router.route('/:productId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], productCtrl.get);

        /** Load when API with Id route parameter is hit */
        this.router.param('productId', productCtrl.load);

        return this.router;
    }
}

export default Route;
