import express from 'express';
import callbackCtrl from '../../controllers/dropshipper/callback.controller';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/ovo')
            .post([
            // (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
            // Middleware.checkDropshipperSession,
            ], callbackCtrl.ovoCallback);

        return this.router;
    }
}

export default Route;
