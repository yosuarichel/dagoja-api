import express from 'express';
import orderCtrl from '../../controllers/dropshipper/order.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], orderCtrl.list);

        this.router.route('/:orderId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], orderCtrl.get);

        /** Load when API with Id route parameter is hit */
        this.router.param('orderId', orderCtrl.load);

        return this.router;
    }
}

export default Route;
