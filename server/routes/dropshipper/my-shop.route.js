import express from 'express';
import myShopCtrl from '../../controllers/dropshipper/my-shop.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
                validation.myShopValidation.validate('createMyShop'),
            ], myShopCtrl.create);

        this.router.route('/shop')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], myShopCtrl.get);

        this.router.route('/:myShopId')
            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
                validation.myShopValidation.validate('updateMyShop'),
            ], myShopCtrl.update);

        /** Load when API with Id route parameter is hit */
        this.router.param('myShopId', myShopCtrl.load);

        return this.router;
    }
}

export default Route;
