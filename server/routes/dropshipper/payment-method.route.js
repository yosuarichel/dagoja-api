import express from 'express';
import paymentMethodCtrl from '../../controllers/dropshipper/payment-method.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], paymentMethodCtrl.list);

        this.router.route('/channels')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], paymentMethodCtrl.getPaymentChannels);

        this.router.route('/:paymentMethodId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], paymentMethodCtrl.get);

        /** Load when API with Id route parameter is hit */
        this.router.param('paymentMethodId', paymentMethodCtrl.load);

        return this.router;
    }
}

export default Route;
