import express from 'express';
import drophsipperCustomerCtrl from '../../controllers/dropshipper/customer.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], drophsipperCustomerCtrl.list)
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
                validation.dropshipperCustomerValidation.validate('createDropshipperCustomer'),
            ], drophsipperCustomerCtrl.create);

        this.router.route('/:dropshipperCustomerId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], drophsipperCustomerCtrl.get)
            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
                validation.dropshipperCustomerValidation.validate('updateDropshipperCustomer'),
            ], drophsipperCustomerCtrl.update)
            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], drophsipperCustomerCtrl.remove);

        /** Load when API with Id route parameter is hit */
        // this.router.param('dropshipperCustomerId', drophsipperCustomerCtrl.load);

        return this.router;
    }
}

export default Route;
