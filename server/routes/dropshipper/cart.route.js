import express from 'express';
import cartCtrl from '../../controllers/dropshipper/cart.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], cartCtrl.list)
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
                validation.dropshipperCartValidation.validate('createDropshipperCart'),
            ], cartCtrl.increase);

        this.router.route('/decrease')
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
                validation.dropshipperCartValidation.validate('decreaseQuantityProduct'),
            ], cartCtrl.decrease);

        this.router.route('/:cartId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], cartCtrl.get)
            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], cartCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('cartId', cartCtrl.load);

        return this.router;
    }
}

export default Route;
