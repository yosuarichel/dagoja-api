import express from 'express';
import paymentCtrl from '../../controllers/dropshipper/payment.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/e-wallet')
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
                // validation.dropshipperCartValidation.validate('createDropshipperCart'),
            ], paymentCtrl.createPayment);

        return this.router;
    }
}

export default Route;
