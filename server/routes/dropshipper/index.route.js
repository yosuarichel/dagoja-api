import express from 'express';
import AuthRoute from './auth.route';
import ProductRoute from './product.route';
import OccupationRoute from './occupation.route';
import CategoryRoute from './category.route';
import CatalogRoute from './catalog.route';
import BannerRoute from './banner.route';
import MyShopRoute from './my-shop.route';
import CartRoute from './cart.route';
import CustomerRoute from './customer.route';
import PaymentMethodRoute from './payment-method.route';
import PaymentRoute from './payment.route';
import CallbackRoute from './callback.route';
import OrderRoute from './order.route';

const router = express.Router();

export default class DropshipperRoute {
    constructor() {
        this.router = router;

        this.router.use('/auth', new AuthRoute());
        this.router.use('/product', new ProductRoute());
        this.router.use('/occupation', new OccupationRoute());
        this.router.use('/category', new CategoryRoute());
        this.router.use('/catalog', new CatalogRoute());
        this.router.use('/banner', new BannerRoute());
        this.router.use('/my-shop', new MyShopRoute());
        this.router.use('/cart', new CartRoute());
        this.router.use('/customer', new CustomerRoute());
        this.router.use('/payment-method', new PaymentMethodRoute());
        this.router.use('/payment', new PaymentRoute());
        this.router.use('/callback', new CallbackRoute());
        this.router.use('/order', new OrderRoute());

        return this.router;
    }
}
