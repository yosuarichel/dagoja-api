import express from 'express';
import categoryCtrl from '../../controllers/dropshipper/category.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], categoryCtrl.list);

        this.router.route('/catalog/category-of-product')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], categoryCtrl.getCategoryOfCatalogProduct);

        this.router.route('/category-of-product')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['dropshipper']),
                Middleware.checkDropshipperSession,
            ], categoryCtrl.getCategoryOfProduct);

        return this.router;
    }
}

export default Route;
