import express from 'express';
import profileCtrl from '../../controllers/supplier/profile.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/account')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], profileCtrl.profileAccount);

        this.router.route('/business')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], profileCtrl.profileBusiness);

        this.router.route('/owner')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], profileCtrl.profileOwner);

        this.router.route('/status')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], profileCtrl.status);

        return this.router;
    }
}

export default Route;
