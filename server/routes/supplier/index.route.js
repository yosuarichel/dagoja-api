import express from 'express';
import AuthRoute from './auth.route';
import OccupationRoute from './occupation.route';
import ProductRoute from './product.route';
import RegulationRoute from './regulation.route';
import ProfileRoute from './profile.route';
import TransactionRoute from './transaction.route';

const router = express.Router();

export default class SupplierRoute {
    constructor() {
        this.router = router;

        this.router.use('/auth', new AuthRoute());
        this.router.use('/product', new ProductRoute());
        this.router.use('/occupation', new OccupationRoute());
        this.router.use('/regulation', new RegulationRoute());
        this.router.use('/profile', new ProfileRoute());
        this.router.use('/transaction', new TransactionRoute());

        return this.router;
    }
}
