import express from 'express';
import authCtrl from '../../controllers/supplier/auth.controller';
import validation from '../../validations/index.validation';
import upload from '../../../config/multer/upload';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/register')
            .post([
                upload.fields([{
                    name: 'supplier_image',
                    maxCount: 1,
                }, {
                    name: 'business_siup_image',
                    maxCount: 1,
                }, {
                    name: 'business_npwp_image',
                    maxCount: 1,
                }, {
                    name: 'owner_nik_image',
                    maxCount: 1,
                }, {
                    name: 'owner_npwp_image',
                    maxCount: 1,
                }]),
                validation.authValidation.validate('registerSupplier'),
            ], authCtrl.registerSupplier);
        this.router.route('/login')
            .post([
                validation.authValidation.validate('loginSupplier'),
            ], authCtrl.loginSupplier);
        this.router.route('/logout')
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], authCtrl.logoutSupplier);

        return this.router;
    }
}

export default Route;
