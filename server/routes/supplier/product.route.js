import express from 'express';
import productCtrl from '../../controllers/supplier/product.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], productCtrl.list);

        this.router.route('/stats')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], productCtrl.stats);

        this.router.route('/:productId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], productCtrl.get);

        return this.router;
    }
}

export default Route;
