import express from 'express';
import transactionCtrl from '../../controllers/supplier/transaction.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], transactionCtrl.history);

        this.router.route('/balance')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], transactionCtrl.balance);

        this.router.route('/:transactionId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], transactionCtrl.historyDetail);

        return this.router;
    }
}

export default Route;
