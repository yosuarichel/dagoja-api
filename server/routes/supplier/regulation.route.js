import express from 'express';
import regulationCtrl from '../../controllers/supplier/regulation.controller';
// import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/:type')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['supplier']),
                Middleware.checkSupplierSession,
            ], regulationCtrl.list);

        return this.router;
    }
}

export default Route;
