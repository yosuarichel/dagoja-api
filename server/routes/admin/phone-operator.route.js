import express from 'express';
import phoneOperatorCtrl from '../../controllers/admin/phone-operator.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], phoneOperatorCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.phoneOperatorValidation.validate('createPhoneOperator'),
            ], phoneOperatorCtrl.create);

        this.router.route('/:phoneOperatorId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], phoneOperatorCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.phoneOperatorValidation.validate('updatePhoneOperator'),
            ], phoneOperatorCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], phoneOperatorCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('phoneOperatorId', phoneOperatorCtrl.load);

        return this.router;
    }
}

export default Route;
