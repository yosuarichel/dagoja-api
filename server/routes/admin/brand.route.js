import express from 'express';
import brandCtrl from '../../controllers/admin/brand.controller';
import validation from '../../validations/index.validation';
import brandImageUploader from '../../../config/multer/brand';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], brandCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                brandImageUploader.single('brand_image'),
                validation.brandValidation.validate('createBrand'),
            ], brandCtrl.create);

        this.router.route('/:brandId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], brandCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                brandImageUploader.single('brand_image'),
                validation.brandValidation.validate('updateBrand'),
            ], brandCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], brandCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('brandId', brandCtrl.load);

        return this.router;
    }
}

export default Route;
