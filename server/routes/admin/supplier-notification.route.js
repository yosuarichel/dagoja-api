import express from 'express';
import supplierNotificationCtrl from '../../controllers/admin/supplier-notification.controller';
import validation from '../../validations/index.validation';
import upload from '../../../config/multer/supplier-notification';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierNotificationCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                upload.single('supplier_notification_image'),
                validation.supplierNotificationValidation.validate('createSupplierNotification'),
            ], supplierNotificationCtrl.create);

        this.router.route('/publish')
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierNotificationCtrl.broadcastNotification);

        this.router.route('/:supplierNotificationId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierNotificationCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                upload.single('supplier_notification_image'),
                validation.supplierNotificationValidation.validate('updateSupplierNotification'),
            ], supplierNotificationCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierNotificationCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('supplierNotificationId', supplierNotificationCtrl.load);

        return this.router;
    }
}

export default Route;
