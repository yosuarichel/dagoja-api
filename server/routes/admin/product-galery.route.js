import express from 'express';
import productGaleryCtrl from '../../controllers/admin/product-galery.controller';
import validation from '../../validations/index.validation';
import productGaleryUploader from '../../../config/multer/product-galery';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], productGaleryCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                productGaleryUploader.array('product_image', 5),
                validation.productGaleryValidation.validate('createProductGalery'),
            ], productGaleryCtrl.create);

        this.router.route('/:productGaleryId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], productGaleryCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                productGaleryUploader.single('product_image'),
                validation.productGaleryValidation.validate('updateProductGalery'),
            ], productGaleryCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], productGaleryCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('productGaleryId', productGaleryCtrl.load);

        return this.router;
    }
}

export default Route;
