import express from 'express';
import adminRoleCtrl from '../../controllers/admin/admin-role.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], adminRoleCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.adminRoleValidation.validate('createAdminRole'),
            ], adminRoleCtrl.create);

        this.router.route('/:adminRoleId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], adminRoleCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.adminRoleValidation.validate('updateAdminRole'),
            ], adminRoleCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], adminRoleCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('adminRoleId', adminRoleCtrl.load);

        return this.router;
    }
}

export default Route;
