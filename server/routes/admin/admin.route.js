import express from 'express';
import adminCtrl from '../../controllers/admin/admin.controller';
import validation from '../../validations/index.validation';
import adminProfileUploader from '../../../config/multer/admin-profile';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], adminCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                adminProfileUploader.single('profile_image'),
                validation.adminValidation.validate('createAdmin'),
            ], adminCtrl.create);

        this.router.route('/:adminId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], adminCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                adminProfileUploader.single('profile_image'),
                validation.adminValidation.validate('updateAdmin'),
            ], adminCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], adminCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('adminId', adminCtrl.load);

        return this.router;
    }
}

export default Route;
