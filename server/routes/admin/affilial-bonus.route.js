import express from 'express';
import affilialBonusCtrl from '../../controllers/admin/affilial-bonus.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], affilialBonusCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.affilialBonusValidation.validate('createAffilialBonus'),
            ], affilialBonusCtrl.create);

        this.router.route('/:affilialBonusId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], affilialBonusCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.affilialBonusValidation.validate('updateAffilialBonus'),
            ], affilialBonusCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], affilialBonusCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('affilialBonusId', affilialBonusCtrl.load);

        return this.router;
    }
}

export default Route;
