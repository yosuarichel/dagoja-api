import express from 'express';
import authCtrl from '../../controllers/admin/auth.controller';
import validation from '../../validations/index.validation';
import upload from '../../../config/multer/admin-profile';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/register-admin')
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                upload.single('profile_image'),
                validation.authValidation.validate('registerAdmin'),
            ], authCtrl.registerAdmin);

        this.router.route('/login-admin')
            .post([
                validation.authValidation.validate('loginAdmin'),
            ], authCtrl.loginAdmin);

        this.router.route('/logout-admin')
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], authCtrl.logoutAdmin);

        return this.router;
    }
}

export default Route;
