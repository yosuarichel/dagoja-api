import express from 'express';
import paymentMethodCtrl from '../../controllers/admin/payment-method.controller';
import validation from '../../validations/index.validation';
import imageUploader from '../../../config/multer/upload';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], paymentMethodCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                imageUploader.single('payment_method_image'),
                validation.paymentMethodValidation.validate('createPaymentMethod'),
            ], paymentMethodCtrl.create);

        this.router.route('/:paymentMethodId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], paymentMethodCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                imageUploader.single('payment_method_image'),
                validation.paymentMethodValidation.validate('updatePaymentMethod'),
            ], paymentMethodCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], paymentMethodCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('paymentMethodId', paymentMethodCtrl.load);

        return this.router;
    }
}

export default Route;
