import express from 'express';
import bannerTypeCtrl from '../../controllers/admin/banner-type.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], bannerTypeCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.bannerTypeValidation.validate('createBannerType'),
            ], bannerTypeCtrl.create);

        this.router.route('/:bannerTypeId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], bannerTypeCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.bannerTypeValidation.validate('updateBannerType'),
            ], bannerTypeCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], bannerTypeCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('bannerTypeId', bannerTypeCtrl.load);

        return this.router;
    }
}

export default Route;
