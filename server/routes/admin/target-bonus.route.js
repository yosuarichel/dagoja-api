import express from 'express';
import targetBonusCtrl from '../../controllers/admin/target-bonus.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], targetBonusCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.targetBonusValidation.validate('createTargetBonus'),
            ], targetBonusCtrl.create);

        this.router.route('/:targetBonusId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], targetBonusCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.targetBonusValidation.validate('updateTargetBonus'),
            ], targetBonusCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], targetBonusCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('targetBonusId', targetBonusCtrl.load);

        return this.router;
    }
}

export default Route;
