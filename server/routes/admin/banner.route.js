import express from 'express';
import bannerCtrl from '../../controllers/admin/banner.controller';
import validation from '../../validations/index.validation';
import bannerImageUploader from '../../../config/multer/banner';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], bannerCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                bannerImageUploader.single('banner_image'),
                validation.bannerValidation.validate('createBanner'),
            ], bannerCtrl.create);

        this.router.route('/:bannerId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], bannerCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                bannerImageUploader.single('banner_image'),
                validation.bannerValidation.validate('updateBanner'),
            ], bannerCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], bannerCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('bannerId', bannerCtrl.load);

        return this.router;
    }
}

export default Route;
