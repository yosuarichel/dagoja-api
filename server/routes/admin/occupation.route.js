import express from 'express';
import occupationCtrl from '../../controllers/admin/occupation.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], occupationCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.occupationValidation.validate('createOccupation'),
            ], occupationCtrl.create);

        this.router.route('/:occupationId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], occupationCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.occupationValidation.validate('updateOccupation'),
            ], occupationCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], occupationCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('occupationId', occupationCtrl.load);

        return this.router;
    }
}

export default Route;
