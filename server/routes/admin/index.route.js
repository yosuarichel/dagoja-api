import express from 'express';
import AdminRoute from './admin.route';
import SettingRoute from './setting.route';
import AdminRoleRoute from './admin-role.route';
import AuthRoute from './auth.route';
import LoginMethodRoute from './login-method.route';
import AppVersionRoute from './app-version.route';
import BrandRoute from './brand.route';
import CategoryRoute from './category.route';
import ProductRoute from './product.route';
import ProductGaleryRoute from './product-galery.route';
import TargetBonusRoute from './target-bonus.route';
import AffilialBonusRoute from './affilial-bonus.route';
import SupplierRoute from './supplier.route';
import PhoneOperatorRoute from './phone-operator.route';
import BannerTypeRoute from './banner-type.route';
import BannerRoute from './banner.route';
import OccupationRoute from './occupation.route';
import CountryRoute from './country.route';
import ProvinceRoute from './province.route';
import CityRoute from './city.route';
import DistrictRoute from './district.route';
import VillageRoute from './village.route';
import BankRoute from './bank.route';
import SupplierLogRoute from './supplier-log.route';
import RegulationRoute from './regulation.route';
import SupplierNotificationRoute from './supplier-notification.route';
import PaymentMethodRoute from './payment-method.route';

const router = express.Router();

export default class Route {
    constructor() {
        this.router = router;

        this.router.use('/admin', new AdminRoute());
        this.router.use('/setting', new SettingRoute());
        this.router.use('/admin-role', new AdminRoleRoute());
        this.router.use('/auth', new AuthRoute());
        this.router.use('/login-method', new LoginMethodRoute());
        this.router.use('/app-version', new AppVersionRoute());
        this.router.use('/brand', new BrandRoute());
        this.router.use('/category', new CategoryRoute());
        this.router.use('/product', new ProductRoute());
        this.router.use('/product-galery', new ProductGaleryRoute());
        this.router.use('/target-bonus', new TargetBonusRoute());
        this.router.use('/affilial-bonus', new AffilialBonusRoute());
        this.router.use('/supplier', new SupplierRoute());
        this.router.use('/phone-operator', new PhoneOperatorRoute());
        this.router.use('/banner-type', new BannerTypeRoute());
        this.router.use('/banner', new BannerRoute());
        this.router.use('/occupation', new OccupationRoute());
        this.router.use('/country', new CountryRoute());
        this.router.use('/province', new ProvinceRoute());
        this.router.use('/city', new CityRoute());
        this.router.use('/district', new DistrictRoute());
        this.router.use('/village', new VillageRoute());
        this.router.use('/bank', new BankRoute());
        this.router.use('/supplier-log', new SupplierLogRoute());
        this.router.use('/regulation', new RegulationRoute());
        this.router.use('/supplier-notification', new SupplierNotificationRoute());
        this.router.use('/payment-method', new PaymentMethodRoute());

        return this.router;
    }
}
