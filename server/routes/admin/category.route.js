import express from 'express';
import categoryCtrl from '../../controllers/admin/category.controller';
import validation from '../../validations/index.validation';
import categoryImageUploader from '../../../config/multer/category';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], categoryCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                categoryImageUploader.single('category_image'),
                validation.categoryValidation.validate('createCategory'),
            ], categoryCtrl.create);

        this.router.route('/:categoryId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], categoryCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                categoryImageUploader.single('category_image'),
                validation.categoryValidation.validate('updateCategory'),
            ], categoryCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], categoryCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('categoryId', categoryCtrl.load);

        return this.router;
    }
}

export default Route;
