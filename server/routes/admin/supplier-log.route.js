import express from 'express';
import supplierLogCtrl from '../../controllers/admin/supplier-log.controller';
import validation from '../../validations/index.validation';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierLogCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.supplierLogValidation.validate('createSupplierLog'),
            ], supplierLogCtrl.create);

        this.router.route('/:supplierLogId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierLogCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.supplierLogValidation.validate('updateSupplierLog'),
            ], supplierLogCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierLogCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('supplierLogId', supplierLogCtrl.load);

        return this.router;
    }
}

export default Route;
