import express from 'express';
import supplierCtrl from '../../controllers/admin/supplier.controller';
import validation from '../../validations/index.validation';
import upload from '../../../config/multer/upload';
import Middleware from '../../misc/middleware';

const router = express.Router();

class Route {
    constructor() {
        this.router = router;

        this.router.route('/')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierCtrl.list)

            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                upload.single('profile_image'),
                validation.supplierValidation.validate('createSupplier'),
            // middleware.sanitizePhoneNumber,
            ], supplierCtrl.create);

        this.router.route('/approve')
            .post([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                validation.supplierValidation.validate('approveSupplier'),
            // middleware.sanitizePhoneNumber,
            ], supplierCtrl.approve);

        this.router.route('/:supplierId')
            .get([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierCtrl.get)

            .put([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
                upload.single('profile_image'),
                validation.supplierValidation.validate('updateSupplier'),
            ], supplierCtrl.update)

            .delete([
                (req, res, next) => Middleware.checkTokenJwt(req, res, next, ['admin']),
                Middleware.checkAdminSession,
            ], supplierCtrl.remove);

        /** Load when API with Id route parameter is hit */
        this.router.param('supplierId', supplierCtrl.load);

        return this.router;
    }
}

export default Route;
