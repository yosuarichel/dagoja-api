import express from 'express';
import path from 'path';
import AdminRoute from './admin/index.route';
import SupplierRoute from './supplier/index.route';
import DropshipperRoute from './dropshipper/index.route';

const router = express.Router();

export default class MainRoute {
    constructor() {
        this.router = router;

        /** GET /health-check - Check service health */
        this.router.get('/health-check', (req, res) => {
            res.send('OK');
        });

        this.router.get('/admin-documentation', (req, res) => {
            res.sendFile(path.resolve('./docs/dagoja-admin-documentation.html'));
        });

        // mount user routes at /users
        this.router.use('/admin', new AdminRoute());
        this.router.use('/supplier', new SupplierRoute());
        this.router.use('/dropshipper', new DropshipperRoute());

        return this.router;
    }
}
