class Response {
    constructor(req, res) {
        this.req = req;
        this.res = res;
    }

    successResponse(response, result) {
        return this.res.status(response).json({
            response,
            result,
        });
    }

    failResponse(response, result, error) {
        if (response === 500 && this.req.app.sentry) {
            this.req.app.sentry.captureException(error);
            // eslint-disable-next-line no-console
            console.log('\x1b[31m', 'Error: ', error);
        }
        return this.res.status(response).json({
            response,
            result,
        });
    }
}

export default Response;
